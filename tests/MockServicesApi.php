<?php

namespace Tests;

use Ensi\AdminAuthClient\Api\EnumsApi as AdminAuthEnumsApi;
use Ensi\AdminAuthClient\Api\OauthApi;
use Ensi\AdminAuthClient\Api\RolesApi as AdminAuthUserRolesApi;
use Ensi\AdminAuthClient\Api\UsersApi as AdminAuthUsersApi;
use Ensi\BasketsClient\Api\CommonApi as BasketsCommonApi;
use Ensi\BuClient\Api\SellersApi;
use Ensi\BuClient\Api\StoresApi;
use Ensi\CmsClient\Api\NameplateProductsApi;
use Ensi\CmsClient\Api\NameplatesApi;
use Ensi\CmsClient\Api\PagesApi;
use Ensi\CmsClient\Api\ProductsApi as CmsProductsApi;
use Ensi\CustomerAuthClient\Api\UsersApi as CustomerAuthUsersApi;
use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\LogisticClient\Api\CargoOrdersApi;
use Ensi\LogisticClient\Api\DeliveryPricesApi;
use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Api\GeosApi;
use Ensi\LogisticClient\Api\KpiApi;
use Ensi\MarketingClient\Api\DiscountProductsApi;
use Ensi\MarketingClient\Api\DiscountRelationsApi;
use Ensi\MarketingClient\Api\DiscountsApi;
use Ensi\MarketingClient\Api\EnumsApi as MarketingEnumsApi;
use Ensi\MarketingClient\Api\PromoCodesApi;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Api\StocksApi;
use Ensi\OmsClient\Api\CommonApi as OmsCommonApi;
use Ensi\OmsClient\Api\DeliveriesApi;
use Ensi\OmsClient\Api\EnumsApi as OmsEnumsApi;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Api\RefundsApi;
use Ensi\PimClient\Api\BrandsApi;
use Ensi\PimClient\Api\CategoriesApi;
use Ensi\PimClient\Api\CommonApi;
use Ensi\PimClient\Api\EnumsApi as PimEnumsApi;
use Ensi\PimClient\Api\ProductFieldsApi;
use Ensi\PimClient\Api\ProductGroupsApi;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Api\PropertiesApi;
use Mockery\MockInterface;

trait MockServicesApi
{
    // =============== Catalog ===============

    // region service Offers
    protected function mockOffersOffersApi(): MockInterface|OffersApi
    {
        return $this->mock(OffersApi::class);
    }

    protected function mockOffersStocksApi(): MockInterface|StocksApi
    {
        return $this->mock(StocksApi::class);
    }
    // endregion

    // region service Pim
    protected function mockPimProductsApi(): MockInterface|ProductsApi
    {
        return $this->mock(ProductsApi::class);
    }

    protected function mockPimPropertiesApi(): MockInterface|PropertiesApi
    {
        return $this->mock(PropertiesApi::class);
    }

    protected function mockPimEnumsApi(): MockInterface|PimEnumsApi
    {
        return $this->mock(PimEnumsApi::class);
    }

    protected function mockPimCategoriesApi(): MockInterface|CategoriesApi
    {
        return $this->mock(CategoriesApi::class);
    }

    protected function mockPimBrandsApi(): MockInterface|BrandsApi
    {
        return $this->mock(BrandsApi::class);
    }

    protected function mockPimProductFieldsApi(): MockInterface|ProductFieldsApi
    {
        return $this->mock(ProductFieldsApi::class);
    }

    protected function mockPimProductGroupsApi(): MockInterface|ProductGroupsApi
    {
        return $this->mock(ProductGroupsApi::class);
    }

    protected function mockPimCommonApi(): MockInterface|CommonApi
    {
        return $this->mock(CommonApi::class);
    }
    // endregion


    // =============== CMS ===============

    // region service CMS
    protected function mockCmsPagesApi(): MockInterface|PagesApi
    {
        return $this->mock(PagesApi::class);
    }

    protected function mockCmsNameplatesApi(): MockInterface|NameplatesApi
    {
        return $this->mock(NameplatesApi::class);
    }

    protected function mockCmsNameplateProductsApi(): MockInterface|NameplateProductsApi
    {
        return $this->mock(NameplateProductsApi::class);
    }

    protected function mockCmsProductsApi(): MockInterface|CmsProductsApi
    {
        return $this->mock(CmsProductsApi::class);
    }
    // endregion


    // =============== Customers ===============

    // region service Customers
    public function mockCustomersCustomersApi(): MockInterface|CustomersApi
    {
        return $this->mock(CustomersApi::class);
    }

    public function mockCustomersAuthUsersApi(): MockInterface|CustomerAuthUsersApi
    {
        return $this->mock(CustomerAuthUsersApi::class);
    }
    // endregion


    // =============== Logistic ===============

    // region service Logistic
    protected function mockLogisticCargoOrdersApi(): MockInterface|CargoOrdersApi
    {
        return $this->mock(CargoOrdersApi::class);
    }

    protected function mockLogisticKpiApi(): MockInterface|KpiApi
    {
        return $this->mock(KpiApi::class);
    }

    public function mockLogisticDeliveryServicesApi(): MockInterface|DeliveryServicesApi
    {
        return $this->mock(DeliveryServicesApi::class);
    }

    public function mockLogisticGeosApi(): MockInterface|GeosApi
    {
        return $this->mock(GeosApi::class);
    }

    public function mockLogisticDeliveryPricesApi(): MockInterface|DeliveryPricesApi
    {
        return $this->mock(DeliveryPricesApi::class);
    }
    // endregion


    // =============== Marketing ===============

    // region service Marketing
    protected function mockMarketingEnumsApi(): MockInterface|MarketingEnumsApi
    {
        return $this->mock(MarketingEnumsApi::class);
    }

    protected function mockMarketingDiscountsApi(): MockInterface|DiscountsApi
    {
        return $this->mock(DiscountsApi::class);
    }

    protected function mockMarketingDiscountProductsApi(): MockInterface|DiscountProductsApi
    {
        return $this->mock(DiscountProductsApi::class);
    }

    protected function mockMarketingDiscountRelationsApi(): MockInterface|DiscountRelationsApi
    {
        return $this->mock(DiscountRelationsApi::class);
    }

    protected function mockMarketingPromoCodesApi(): MockInterface|PromoCodesApi
    {
        return $this->mock(PromoCodesApi::class);
    }
    // endregion


    // =============== Orders ===============

    // region service OMS
    public function mockOmsOrdersApi(): MockInterface|OrdersApi
    {
        return $this->mock(OrdersApi::class);
    }

    public function mockOmsDeliveriesApi(): MockInterface|DeliveriesApi
    {
        return $this->mock(DeliveriesApi::class);
    }

    public function mockOmsRefundsApi(): MockInterface|RefundsApi
    {
        return $this->mock(RefundsApi::class);
    }

    public function mockOmsEnumsApi(): MockInterface|OmsEnumsApi
    {
        return $this->mock(OmsEnumsApi::class);
    }

    public function mockOmsCommonApi(): MockInterface|OmsCommonApi
    {
        return $this->mock(OmsCommonApi::class);
    }
    // endregion

    // region service Baskets
    public function mockBasketsCommonApi(): MockInterface|BasketsCommonApi
    {
        return $this->mock(BasketsCommonApi::class);
    }
    // endregion


    // =============== Units ===============

    // region service Admin Auth
    public function mockAdminAuthOauthApi(): MockInterface|OauthApi
    {
        return $this->mock(OauthApi::class);
    }

    public function mockAdminAuthUsersApi(): MockInterface|AdminAuthUsersApi
    {
        return $this->mock(AdminAuthUsersApi::class);
    }

    public function mockAdminAuthEnumsApi(): MockInterface|AdminAuthEnumsApi
    {
        return $this->mock(AdminAuthEnumsApi::class);
    }

    public function mockAdminAuthUserRolesApi(): MockInterface|AdminAuthUserRolesApi
    {
        return $this->mock(AdminAuthUserRolesApi::class);
    }
    // endregion

    // region service Business Units
    protected function mockBusinessUnitsSellersApi(): MockInterface|SellersApi
    {
        return $this->mock(SellersApi::class);
    }

    protected function mockBusinessUnitsStoresApi(): MockInterface|StoresApi
    {
        return $this->mock(StoresApi::class);
    }
    // endregion
}
