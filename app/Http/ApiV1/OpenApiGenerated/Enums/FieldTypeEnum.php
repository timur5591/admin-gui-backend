<?php

/**
 * NOTE: This file is auto generated by OpenAPI Generator.
 * Do NOT edit it manually. Run `php artisan openapi:generate-server`.
 */

namespace App\Http\ApiV1\OpenApiGenerated\Enums;

/**
 * Типы поля. Расшифровка значений:
 * * `string` - Строка
 * * `email` - Email
 * * `phone` - Телефон
 * * `datetime` - Дата со временем
 * * `date` - Дата без времени
 * * `url` - Ссылка
 * * `int` - Целое число
 * * `price` - Цена
 * * `float` - Число с плавающей точкой
 * * `enum` - Перечисление
 * * `bool` - Булево
 * * `photo` - Фото
 * * `image` - Картинка
 * * `plural_numeric` - Числовое поле с несколькими типами
 */
enum FieldTypeEnum: string
{
    /** Строка */
    case STRING = 'string';
    /** Email */
    case EMAIL = 'email';
    /** Телефон */
    case PHONE = 'phone';
    /** Дата со временем */
    case DATETIME = 'datetime';
    /** Дата без времени */
    case DATE = 'date';
    /** Ссылка */
    case URL = 'url';
    /** Целое число */
    case INT = 'int';
    /** Цена */
    case PRICE = 'price';
    /** Число с плавающей точкой */
    case FLOAT = 'float';
    /** Перечисление */
    case ENUM = 'enum';
    /** Булево */
    case BOOL = 'bool';
    /** Фото */
    case PHOTO = 'photo';
    /** Картинка */
    case IMAGE = 'image';
    /** Числовое поле с несколькими типами */
    case PLURAL_NUMERIC = 'plural_numeric';
}
