<?php

/**
 * NOTE: This file is auto generated by OpenAPI Generator.
 * Do NOT edit it manually. Run `php artisan openapi:generate-server`.
 */

namespace App\Http\ApiV1\OpenApiGenerated\Enums;

/**
 * Статус отгрузки. Расшифровка значений:
 * * `10` - Новый
 * * `20` - В работе
 * * `100` - Собран
 * * `200` - Отменен
 */
enum OrdersShipmentStatusEnum: int
{
    /** Новый */
    case NEW = 10;
    /** В работе */
    case IN_WORK = 20;
    /** Собран */
    case ASSEMBLED = 100;
    /** Отменен */
    case CANCELED = 200;
}
