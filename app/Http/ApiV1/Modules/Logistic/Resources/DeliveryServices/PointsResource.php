<?php

namespace App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices;

use App\Http\ApiV1\Support\Resources\AddressResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\LogisticClient\Dto\Point;
use Illuminate\Http\Request;

/**
 *  Class PointsResource
 * @package App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices
 *
 * @mixin Point
 */
class PointsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'delivery_service' => $this->getDeliveryService(),
            'active' => $this->getActive(),
            'type' => $this->getType(),
            'name' => $this->getName(),
            'external_id' => $this->getExternalId(),
            'apiship_external_id' => $this->getApishipExternalId(),
            'has_payment_card' => $this->getHasPaymentCard(),
            'address' => new AddressResource($this->whenNotNull($this->getAddress())),
            'city_guid' => $this->getCityGuid(),
            'email' => $this->getEmail(),
            'phone' => $this->getPhone(),
            'timetable' => $this->getTimetable(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
            'metro_stations' => MetroStationsResource::collection($this->whenNotNull($this->getMetroStations())),
        ];
    }
}
