<?php

namespace App\Http\ApiV1\Modules\Logistic\Queries\DeliveryPrices;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderFirstTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\LogisticClient\Api\DeliveryPricesApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\DeliveryPriceResponse;
use Ensi\LogisticClient\Dto\RequestBodyPagination;
use Ensi\LogisticClient\Dto\SearchDeliveryPricesRequest;
use Ensi\LogisticClient\Dto\SearchDeliveryPricesResponse;
use Illuminate\Http\Request;

/**
 * Class DeliveryPricesQuery
 * @package App\Http\ApiV1\Modules\Logistic\Queries\DeliveryPrices
 */
class DeliveryPricesQuery extends QueryBuilder
{
    use QueryBuilderFindTrait;
    use QueryBuilderFirstTrait;
    use QueryBuilderGetTrait;

    /**
     * DeliveryPricesQuery constructor.
     * @param  Request  $httpRequest
     * @param  DeliveryPricesApi  $deliveryPricesApi
     */
    public function __construct(protected Request $httpRequest, protected DeliveryPricesApi $deliveryPricesApi)
    {
        parent::__construct($httpRequest);
    }

    /**
     * @return string
     */
    protected function requestFirstClass(): string
    {
        return SearchDeliveryPricesRequest::class;
    }

    /**
     * @return string
     */
    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    /**
     * @return string
     */
    protected function requestGetClass(): string
    {
        return SearchDeliveryPricesRequest::class;
    }

    /**
     * @param int $id
     * @return DeliveryPriceResponse
     * @throws ApiException
     */
    protected function searchById($id): DeliveryPriceResponse
    {
        return $this->deliveryPricesApi->getDeliveryPrice($id, $this->httpRequest->get('include'));
    }

    /**
     * @param SearchDeliveryPricesRequest $request
     * @return SearchDeliveryPricesResponse
     * @throws ApiException
     */
    protected function search($request): SearchDeliveryPricesResponse
    {
        return $this->deliveryPricesApi->searchDeliveryPrices($request);
    }

    /**
     * @param SearchDeliveryPricesRequest $request
     * @return DeliveryPriceResponse
     * @throws ApiException
     */
    protected function searchOne($request): DeliveryPriceResponse
    {
        return $this->deliveryPricesApi->searchOneDeliveryPrice($request);
    }
}
