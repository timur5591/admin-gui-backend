<?php

namespace App\Http\ApiV1\Modules\Logistic\Queries\DeliveryServices;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderFirstTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\DeliveryServiceResponse;
use Ensi\LogisticClient\Dto\RequestBodyPagination;
use Ensi\LogisticClient\Dto\SearchDeliveryServicesRequest;
use Ensi\LogisticClient\Dto\SearchDeliveryServicesResponse;
use Illuminate\Http\Request;

/**
 * Class DeliveryServicesQuery
 * @package App\Http\ApiV1\Modules\Logistic\Queries\DeliveryServices
 */
class DeliveryServicesQuery extends QueryBuilder
{
    use QueryBuilderFindTrait;
    use QueryBuilderFirstTrait;
    use QueryBuilderGetTrait;

    /**
     * DeliveryServicesQuery constructor.
     * @param Request $httpRequest
     * @param DeliveryServicesApi $deliveryServicesApi
     */
    public function __construct(protected Request $httpRequest, protected DeliveryServicesApi $deliveryServicesApi)
    {
        parent::__construct($httpRequest);
    }

    /**
     * @return string
     */
    protected function requestFirstClass(): string
    {
        return SearchDeliveryServicesRequest::class;
    }

    /**
     * @return string
     */
    protected function requestGetClass(): string
    {
        return SearchDeliveryServicesRequest::class;
    }

    /**
     * @return string
     */
    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    /**
     * @param $id
     * @return DeliveryServiceResponse
     * @throws ApiException
     */
    public function searchById($id): DeliveryServiceResponse
    {
        return $this->deliveryServicesApi->getDeliveryService($id, $this->httpRequest->get('include'));
    }

    /**
     * @param $request
     * @return SearchDeliveryServicesResponse
     * @throws ApiException
     */
    public function search($request): SearchDeliveryServicesResponse
    {
        return $this->deliveryServicesApi->searchDeliveryServices($request);
    }

    /**
     * @param $request
     * @return DeliveryServiceResponse
     * @throws ApiException
     */
    public function searchOne($request): DeliveryServiceResponse
    {
        return $this->deliveryServicesApi->searchOneDeliveryService($request);
    }
}
