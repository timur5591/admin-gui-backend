<?php

namespace App\Http\ApiV1\Modules\Logistic\Tests\DeliveryPrices\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryServiceEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class DeliveryPricesRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'federal_district_id' => $this->faker->modelId(),
            'region_id' => $this->faker->modelId(),
            'region_guid' => $this->faker->uuid(),
            'delivery_service' => $this->faker->randomElement(array_column(LogisticDeliveryServiceEnum::cases(), 'value')),
            'delivery_method' => $this->faker->randomElement(array_column(LogisticDeliveryMethodEnum::cases(), 'value')),
            'price' => $this->faker->randomNumber(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
