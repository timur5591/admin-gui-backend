<?php

namespace App\Http\ApiV1\Modules\Logistic\Tests\Geos\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class FederalDistrictsRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'name' => $this->faker->name(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
