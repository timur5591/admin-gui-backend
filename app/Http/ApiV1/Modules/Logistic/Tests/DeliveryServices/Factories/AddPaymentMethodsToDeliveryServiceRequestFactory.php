<?php

namespace App\Http\ApiV1\Modules\Logistic\Tests\DeliveryServices\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\OrdersPaymentMethodEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class AddPaymentMethodsToDeliveryServiceRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'payment_methods' => $this->faker->randomElements(array_column(OrdersPaymentMethodEnum::cases(), 'value')),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
