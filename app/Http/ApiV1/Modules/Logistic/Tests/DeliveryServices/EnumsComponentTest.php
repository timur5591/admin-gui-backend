<?php

use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LogisticClient\Dto\DeliveryMethodEnum;
use Ensi\LogisticClient\Dto\DeliveryServiceStatusEnum;
use Ensi\LogisticClient\Dto\ShipmentMethodEnum;

use function Pest\Laravel\getJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'logistic');

test('GET /api/v1/logistic/delivery-service-statuses 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => DeliveryServiceStatusEnum::ACTIVE,
        'name' => DeliveryServiceStatusEnum::getDescriptions()[DeliveryServiceStatusEnum::ACTIVE],
    ];

    getJson("/api/v1/logistic/delivery-service-statuses")
        ->assertStatus(200)
        ->assertJsonFragment($item);
});

test('GET /api/v1/logistic/delivery-methods 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => DeliveryMethodEnum::DELIVERY,
        'name' => DeliveryMethodEnum::getDescriptions()[DeliveryMethodEnum::DELIVERY],
    ];

    getJson("/api/v1/logistic/delivery-methods")
        ->assertStatus(200)
        ->assertJsonFragment($item);
});

test('GET /api/v1/logistic/shipment-methods 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => ShipmentMethodEnum::DS_COURIER,
        'name' => ShipmentMethodEnum::getDescriptions()[ShipmentMethodEnum::DS_COURIER],
    ];

    getJson("/api/v1/logistic/shipment-methods")
        ->assertStatus(200)
        ->assertJsonFragment($item);
});
