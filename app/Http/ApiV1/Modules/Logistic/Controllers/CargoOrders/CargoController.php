<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\CargoOrders;

use App\Domain\Logistic\Actions\CargoOrders\CancelCargoAction;
use App\Domain\Logistic\Actions\CargoOrders\PatchCargoAction;
use App\Http\ApiV1\Modules\Logistic\Queries\CargoOrders\CargoQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\CargoOrders\PatchCargoRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\CargoOrders\CargoResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CargoController
{
    public function patch(int $cargoId, PatchCargoRequest $request, PatchCargoAction $action): CargoResource
    {
        return CargoResource::make($action->execute($cargoId, $request->validated()));
    }

    public function cancel(int $cargoId, CancelCargoAction $action): CargoResource
    {
        return CargoResource::make($action->execute($cargoId));
    }

    public function search(CargoQuery $query): AnonymousResourceCollection
    {
        return CargoResource::collectPage($query->get());
    }
}
