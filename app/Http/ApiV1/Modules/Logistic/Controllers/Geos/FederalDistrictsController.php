<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\Geos;

use App\Domain\Logistic\Actions\Geos\CreateFederalDistrictAction;
use App\Domain\Logistic\Actions\Geos\DeleteFederalDistrictAction;
use App\Domain\Logistic\Actions\Geos\PatchFederalDistrictAction;
use App\Domain\Logistic\Actions\Geos\ReplaceFederalDistrictAction;
use App\Http\ApiV1\Modules\Logistic\Queries\Geos\FederalDistrictsQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\Geos\CreateOrReplaceFederalDistrictRequest;
use App\Http\ApiV1\Modules\Logistic\Requests\Geos\PatchFederalDistrictRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\Geos\FederalDistrictsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class FederalDistrictsController
 * @package App\Http\ApiV1\Modules\Logistic\Controllers\Geos
 */
class FederalDistrictsController
{
    public function create(CreateOrReplaceFederalDistrictRequest $request, CreateFederalDistrictAction $action): FederalDistrictsResource
    {
        return new FederalDistrictsResource($action->execute($request->validated()));
    }

    public function replace(int $id, CreateOrReplaceFederalDistrictRequest $request, ReplaceFederalDistrictAction $action): FederalDistrictsResource
    {
        return new FederalDistrictsResource($action->execute($id, $request->validated()));
    }

    public function patch(int $id, PatchFederalDistrictRequest $request, PatchFederalDistrictAction $action): FederalDistrictsResource
    {
        return new FederalDistrictsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteFederalDistrictAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, FederalDistrictsQuery $query): FederalDistrictsResource
    {
        return new FederalDistrictsResource($query->find($id));
    }

    public function search(FederalDistrictsQuery $query): AnonymousResourceCollection
    {
        return FederalDistrictsResource::collectPage($query->get());
    }

    public function searchOne(FederalDistrictsQuery $query): FederalDistrictsResource
    {
        return new FederalDistrictsResource($query->first());
    }
}
