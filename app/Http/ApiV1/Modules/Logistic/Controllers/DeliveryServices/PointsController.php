<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices;

use App\Http\ApiV1\Modules\Logistic\Queries\DeliveryServices\PointsQuery;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices\PointEnumValueResource;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices\PointsResource;
use App\Http\ApiV1\Support\Requests\CommonFilterEnumValuesRequest;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class PointsController
 * @package App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices
 */
class PointsController
{
    /**
     * @param PointsQuery $query
     * @return AnonymousResourceCollection
     */
    public function search(PointsQuery $query): AnonymousResourceCollection
    {
        return PointsResource::collectPage($query->get());
    }

    public function searchEnumValues(CommonFilterEnumValuesRequest $request, PointsQuery $query): AnonymousResourceCollection
    {
        return PointEnumValueResource::collection($query->searchEnums());
    }
}
