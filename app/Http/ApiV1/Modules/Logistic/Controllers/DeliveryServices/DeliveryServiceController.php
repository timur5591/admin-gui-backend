<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices;

use App\Domain\Logistic\Actions\DeliveryServices\PatchDeliveryServiceAction;
use App\Http\ApiV1\Modules\Logistic\Queries\DeliveryServices\DeliveryServicesQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices\PatchDeliveryServiceRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices\DeliveryServicesResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class DeliveryServiceController
 * @package App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices
 */
class DeliveryServiceController
{
    public function patch(int $id, PatchDeliveryServiceRequest $request, PatchDeliveryServiceAction $action): DeliveryServicesResource
    {
        return new DeliveryServicesResource($action->execute($id, $request->validated()));
    }

    public function get(int $id, DeliveryServicesQuery $query): DeliveryServicesResource
    {
        return new DeliveryServicesResource($query->find($id));
    }

    public function search(DeliveryServicesQuery $query): AnonymousResourceCollection
    {
        return DeliveryServicesResource::collectPage($query->get());
    }

    public function searchOne(DeliveryServicesQuery $query): DeliveryServicesResource
    {
        return new DeliveryServicesResource($query->first());
    }
}
