<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices;

use App\Domain\Logistic\Actions\DeliveryServiceManager\CreateDeliveryServiceManagerAction;
use App\Domain\Logistic\Actions\DeliveryServiceManager\DeleteDeliveryServiceManagerAction;
use App\Domain\Logistic\Actions\DeliveryServiceManager\PatchDeliveryServiceManagerAction;
use App\Domain\Logistic\Actions\DeliveryServiceManager\ReplaceDeliveryServiceManagerAction;
use App\Http\ApiV1\Modules\Logistic\Queries\DeliveryServices\DeliveryServiceManagersQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices\CreateOrReplaceDeliveryServiceManagerRequest;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices\PatchDeliveryServiceManagerRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices\DeliveryServiceManagersResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class DeliveryServiceManagersController
{
    public function create(CreateOrReplaceDeliveryServiceManagerRequest $request, CreateDeliveryServiceManagerAction $action): DeliveryServiceManagersResource
    {
        return new DeliveryServiceManagersResource($action->execute($request->validated()));
    }

    public function replace(int $id, CreateOrReplaceDeliveryServiceManagerRequest $request, ReplaceDeliveryServiceManagerAction $action): DeliveryServiceManagersResource
    {
        return new DeliveryServiceManagersResource($action->execute($id, $request->validated()));
    }

    public function patch(int $id, PatchDeliveryServiceManagerRequest $request, PatchDeliveryServiceManagerAction $action): DeliveryServiceManagersResource
    {
        return new DeliveryServiceManagersResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteDeliveryServiceManagerAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, DeliveryServiceManagersQuery $query): DeliveryServiceManagersResource
    {
        return new DeliveryServiceManagersResource($query->find($id));
    }

    public function search(DeliveryServiceManagersQuery $query): AnonymousResourceCollection
    {
        return DeliveryServiceManagersResource::collectPage($query->get());
    }

    public function searchOne(DeliveryServiceManagersQuery $query): DeliveryServiceManagersResource
    {
        return new DeliveryServiceManagersResource($query->first());
    }
}
