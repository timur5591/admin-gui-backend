<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices;

use App\Domain\Logistic\Actions\DeliveryServices\AddPaymentMethodsToDeliveryServiceAction;
use App\Domain\Logistic\Actions\DeliveryServices\DeletePaymentMethodFromDeliveryServiceAction;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices\AddPaymentMethodsToDeliveryServiceRequest;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices\DeletePaymentMethodFromDeliveryServiceRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class DeliveryServicePaymentMethodLinksController
{
    public function add(int $id, AddPaymentMethodsToDeliveryServiceRequest $request, AddPaymentMethodsToDeliveryServiceAction $action): EmptyResource
    {
        $action->execute($id, $request->getPaymentMethods());

        return new EmptyResource();
    }

    public function delete(int $id, DeletePaymentMethodFromDeliveryServiceRequest $request, DeletePaymentMethodFromDeliveryServiceAction $action): EmptyResource
    {
        $action->execute($id, $request->getPaymentMethod());

        return new EmptyResource();
    }
}
