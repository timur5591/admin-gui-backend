<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices;

use App\Domain\Logistic\Actions\DeliveryServiceDocument\CreateDeliveryServiceDocumentAction;
use App\Domain\Logistic\Actions\DeliveryServiceDocument\DeleteDeliveryServiceDocumentAction;
use App\Domain\Logistic\Actions\DeliveryServiceDocument\PatchDeliveryServiceDocumentAction;
use App\Domain\Logistic\Actions\DeliveryServiceDocument\ReplaceDeliveryServiceDocumentAction;
use App\Http\ApiV1\Modules\Logistic\Queries\DeliveryServices\DeliveryServiceDocumentsQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices\CreateOrReplaceDeliveryServiceDocumentRequest;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices\PatchDeliveryServiceDocumentRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices\DeliveryServiceDocumentsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class DeliveryServiceDocumentsController
{
    public function create(CreateOrReplaceDeliveryServiceDocumentRequest $request, CreateDeliveryServiceDocumentAction $action): DeliveryServiceDocumentsResource
    {
        return new DeliveryServiceDocumentsResource($action->execute($request->validated()));
    }

    public function replace(int $id, CreateOrReplaceDeliveryServiceDocumentRequest $request, ReplaceDeliveryServiceDocumentAction $action): DeliveryServiceDocumentsResource
    {
        return new DeliveryServiceDocumentsResource($action->execute($id, $request->validated()));
    }

    public function patch(int $id, PatchDeliveryServiceDocumentRequest $request, PatchDeliveryServiceDocumentAction $action): DeliveryServiceDocumentsResource
    {
        return new DeliveryServiceDocumentsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteDeliveryServiceDocumentAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, DeliveryServiceDocumentsQuery $query): DeliveryServiceDocumentsResource
    {
        return new DeliveryServiceDocumentsResource($query->find($id));
    }

    public function search(DeliveryServiceDocumentsQuery $query): AnonymousResourceCollection
    {
        return DeliveryServiceDocumentsResource::collectPage($query->get());
    }

    public function searchOne(DeliveryServiceDocumentsQuery $query): DeliveryServiceDocumentsResource
    {
        return new DeliveryServiceDocumentsResource($query->first());
    }
}
