<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryKpis;

use App\Domain\Logistic\Actions\DeliveryKpis\CreateDeliveryKpiCtAction;
use App\Domain\Logistic\Actions\DeliveryKpis\DeleteDeliveryKpiCtAction;
use App\Domain\Logistic\Actions\DeliveryKpis\ReplaceDeliveryKpiCtAction;
use App\Http\ApiV1\Modules\Logistic\Queries\DeliveryKpis\DeliveryKpiCtQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryKpis\CreateOrReplaceDeliveryKpiCtRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryKpis\DeliveryKpiCtResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Ensi\LogisticClient\ApiException;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class DeliveryKpiCtController
 * @package App\Http\ApiV1\Modules\DeliveryKpis\Controllers
 */
class DeliveryKpiCtController
{
    /**
     * @param DeliveryKpiCtQuery $query
     * @return AnonymousResourceCollection
     */
    public function search(DeliveryKpiCtQuery $query): AnonymousResourceCollection
    {
        return DeliveryKpiCtResource::collectPage($query->get());
    }

    /**
     * @param DeliveryKpiCtQuery $query
     * @return DeliveryKpiCtResource
     */
    public function searchOne(DeliveryKpiCtQuery $query): DeliveryKpiCtResource
    {
        return new DeliveryKpiCtResource($query->first());
    }

    /**
     * @param int $sellerId
     * @param DeliveryKpiCtQuery $query
     * @return DeliveryKpiCtResource
     */
    public function get(int $sellerId, DeliveryKpiCtQuery $query): DeliveryKpiCtResource
    {
        return new DeliveryKpiCtResource($query->find($sellerId));
    }

    /**
     * @param int $sellerId
     * @param CreateOrReplaceDeliveryKpiCtRequest $request
     * @param CreateDeliveryKpiCtAction $action
     * @return DeliveryKpiCtResource
     * @throws ApiException
     */
    public function create(int $sellerId, CreateOrReplaceDeliveryKpiCtRequest $request, CreateDeliveryKpiCtAction $action): DeliveryKpiCtResource
    {
        return new DeliveryKpiCtResource($action->execute($sellerId, $request->validated()));
    }

    /**
     * @param int $sellerId
     * @param CreateOrReplaceDeliveryKpiCtRequest $request
     * @param ReplaceDeliveryKpiCtAction $action
     * @return DeliveryKpiCtResource
     * @throws ApiException
     */
    public function replace(int $sellerId, CreateOrReplaceDeliveryKpiCtRequest $request, ReplaceDeliveryKpiCtAction $action): DeliveryKpiCtResource
    {
        return new DeliveryKpiCtResource($action->execute($sellerId, $request->validated()));
    }

    /**
     * @param int $sellerId
     * @param DeleteDeliveryKpiCtAction $action
     * @return EmptyResource
     * @throws ApiException
     */
    public function delete(int $sellerId, DeleteDeliveryKpiCtAction $action): EmptyResource
    {
        $action->execute($sellerId);

        return new EmptyResource();
    }
}
