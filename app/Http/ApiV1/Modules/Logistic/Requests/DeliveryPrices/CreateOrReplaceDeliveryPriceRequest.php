<?php

namespace App\Http\ApiV1\Modules\Logistic\Requests\DeliveryPrices;

use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryServiceEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

/**
 * Class CreateOrReplaceDeliveryPriceRequest
 * @package App\Http\ApiV1\Modules\Logistic\Requests\DeliveryPrices
 */
class CreateOrReplaceDeliveryPriceRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'federal_district_id' => ['required', 'integer'],
            'region_id' => ['nullable', 'integer'],
            'region_guid' => ['nullable', 'string'],
            'delivery_service' => ['required', new Enum(LogisticDeliveryServiceEnum::class)],
            'delivery_method' => ['required', new Enum(LogisticDeliveryMethodEnum::class)],
            'price' => ['required', 'integer', 'min:0'],
      ];
    }
}
