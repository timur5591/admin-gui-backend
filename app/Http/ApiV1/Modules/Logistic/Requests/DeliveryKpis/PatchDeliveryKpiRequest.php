<?php

namespace App\Http\ApiV1\Modules\Logistic\Requests\DeliveryKpis;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

/**
 * Class PatchDeliveryKpiRequest
 * @package App\Http\ApiV1\Modules\Logistic\Requests\DeliveryKpis
 */
class PatchDeliveryKpiRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'rtg' => ['nullable', 'integer', 'min:0'],
            'ct' => ['nullable', 'integer', 'min:0'],
            'ppt' => ['nullable', 'integer', 'min:0'],
      ];
    }
}
