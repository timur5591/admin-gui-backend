<?php

namespace App\Http\ApiV1\Modules\Logistic\Requests\Geos;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

/**
 * Class PatchRegionRequest
 * @package App\Http\ApiV1\Modules\Logistic\Requests\Geos
 */
class PatchRegionRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'federal_district_id' => ['nullable', 'integer'],
            'name' => ['nullable', 'string'],
            'guid' => ['nullable', 'string'],
      ];
    }
}
