<?php

namespace App\Http\ApiV1\Modules\Cms\Policies;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class PagesControllerPolicy
{
    use HandlesAuthorization;

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PAGE_LIST_READ,
            RightsAccessEnum::PAGE_DETAIL_READ,
        ]);
    }

    public function searchOne(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PAGE_DETAIL_READ,
        ]);
    }

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PAGE_DETAIL_READ,
        ]);
    }

    public function create(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PAGE_CREATE,
        ]);
    }

    public function patch(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PAGE_DETAIL_EDIT,
            RightsAccessEnum::PAGE_DETAIL_ACTIVE_EDIT,
        ]);
    }

    public function delete(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PAGE_DELETE,
        ]);
    }
}
