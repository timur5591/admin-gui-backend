<?php

namespace App\Http\ApiV1\Modules\Cms\Policies;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class NameplateProductsControllerPolicy
{
    use HandlesAuthorization;

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::NAMEPLATE_DETAIL_READ,
        ]);
    }

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::NAMEPLATE_DETAIL_READ,
        ]);
    }
}
