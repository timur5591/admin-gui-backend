<?php

namespace App\Http\ApiV1\Modules\Cms\Policies;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class NameplatesControllerPolicy
{
    use HandlesAuthorization;

    public function create(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::NAMEPLATE_CREATE,
        ]);
    }

    public function patch(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::NAMEPLATE_DETAIL_EDIT,
        ]);
    }

    public function delete(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::NAMEPLATE_DELETE,
        ]);
    }

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::NAMEPLATE_DETAIL_READ,
        ]);
    }

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::NAMEPLATE_LIST_READ,
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_NAMEPLATES_READ,
        ]);
    }

    public function addProducts(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::NAMEPLATE_DETAIL_EDIT,
        ]);
    }

    public function deleteProducts(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::NAMEPLATE_DETAIL_EDIT,
        ]);
    }
}
