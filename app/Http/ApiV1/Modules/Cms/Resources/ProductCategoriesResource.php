<?php

namespace App\Http\ApiV1\Modules\Cms\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CmsClient\Dto\ProductCategory;

/**
 * @mixin ProductCategory
 */
class ProductCategoriesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'url' => $this->getUrl(),
            'active' => $this->getActive(),
            'order' => $this->getOrder(),
            'parent_id' => $this->getParentId(),
            'pim_categories' => ProductPimCategoriesResource::collection($this->whenNotNull($this->getPimCategories())),
        ];
    }
}
