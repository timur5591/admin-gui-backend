<?php

namespace App\Http\ApiV1\Modules\Cms\Resources;

use App\Domain\Cms\Data\Nameplates\NameplateProductData;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductsResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin NameplateProductData */
class NameplateProductsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->data->getId(),
            'nameplate_id' => $this->data->getNameplateId(),
            'product_id' => $this->data->getProductId(),
            'product' => ProductsResource::make($this->whenNotNull($this->product)),

            'created_at' => $this->dateTimeToIso($this->data->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->data->getUpdatedAt()),
        ];
    }
}
