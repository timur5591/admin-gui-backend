<?php

namespace App\Http\ApiV1\Modules\Cms\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CmsClient\Dto\Nameplate;

/** @mixin Nameplate */
class NameplatesResource extends BaseJsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'code' => $this->getCode(),

            'background_color' => $this->getBackgroundColor(),
            'text_color' => $this->getTextColor(),

            'is_active' => $this->getIsActive(),

            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
        ];
    }
}
