<?php

namespace App\Http\ApiV1\Modules\Cms\Resources;

use App\Http\ApiV1\Modules\Catalog\Resources\Properties\DirectoryValuesResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\Property;
use Illuminate\Http\Request;

/** @mixin Property */
class ProductGroupFiltersResource extends BaseJsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'code' => $this->getCode(),
            'name' => $this->getName(),
            'display_name' => $this->getDisplayName(),
            'type' => $this->getType(),
            'is_multiple' => $this->getIsMultiple(),
            'is_filterable' => $this->getIsFilterable(),

            'directory' => DirectoryValuesResource::collection($this->whenNotNull($this->getDirectory())),
        ];
    }
}
