<?php

namespace App\Http\ApiV1\Modules\Cms\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CmsClient\Dto\BannerButton;
use Illuminate\Http\Request;

/** @mixin BannerButton */
class BannerButtonsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'url' => $this->getUrl(),
            'text' => $this->getText(),
            'location' => $this->getLocation(),
            'type' => $this->getType(),
        ];
    }
}
