<?php

namespace App\Http\ApiV1\Modules\Cms\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CmsClient\Dto\ProductPimCategoryFilter;

/**
 * @mixin ProductPimCategoryFilter
 */
class ProductPimCategoryFiltersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'code' => $this->getCode(),
            'value' => $this->getValue(),
        ];
    }
}
