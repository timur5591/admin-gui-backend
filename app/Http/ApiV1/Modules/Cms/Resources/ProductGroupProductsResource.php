<?php

namespace App\Http\ApiV1\Modules\Cms\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CmsClient\Dto\ProductGroupProduct;
use Illuminate\Http\Request;

/** @mixin ProductGroupProduct */
class ProductGroupProductsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'sort' => $this->getSort(),
            'product_group_id' => $this->getProductGroupId(),
            'product_id' => $this->getProductId(),
        ];
    }
}
