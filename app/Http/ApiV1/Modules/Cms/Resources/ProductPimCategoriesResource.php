<?php

namespace App\Http\ApiV1\Modules\Cms\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CmsClient\Dto\ProductPimCategory;

/**
 * @mixin ProductPimCategory
 */
class ProductPimCategoriesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'code' => $this->getCode(),
            'product_category_id' => $this->getProductCategoryId(),
            'filters' => ProductPimCategoryFiltersResource::collection($this->whenNotNull($this->getFilters())),
        ];
    }
}
