<?php

namespace App\Http\ApiV1\Modules\Cms\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CmsClient\Api\ProductPimCategoriesApi;
use Ensi\CmsClient\ApiException;
use Ensi\CmsClient\Dto\ErrorResponse;
use Ensi\CmsClient\Dto\ProductPimCategoryResponse;
use Ensi\CmsClient\Dto\RequestBodyPagination;
use Ensi\CmsClient\Dto\SearchProductPimCategoriesRequest;
use Ensi\CmsClient\Dto\SearchProductPimCategoriesResponse;
use Illuminate\Http\Request;

class ProductPimCategoriesQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;
    use QueryBuilderFindTrait;

    public function __construct(Request $request, private ProductPimCategoriesApi $api)
    {
        parent::__construct($request);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id): ProductPimCategoryResponse|ErrorResponse
    {
        return $this->api->getProductPimCategory($id, $this->getInclude());
    }

    /**
     * @throws ApiException
     */
    protected function search($request): ErrorResponse|SearchProductPimCategoriesResponse
    {
        return $this->api->searchProductPimCategory($request);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchProductPimCategoriesRequest::class;
    }
}
