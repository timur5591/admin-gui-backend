<?php

namespace App\Http\ApiV1\Modules\Cms\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFirstTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CmsClient\Api\MenusApi;
use Ensi\CmsClient\Dto\RequestBodyPagination;
use Ensi\CmsClient\Dto\SearchMenusRequest;
use Ensi\CmsClient\Dto\SearchOneMenusRequest;
use Illuminate\Http\Request;

class SearchMenusQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;
    use QueryBuilderFirstTrait;

    public function __construct(
        Request $httpRequest,
        protected MenusApi $menusApi,
    ) {
        parent::__construct($httpRequest);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchMenusRequest::class;
    }

    protected function requestFirstClass(): string
    {
        return SearchOneMenusRequest::class;
    }

    protected function search($request)
    {
        return $this->menusApi->searchMenus($request);
    }

    protected function searchOne($request)
    {
        return $this->menusApi->searchOneMenus($request);
    }
}
