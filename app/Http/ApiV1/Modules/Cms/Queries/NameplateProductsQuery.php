<?php

namespace App\Http\ApiV1\Modules\Cms\Queries;

use App\Domain\Cms\Data\Nameplates\NameplateProductData;
use App\Domain\Common\Actions\AsyncLoadAction;
use App\Http\ApiV1\Modules\Cms\Queries\IncludeLoaders\CmsProductsLoader;
use Ensi\CmsClient\Api\NameplateProductsApi;
use Ensi\CmsClient\ApiException;
use Ensi\CmsClient\Dto\NameplateProduct;
use Ensi\CmsClient\Dto\NameplateProductResponse;
use Ensi\CmsClient\Dto\SearchNameplateProductsRequest;
use Ensi\CmsClient\Dto\SearchNameplateProductsResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class NameplateProductsQuery extends CmsQuery
{
    public function __construct(
        protected AsyncLoadAction $asyncLoadAction,
        protected CmsProductsLoader $productsLoader,
        protected NameplateProductsApi $api,
        Request $request,
    ) {
        parent::__construct($request, SearchNameplateProductsRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id): NameplateProductResponse
    {
        return $this->api->getNameplateProduct($id);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchNameplateProductsResponse
    {
        return $this->api->searchNameplateProducts($request);
    }

    protected function convertFindToItem($response): NameplateProductData
    {
        return $this->convertArray([$response->getData()])->first();
    }

    protected function convertGetToItems($response): Collection
    {
        return $this->convertArray($response->getData());
    }

    protected function convertArray(array $array): Collection
    {
        /** @var Collection<NameplateProduct> $nameplateProducts */
        $nameplateProducts = collect($array);

        $this->productsLoader->setProductIds($nameplateProducts->pluck('product_id'));
        $this->asyncLoadAction->execute([$this->productsLoader]);

        return NameplateProductData::mapCollect($nameplateProducts, $this->productsLoader->products);
    }

    protected function getHttpInclude(): array
    {
        $httpIncludes = parent::getHttpInclude();

        $includes = [];
        foreach ($httpIncludes as $include) {
            switch ($include) {
                case "product":
                    $this->productsLoader->load = true;

                    break;
                default:
                    $includes[] = $include;
            }
        }

        return array_values(array_unique($includes));
    }
}
