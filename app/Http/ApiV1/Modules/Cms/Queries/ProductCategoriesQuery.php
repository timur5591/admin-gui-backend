<?php

namespace App\Http\ApiV1\Modules\Cms\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CmsClient\Api\ProductCategoriesApi;
use Ensi\CmsClient\ApiException;
use Ensi\CmsClient\Dto\ErrorResponse;
use Ensi\CmsClient\Dto\ProductCategoryResponse;
use Ensi\CmsClient\Dto\RequestBodyPagination;
use Ensi\CmsClient\Dto\SearchProductCategoriesRequest;
use Ensi\CmsClient\Dto\SearchProductCategoriesResponse;
use Illuminate\Http\Request;

class ProductCategoriesQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;
    use QueryBuilderFindTrait;

    public function __construct(Request $request, private ProductCategoriesApi $api)
    {
        parent::__construct($request);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id): ProductCategoryResponse|ErrorResponse
    {
        return $this->api->getProductCategory($id, $this->getInclude());
    }

    /**
     * @throws ApiException
     */
    protected function search($request): ErrorResponse|SearchProductCategoriesResponse
    {
        return $this->api->searchProductCategories($request);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchProductCategoriesRequest::class;
    }
}
