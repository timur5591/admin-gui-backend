<?php

namespace App\Http\ApiV1\Modules\Cms\Queries\IncludeLoaders;

use App\Domain\Common\Data\AsyncLoader;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\RequestBodyPagination;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Ensi\PimClient\Dto\SearchProductsResponse;
use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Support\Collection;

class CmsProductsLoader implements AsyncLoader
{
    protected array $productIds = [];
    /* @var Collection<Product> */
    public Collection $products;
    public bool $load = false;

    public function __construct(protected ProductsApi $productsApi)
    {
        $this->products = collect();
    }

    public function setProductIds(Collection $value): void
    {
        $this->productIds = $value->unique()->values()->filter()->all();
    }

    public function requestAsync(): ?PromiseInterface
    {
        if (!$this->load || !$this->productIds) {
            return null;
        }

        $request = new SearchProductsRequest();
        $request->setFilter((object)['id' => $this->productIds]);
        $request->setPagination((new RequestBodyPagination())->setLimit(count($this->productIds)));

        return $this->productsApi->searchProductsAsync($request);
    }

    /**
     * @param SearchProductsResponse $response
     */
    public function processResponse($response)
    {
        $this->products = collect($response->getData())->keyBy(function (Product $product) {
            return $product->getId();
        });
    }
}
