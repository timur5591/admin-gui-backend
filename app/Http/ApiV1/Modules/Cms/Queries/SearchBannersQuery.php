<?php

namespace App\Http\ApiV1\Modules\Cms\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFirstTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CmsClient\Api\BannersApi;
use Ensi\CmsClient\Dto\RequestBodyPagination;
use Ensi\CmsClient\Dto\SearchBannersRequest;
use Ensi\CmsClient\Dto\SearchOneBannersRequest;
use Illuminate\Http\Request;

class SearchBannersQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;
    use QueryBuilderFirstTrait;

    public function __construct(
        Request $httpRequest,
        protected BannersApi $bannersApi,
    ) {
        parent::__construct($httpRequest);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchBannersRequest::class;
    }

    protected function requestFirstClass(): string
    {
        return SearchOneBannersRequest::class;
    }

    protected function search($request)
    {
        return $this->bannersApi->searchBanners($request);
    }

    protected function searchOne($request)
    {
        return $this->bannersApi->searchOneBanners($request);
    }
}
