<?php

namespace App\Http\ApiV1\Modules\Cms\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFilterTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderSortTrait;
use Ensi\CmsClient\Api\ProductCategoriesApi;
use Ensi\CmsClient\Dto\ProductCategoriesTreeRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ProductCategoriesTreeQuery extends QueryBuilder
{
    use QueryBuilderFilterTrait;
    use QueryBuilderSortTrait;

    public function __construct(Request $request, private ProductCategoriesApi $api)
    {
        parent::__construct($request);
    }

    public function get(): Collection
    {
        $request = new ProductCategoriesTreeRequest();
        $this->fillFilters($request);
        $this->fillSort($request);

        return collect($this->api->getProductCategoriesTree($request)->getData());
    }
}
