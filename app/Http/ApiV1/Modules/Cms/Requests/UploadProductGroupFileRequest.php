<?php

namespace App\Http\ApiV1\Modules\Cms\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class UploadProductGroupFileRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'file' => ['required', 'image', 'max:2048'],
        ];
    }
}
