<?php

namespace App\Http\ApiV1\Modules\Cms\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class MassDeleteProductCategoriesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'ids' => ['required', 'array'],
            'ids.*' => ['integer'],
        ];
    }

    /**
     * Возвращает идентификаторы удаляемых сущностей.
     * @return array
     */
    public function getIds(): array
    {
        return $this->validated()['ids'];
    }
}
