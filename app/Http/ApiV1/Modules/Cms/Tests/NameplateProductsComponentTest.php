<?php

use App\Domain\Catalog\Tests\Factories\Products\ProductFactory;
use App\Domain\Cms\Tests\Factories\NameplateProductFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use Ensi\LaravelTestFactories\PromiseFactory;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'cms', 'nameplate');

test('GET /api/v1/cms/nameplates/nameplate-products/{id} success', function () {
    /** @var ApiV1ComponentTestCase $this */
    $nameplateProductId = 1;

    $this->mockCmsNameplateProductsApi()->allows([
        'getNameplateProduct' => NameplateProductFactory::new()->makeResponse(['id' => $nameplateProductId]),
    ]);

    getJson("/api/v1/cms/nameplates/nameplate-products/{$nameplateProductId}")
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'nameplate_id', 'product_id']])
        ->assertJsonPath('data.id', $nameplateProductId);
});

test('POST /api/v1/cms/nameplates/nameplate-products:search include success', function () {
    /** @var ApiV1ComponentTestCase $this */
    $nameplateProductId = 1;
    $count = 2;

    $nameplateProducts = NameplateProductFactory::new()->makeResponseSearch([
        ['id' => $nameplateProductId],
        ['id' => $nameplateProductId + 1],
    ], $count);

    $this->mockCmsNameplateProductsApi()->allows([
        'searchNameplateProducts' => $nameplateProducts,
    ]);

    $this->mockPimProductsApi()->allows([
        'searchProductsAsync' => PromiseFactory::make(
            ProductFactory::new()->makeResponseSearch([['id' => current($nameplateProducts->getData())->getProductId()]])
        ),
    ]);

    $request = [
        'filter' => ['nameplate_id' => 1],
        'include' => ['product'],
    ];

    postJson('/api/v1/cms/nameplates/nameplate-products:search', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'nameplate_id', 'product_id']]])
        ->assertJsonPath('data.0.id', $nameplateProductId);
});

test('GET /api/v1/cms/nameplates/nameplate-products:meta success', function () {
    getJson('/api/v1/cms/nameplates/nameplate-products:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});
