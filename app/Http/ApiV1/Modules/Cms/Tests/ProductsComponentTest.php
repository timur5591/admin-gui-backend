<?php

use App\Http\ApiV1\Modules\Cms\Tests\Factories\RelationIdsRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use Ensi\CmsClient\Dto\EmptyDataResponse;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'cms');

test('POST /api/v1/cms/products/{id}:add-nameplates 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $productId = 1;
    $this->mockCmsProductsApi()->allows([
        'addProductNameplates' => new EmptyDataResponse(),
    ]);

    $request = RelationIdsRequestFactory::new()->make();

    postJson("/api/v1/cms/products/{$productId}:add-nameplates", $request)
        ->assertOk();
});

test('DELETE /api/v1/cms/products/{id}:delete-nameplates 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $productId = 1;
    $this->mockCmsProductsApi()->allows([
        'deleteProductNameplates' => new EmptyDataResponse(),
    ]);

    $request = RelationIdsRequestFactory::new()->make();

    deleteJson("/api/v1/cms/products/{$productId}:delete-nameplates", $request)
        ->assertOk();
});
