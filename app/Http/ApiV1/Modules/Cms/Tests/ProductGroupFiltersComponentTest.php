<?php

use App\Domain\Catalog\Tests\Factories\Categories\PropertyFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class)->group('component', 'cms');

test('POST /api/v1/cms/product-group-filters:search', function () {
    $this->mockPimPropertiesApi()->allows([
        'searchProperties' => PropertyFactory::new()->makeResponseSearch([['id' => 212]]),
    ]);

    postJson('/api/v1/cms/product-group-filters:search', ['filter' => ['category_id' => 10]])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', 212);
});
