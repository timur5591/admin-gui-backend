<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers;

use App\Domain\Contents\Actions\CreateBannersAction;
use App\Domain\Contents\Actions\DeleteBannersAction;
use App\Domain\Contents\Actions\ReplaceBannersAction;
use App\Http\ApiV1\Modules\Cms\Queries\SearchBannersQuery;
use App\Http\ApiV1\Modules\Cms\Requests\CreateBannerRequest;
use App\Http\ApiV1\Modules\Cms\Requests\ReplaceBannerRequest;
use App\Http\ApiV1\Modules\Cms\Resources\BannersResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class BannersController
{
    public function create(CreateBannerRequest $request, CreateBannersAction $action)
    {
        return new BannersResource($action->execute($request->validated()));
    }

    public function replace(int $id, ReplaceBannerRequest $request, ReplaceBannersAction $action)
    {
        return new BannersResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteBannersAction $action)
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function search(SearchBannersQuery $query)
    {
        return BannersResource::collectPage($query->get());
    }

    public function searchOne(SearchBannersQuery $query)
    {
        return BannersResource::make($query->first());
    }
}
