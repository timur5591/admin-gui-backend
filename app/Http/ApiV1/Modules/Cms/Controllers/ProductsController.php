<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers;

use App\Domain\Cms\Actions\Products\AddNameplatesAction;
use App\Domain\Cms\Actions\Products\DeleteNameplatesAction;
use App\Http\ApiV1\Modules\Cms\Requests\AddProductNameplatesRequest;
use App\Http\ApiV1\Modules\Cms\Requests\DeleteProductNameplatesRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class ProductsController
{
    public function addNameplates(int $id, AddProductNameplatesRequest $request, AddNameplatesAction $action): Responsable
    {
        $action->execute($id, $request->getNameplateIds());

        return new EmptyResource();
    }

    public function deleteNameplates(int $id, DeleteProductNameplatesRequest $request, DeleteNameplatesAction $action): Responsable
    {
        $action->execute($id, $request->getNameplateIds());

        return new EmptyResource();
    }
}
