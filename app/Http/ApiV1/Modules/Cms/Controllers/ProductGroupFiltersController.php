<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers;

use App\Http\ApiV1\Modules\Cms\Queries\SearchProductGroupFiltersQuery;
use App\Http\ApiV1\Modules\Cms\Resources\ProductGroupFiltersResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductGroupFiltersController
{
    public function search(SearchProductGroupFiltersQuery $searchFiltersQuery): AnonymousResourceCollection
    {
        return ProductGroupFiltersResource::collectPage($searchFiltersQuery->get());
    }
}
