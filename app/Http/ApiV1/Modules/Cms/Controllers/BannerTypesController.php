<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers;

use App\Http\ApiV1\Modules\Cms\Queries\SearchBannerTypesQuery;
use App\Http\ApiV1\Modules\Cms\Resources\BannerTypesResource;

class BannerTypesController
{
    public function search(SearchBannerTypesQuery $query)
    {
        return BannerTypesResource::collectPage($query->get());
    }
}
