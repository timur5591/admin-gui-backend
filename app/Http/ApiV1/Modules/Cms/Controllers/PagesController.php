<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers;

use App\Domain\Common\Data\Meta\Field;
use App\Domain\Contents\Actions\Pages\CreatePageAction;
use App\Domain\Contents\Actions\Pages\DeletePageAction;
use App\Domain\Contents\Actions\Pages\PatchPageAction;
use App\Http\ApiV1\Modules\Cms\Queries\SearchPagesQuery;
use App\Http\ApiV1\Modules\Cms\Requests\CreatePageRequest;
use App\Http\ApiV1\Modules\Cms\Requests\PatchPageRequest;
use App\Http\ApiV1\Modules\Cms\Resources\PagesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Ensi\CmsClient\ApiException;
use Exception;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PagesController
{
    public function get(int $id, SearchPagesQuery $query): PagesResource
    {
        return new PagesResource($query->find($id));
    }

    /**
     * @throws ApiException
     */
    public function create(CreatePageRequest $request, CreatePageAction $action): PagesResource
    {
        return new PagesResource($action->execute($request->validated()));
    }

    /**
     * @throws ApiException
     */
    public function patch(int $id, PatchPageRequest $request, PatchPageAction $action): PagesResource
    {
        return new PagesResource($action->execute($id, $request->validated()));
    }

    /**
     * @throws ApiException
     */
    public function delete(int $id, DeletePageAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function search(SearchPagesQuery $query): AnonymousResourceCollection
    {
        return PagesResource::collectPage($query->get());
    }

    public function searchOne(SearchPagesQuery $query): PagesResource
    {
        return PagesResource::make($query->first());
    }

    /**
     * @throws Exception
     */
    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->sortDefault(direction: 'desc')->detailLink(),
            Field::text('name', 'Наименование')->listDefault()->filterDefault()->sort(),
            Field::boolean('is_active', 'Активность')->listDefault()->sort()->filter(),
            Field::string('slug', 'Ссылка')->listDefault()->filter(),

            Field::datetime('active_from', 'Начало публикации')->listDefault(),
            Field::datetime('active_to', 'Окончание публикации')->listDefault(),

            Field::datetime('created_at', 'Дата создания')->listDefault(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault(),
        ]);
    }
}
