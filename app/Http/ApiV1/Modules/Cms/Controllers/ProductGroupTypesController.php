<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers;

use App\Http\ApiV1\Modules\Cms\Queries\SearchProductGroupTypesQuery;
use App\Http\ApiV1\Modules\Cms\Resources\ProductGroupTypesResource;

class ProductGroupTypesController
{
    public function search(SearchProductGroupTypesQuery $query)
    {
        return ProductGroupTypesResource::collectPage($query->get());
    }
}
