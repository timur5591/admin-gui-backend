<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers;

use App\Domain\Contents\Actions\CreateProductGroupsAction;
use App\Domain\Contents\Actions\DeleteProductGroupsAction;
use App\Domain\Contents\Actions\ReplaceProductGroupsAction;
use App\Http\ApiV1\Modules\Cms\Queries\SearchProductGroupsQuery;
use App\Http\ApiV1\Modules\Cms\Requests\CreateProductGroupRequest;
use App\Http\ApiV1\Modules\Cms\Requests\ReplaceProductGroupRequest;
use App\Http\ApiV1\Modules\Cms\Resources\ProductGroupsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class ProductGroupsController
{
    public function create(CreateProductGroupRequest $request, CreateProductGroupsAction $action)
    {
        return new ProductGroupsResource($action->execute($request->validated()));
    }

    public function replace(int $id, ReplaceProductGroupRequest $request, ReplaceProductGroupsAction $action)
    {
        return new ProductGroupsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteProductGroupsAction $action)
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function search(SearchProductGroupsQuery $query)
    {
        return ProductGroupsResource::collectPage($query->get());
    }

    public function searchOne(SearchProductGroupsQuery $query)
    {
        return ProductGroupsResource::make($query->first());
    }
}
