<?php

namespace App\Http\ApiV1\Modules\Marketing\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\MarketingClient\Dto\PromoCodeStatusEnum;
use Illuminate\Validation\Rule;

class CreatePromoCodeRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'code' => ['required', 'string'],
            'counter' => ['nullable', 'integer', 'gt:0'],
            'start_date' => ['nullable', 'date'],
            'end_date' => ['nullable', 'date', 'after_or_equal:start_date'],
            'status' => ['required', 'integer', Rule::in(PromoCodeStatusEnum::getAllowableEnumValues())],
            'discount_id' => ['required', 'integer', 'min:1'],
        ];
    }
}
