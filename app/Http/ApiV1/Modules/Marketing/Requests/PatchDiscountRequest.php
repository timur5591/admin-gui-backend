<?php

namespace App\Http\ApiV1\Modules\Marketing\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\MarketingClient\Dto\DiscountStatusEnum;
use Ensi\MarketingClient\Dto\DiscountValueTypeEnum;
use Illuminate\Validation\Rule;

class PatchDiscountRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['string'],
            'value_type' => ['integer', Rule::in(DiscountValueTypeEnum::getAllowableEnumValues())],
            'value' => ['integer', 'min:1'],
            'status' => ['integer', Rule::in(DiscountStatusEnum::getAllowableEnumValues())],
            'start_date' => ['nullable', 'date'],
            'end_date' => ['nullable', 'date', 'after_or_equal:start_date'],
            'promo_code_only' => ['boolean'],
        ];
    }
}
