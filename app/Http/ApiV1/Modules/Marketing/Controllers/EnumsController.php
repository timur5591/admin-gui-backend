<?php

namespace App\Http\ApiV1\Modules\Marketing\Controllers;

use App\Http\ApiV1\Support\Controllers\Data\EnumData;
use App\Http\ApiV1\Support\Resources\EnumResource;
use Ensi\MarketingClient\Dto\DiscountStatusEnum;
use Ensi\MarketingClient\Dto\DiscountTypeEnum;
use Ensi\MarketingClient\Dto\DiscountValueTypeEnum;
use Ensi\MarketingClient\Dto\PromoCodeStatusEnum;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class EnumsController
{
    public function discountStatuses(): AnonymousResourceCollection
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(DiscountStatusEnum::getDescriptions()));
    }

    public function discountTypes(): AnonymousResourceCollection
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(DiscountTypeEnum::getDescriptions()));
    }

    public function discountValueTypes(): AnonymousResourceCollection
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(DiscountValueTypeEnum::getDescriptions()));
    }

    public function promoCodeStatuses(): AnonymousResourceCollection
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(PromoCodeStatusEnum::getDescriptions()));
    }
}
