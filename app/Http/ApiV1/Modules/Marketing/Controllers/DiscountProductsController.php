<?php

namespace App\Http\ApiV1\Modules\Marketing\Controllers;

use App\Domain\Common\Data\Meta\Enum\Catalog\ProductDraftEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Marketing\Queries\DiscountProductsQuery;
use App\Http\ApiV1\Modules\Marketing\Resources\DiscountProductsResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Exception;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class DiscountProductsController
{
    public function get(int $id, DiscountProductsQuery $query): DiscountProductsResource
    {
        return new DiscountProductsResource($query->find($id));
    }

    public function search(DiscountProductsQuery $query): AnonymousResourceCollection
    {
        return DiscountProductsResource::collectPage($query->get());
    }

    /**
     * @throws Exception
     */
    public function meta(
        ProductDraftEnumInfo $productDraftEnumInfo,
    ): ModelMetaResource {
        return new ModelMetaResource([
            Field::id()->filter()->listDefault()->filterDefault()->detailLink(),

            Field::enum('product_id', 'Название продукта', $productDraftEnumInfo)->listHide(),

            Field::integer('product.id', 'ID продукта')->object('product')->listDefault(),
            Field::string('product.name', 'Название продукта')->object('product')->listDefault(),
            Field::photo('product.main_image_file', 'Изображение')->object('product')->listDefault(),
            Field::string('product.vendor_code', 'Артикул')->object('product')->listDefault(),
            Field::string('product.barcode', 'Штрихкод (EAN)')->object('product')->listDefault(),

            Field::datetime('created_at', 'Дата создания'),
            Field::datetime('updated_at', 'Дата обновления'),
        ]);
    }
}
