<?php

namespace App\Http\ApiV1\Modules\Marketing\Queries;

use App\Domain\Common\Actions\AsyncLoadAction;
use App\Domain\Marketing\Data\Discounts\DiscountProductData;
use App\Http\ApiV1\Modules\Marketing\Queries\IncludeLoaders\MarketingProductsLoader;
use Ensi\MarketingClient\Api\DiscountProductsApi;
use Ensi\MarketingClient\ApiException;
use Ensi\MarketingClient\Dto\DiscountProduct;
use Ensi\MarketingClient\Dto\DiscountProductResponse;
use Ensi\MarketingClient\Dto\SearchDiscountProductsRequest;
use Ensi\MarketingClient\Dto\SearchDiscountProductsResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class DiscountProductsQuery extends MarketingQuery
{
    public function __construct(
        protected AsyncLoadAction $asyncLoadAction,
        protected MarketingProductsLoader $productsLoader,
        protected DiscountProductsApi $api,
        Request $request,
    ) {
        parent::__construct($request, SearchDiscountProductsRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchDiscountProductsResponse
    {
        return $this->api->searchDiscountProducts($request);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id): DiscountProductResponse
    {
        return $this->api->getDiscountProduct($id);
    }

    protected function convertFindToItem($response): DiscountProductData
    {
        return $this->convertArray([$response->getData()])->first();
    }

    protected function convertGetToItems($response): Collection
    {
        return $this->convertArray($response->getData());
    }

    protected function convertArray(array $array): Collection
    {
        /** @var Collection<DiscountProduct> $orders */
        $discountProducts = collect($array);

        $this->productsLoader->setProductIds($discountProducts->pluck('product_id'));
        $this->asyncLoadAction->execute([$this->productsLoader]);

        return DiscountProductData::mapCollect($discountProducts, $this->productsLoader->products);
    }

    protected function getHttpInclude(): array
    {
        $httpIncludes = parent::getHttpInclude();

        $includes = [];
        foreach ($httpIncludes as $include) {
            switch ($include) {
                case "product":
                    $this->productsLoader->load = true;

                    break;
                default:
                    $includes[] = $include;
            }
        }

        return array_values(array_unique($includes));
    }
}
