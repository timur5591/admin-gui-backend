<?php

namespace App\Http\ApiV1\Modules\Marketing\Queries\IncludeLoaders;

use App\Domain\Common\Data\AsyncLoader;
use Illuminate\Support\Collection;

abstract class MarketingLoader implements AsyncLoader
{
    protected function prepareFilterValues(Collection $values): array
    {
        return $values->unique()->values()->filter()->all();
    }
}
