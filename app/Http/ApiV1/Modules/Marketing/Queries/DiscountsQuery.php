<?php

namespace App\Http\ApiV1\Modules\Marketing\Queries;

use App\Domain\Common\Actions\AsyncLoadAction;
use App\Domain\Marketing\Data\Discounts\DiscountData;
use App\Http\ApiV1\Modules\Marketing\Queries\IncludeLoaders\MarketingProductsLoader;
use Ensi\MarketingClient\Api\DiscountsApi;
use Ensi\MarketingClient\ApiException;
use Ensi\MarketingClient\Dto\Discount;
use Ensi\MarketingClient\Dto\DiscountResponse;
use Ensi\MarketingClient\Dto\SearchDiscountsRequest;
use Ensi\MarketingClient\Dto\SearchDiscountsResponse;
use Ensi\PimClient\Dto\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class DiscountsQuery extends MarketingQuery
{
    /* @var Collection<Product> */
    protected Collection $products;

    public function __construct(
        protected AsyncLoadAction $asyncLoadAction,
        protected MarketingProductsLoader $productsLoader,
        protected DiscountsApi $api,
        Request $request,
    ) {
        parent::__construct($request, SearchDiscountsRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchDiscountsResponse
    {
        return $this->api->searchDiscounts($request);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id): DiscountResponse
    {
        return $this->api->getDiscount($id, $this->getInclude());
    }

    protected function convertFindToItem($response): DiscountData
    {
        return $this->convertArray([$response->getData()])->first();
    }

    protected function convertGetToItems($response): Collection
    {
        return $this->convertArray($response->getData());
    }

    protected function convertArray(array $array): Collection
    {
        /** @var Collection<Discount> $discounts */
        $discounts = collect($array);

        $this->productsLoader->setProductIds($this->getProducts($discounts)->pluck('product_id'));
        $this->asyncLoadAction->execute([$this->productsLoader]);

        return DiscountData::mapCollect($discounts, $this->productsLoader->products);
    }

    protected function getHttpInclude(): array
    {
        $httpIncludes = parent::getHttpInclude();

        $includes = [];
        foreach ($httpIncludes as $include) {
            switch ($include) {
                case "products.product":
                    $this->productsLoader->load = true;

                    break;
                default:
                    $includes[] = $include;
            }
        }

        return array_values(array_unique($includes));
    }

    protected function getProducts(Collection $discounts): Collection
    {
        return $discounts
            ->pluck('products')
            ->collapse();
    }
}
