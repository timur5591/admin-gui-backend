<?php

namespace App\Http\ApiV1\Modules\Marketing\Queries;

use App\Domain\Common\Actions\AsyncLoadAction;
use App\Domain\Marketing\Data\Discounts\PromoCodeData;
use App\Http\ApiV1\Modules\Marketing\Queries\IncludeLoaders\MarketingProductsLoader;
use Ensi\MarketingClient\Api\PromoCodesApi;
use Ensi\MarketingClient\ApiException;
use Ensi\MarketingClient\Dto\PromoCode;
use Ensi\MarketingClient\Dto\PromoCodeResponse;
use Ensi\MarketingClient\Dto\SearchPromoCodesRequest;
use Ensi\MarketingClient\Dto\SearchPromoCodesResponse;
use Ensi\PimClient\Dto\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class PromoCodesQuery extends MarketingQuery
{
    /* @var Collection<Product> */
    protected Collection $products;

    public function __construct(
        protected AsyncLoadAction $asyncLoadAction,
        protected MarketingProductsLoader $productsLoader,
        protected PromoCodesApi $api,
        Request $request,
    ) {
        parent::__construct($request, SearchPromoCodesRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchPromoCodesResponse
    {
        return $this->api->searchPromoCodes($request);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id): PromoCodeResponse
    {
        return $this->api->getPromoCode($id);
    }

    protected function convertFindToItem($response): PromoCodeData
    {
        return $this->convertArray([$response->getData()])->first();
    }

    protected function convertGetToItems($response): Collection
    {
        return $this->convertArray($response->getData());
    }

    protected function convertArray(array $array): Collection
    {
        /** @var Collection<PromoCode> $promoCodes */
        $promoCodes = collect($array);

        $this->productsLoader->setProductIds($this->getProducts($promoCodes)->pluck('product_id'));
        $this->asyncLoadAction->execute([$this->productsLoader]);

        return PromoCodeData::mapCollect($promoCodes, $this->productsLoader->products);
    }

    protected function getHttpInclude(): array
    {
        $httpIncludes = parent::getHttpInclude();

        $includes = [];
        foreach ($httpIncludes as $include) {
            switch ($include) {
                case "discount.products.product":
                    $this->productsLoader->load = true;

                    break;
                default:
                    $includes[] = $include;
            }
        }

        return array_values(array_unique($includes));
    }

    protected function getProducts(Collection $promoCodes): Collection
    {
        return $promoCodes
            ->pluck('discount')
            ->pluck('products')
            ->collapse();
    }
}
