<?php

use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\MarketingClient\Dto\DiscountStatusEnum;
use Ensi\MarketingClient\Dto\DiscountTypeEnum;
use Ensi\MarketingClient\Dto\DiscountValueTypeEnum;
use Ensi\MarketingClient\Dto\PromoCodeStatusEnum;

use function Pest\Laravel\getJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'marketing', 'enums');

test('GET /api/v1/marketing/discount-statuses 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => DiscountStatusEnum::ACTIVE,
        'name' => DiscountStatusEnum::getDescriptions()[DiscountStatusEnum::ACTIVE],
    ];

    getJson('/api/v1/marketing/discount-statuses')
        ->assertOk()
        ->assertJsonFragment($item);
});

test('GET /api/v1/marketing/discount-types 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => DiscountTypeEnum::OFFER,
        'name' => DiscountTypeEnum::getDescriptions()[DiscountTypeEnum::OFFER],
    ];

    getJson('/api/v1/marketing/discount-types')
        ->assertOk()
        ->assertJsonFragment($item);
});

test('GET /api/v1/marketing/discount-value-types 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => DiscountValueTypeEnum::PERCENT,
        'name' => DiscountValueTypeEnum::getDescriptions()[DiscountValueTypeEnum::PERCENT],
    ];

    getJson('/api/v1/marketing/discount-value-types')
        ->assertOk()
        ->assertJsonFragment($item);
});

test('GET /api/v1/marketing/promo-code-statuses 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => PromoCodeStatusEnum::PAUSED,
        'name' => PromoCodeStatusEnum::getDescriptions()[PromoCodeStatusEnum::PAUSED],
    ];

    getJson('/api/v1/marketing/promo-code-statuses')
        ->assertOk()
        ->assertJsonFragment($item);
});
