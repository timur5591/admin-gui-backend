<?php

namespace App\Http\ApiV1\Modules\Marketing\Tests\Factories;

use Ensi\MarketingClient\Dto\DiscountTypeEnum;

class CreateDiscountRequestFactory extends PatchDiscountRequestFactory
{
    protected function definition(): array
    {
        /** @var DiscountTypeEnum $type */
        $type = $this->faker->randomElement(DiscountTypeEnum::getAllowableEnumValues());
        $products = $type == DiscountTypeEnum::OFFER ? $this->makeProducts() : null;

        $definition = [
            'type' => $type,
        ];
        if ($products) {
            $definition['products'] = $products;
        }

        return array_merge($definition, parent::definition());
    }

    protected function makeProducts(): array
    {
        return $this->faker->randomList(fn () => ['product_id' => $this->faker->modelId()], 1);
    }
}
