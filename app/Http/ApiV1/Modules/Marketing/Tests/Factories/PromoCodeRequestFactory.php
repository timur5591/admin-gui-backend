<?php

namespace App\Http\ApiV1\Modules\Marketing\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\MarketingClient\Dto\PromoCodeStatusEnum;

class PromoCodeRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $startData = $this->faker->nullable()->date();

        return [
            'name' => $this->faker->text(50),
            'code' => $this->faker->text(50),
            'counter' => $this->faker->nullable()->numberBetween(1),
            'start_date' => $startData,
            'end_date' => $startData ?
                $this->faker->nullable()->dateTimeBetween($startData, '+5 days')?->format('Y-m-d') :
                $this->faker->nullable()->date(),
            'status' => $this->faker->randomElement(PromoCodeStatusEnum::getAllowableEnumValues()),
            'discount_id' => $this->faker->modelId(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
