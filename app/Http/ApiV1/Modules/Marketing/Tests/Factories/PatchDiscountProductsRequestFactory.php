<?php

namespace App\Http\ApiV1\Modules\Marketing\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class PatchDiscountProductsRequestFactory extends BaseApiFactory
{
    protected array $products = [];

    protected function definition(): array
    {
        return [
            'products' => $this->makeProducts(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function fillProductIds(array $ids): self
    {
        $products = array_map(fn ($id) => ['product_id' => $id], $ids);
        $this->products = array_merge($this->products, $products);

        return $this;
    }

    protected function makeProducts(): array
    {
        return $this->products ?: $this->faker->randomList(fn () => ['product_id' => $this->faker->modelId()], 1);
    }
}
