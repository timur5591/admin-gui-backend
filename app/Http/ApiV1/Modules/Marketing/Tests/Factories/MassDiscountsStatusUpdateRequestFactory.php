<?php

namespace App\Http\ApiV1\Modules\Marketing\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\MarketingClient\Dto\DiscountStatusEnum;

class MassDiscountsStatusUpdateRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'ids' => $this->faker->randomList(fn () => $this->faker->modelId(), 1),
            'status' => $this->faker->randomElement(DiscountStatusEnum::getAllowableEnumValues()),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
