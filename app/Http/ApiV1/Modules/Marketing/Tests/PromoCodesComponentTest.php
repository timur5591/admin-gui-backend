<?php

use App\Domain\Marketing\Tests\Factories\PromoCodeFactory;
use App\Http\ApiV1\Modules\Marketing\Tests\Factories\PromoCodeRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\MarketingClient\Dto\EmptyDataResponse;
use Ensi\TestFactories\FakerProvider;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');


test('POST /api/v1/marketing/promo-codes success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $promoCodeId = 1;

    $this->mockMarketingPromoCodesApi()->allows([
        'createPromoCode' => PromoCodeFactory::new()->makeResponse(['id' => $promoCodeId]),
    ]);
    $request = PromoCodeRequestFactory::new()->make();

    postJson("/api/v1/marketing/promo-codes", $request)
        ->assertOk()
        ->assertJsonPath('data.id', $promoCodeId);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/marketing/promo-codes/{id} success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $promoCodeId = 1;

    $this->mockMarketingPromoCodesApi()->allows([
        'patchPromoCode' => PromoCodeFactory::new()->makeResponse(['id' => $promoCodeId]),
    ]);
    $request = PromoCodeRequestFactory::new()->make();

    patchJson("/api/v1/marketing/promo-codes/{$promoCodeId}", $request)
        ->assertOk()
        ->assertJsonPath('data.id', $promoCodeId);
})->with(FakerProvider::$optionalDataset);

test('DELETE /api/v1/marketing/promo-codes/{id} success', function () {
    /** @var ApiV1ComponentTestCase $this */
    $promoCodeId = 1;

    $this->mockMarketingPromoCodesApi()->allows([
        'deletePromoCode' => new EmptyDataResponse(),
    ]);

    deleteJson("/api/v1/marketing/promo-codes/{$promoCodeId}")
        ->assertOk();
});

test('GET /api/v1/marketing/promo-codes/{id} success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $promoCodeId = 1;

    $this->mockMarketingPromoCodesApi()->allows([
        'getPromoCode' => PromoCodeFactory::new()->makeResponse(['id' => $promoCodeId]),
    ]);

    getJson("/api/v1/marketing/promo-codes/{$promoCodeId}")
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'discount_id', 'code', 'status']])
        ->assertJsonPath('data.id', $promoCodeId);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/marketing/promo-codes:search success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $promoCodeId = 1;
    $count = 2;

    $this->mockMarketingPromoCodesApi()->allows([
        'searchPromoCodes' => PromoCodeFactory::new()->makeResponseSearch([['id' => $promoCodeId]], $count),
    ]);

    $request = [
        'filter' => ['name_like' => 'промокод'],
    ];

    postJson('/api/v1/marketing/promo-codes:search', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'discount_id', 'code', 'status']]])
        ->assertJsonPath('data.0.id', $promoCodeId);
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/marketing/promo-codes:meta success', function () {
    getJson('/api/v1/marketing/promo-codes:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});
