<?php

use App\Domain\Catalog\Tests\Factories\Products\ProductFactory;
use App\Domain\Marketing\Tests\Factories\DiscountFactory;
use App\Domain\Marketing\Tests\Factories\DiscountProductFactory;
use App\Http\ApiV1\Modules\Marketing\Tests\Factories\CreateDiscountRequestFactory;
use App\Http\ApiV1\Modules\Marketing\Tests\Factories\MassDiscountsStatusUpdateRequestFactory;
use App\Http\ApiV1\Modules\Marketing\Tests\Factories\PatchDiscountRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\PromiseFactory;
use Ensi\MarketingClient\Dto\EmptyDataResponse;
use Ensi\TestFactories\FakerProvider;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'marketing', 'discount');

test('POST /api/v1/marketing/discounts success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $discountId = 1;

    $this->mockMarketingDiscountsApi()->allows([
        'createDiscount' => DiscountFactory::new()->makeResponse(['id' => $discountId]),
    ]);
    $request = CreateDiscountRequestFactory::new()->make();

    postJson("/api/v1/marketing/discounts", $request)
        ->assertOk()
        ->assertJsonPath('data.id', $discountId);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/marketing/discounts/{id} success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $discountId = 1;

    $this->mockMarketingDiscountsApi()->allows([
        'patchDiscount' => DiscountFactory::new()->makeResponse(['id' => $discountId]),
    ]);
    $request = PatchDiscountRequestFactory::new()->make();

    patchJson("/api/v1/marketing/discounts/{$discountId}", $request)
        ->assertOk()
        ->assertJsonPath('data.id', $discountId);
})->with(FakerProvider::$optionalDataset);

test('DELETE /api/v1/marketing/discounts/{id} success', function () {
    /** @var ApiV1ComponentTestCase $this */
    $discountId = 1;

    $this->mockMarketingDiscountsApi()->allows([
        'deleteDiscount' => new EmptyDataResponse(),
    ]);

    deleteJson("/api/v1/marketing/discounts/{$discountId}")
        ->assertOk();
});

test('GET /api/v1/marketing/discounts/{id} success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $discountId = 1;

    $this->mockMarketingDiscountsApi()->allows([
        'getDiscount' => DiscountFactory::new()->makeResponse(['id' => $discountId]),
    ]);

    getJson("/api/v1/marketing/discounts/{$discountId}")
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'type', 'value_type', 'value', 'status']])
        ->assertJsonPath('data.id', $discountId);
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/marketing/discounts/{id} success include products', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $discountId = 1;

    $discountProduct = DiscountProductFactory::new()->make();

    $this->mockMarketingDiscountsApi()->allows([
        'getDiscount' => DiscountFactory::new()->withProducts($discountProduct)->makeResponse(['id' => $discountId]),
    ]);

    $this->mockPimProductsApi()->allows([
        'searchProductsAsync' => PromiseFactory::make(
            ProductFactory::new()->makeResponseSearch([['id' => $discountProduct->getProductId()]])
        ),
    ]);

    getJson("/api/v1/marketing/discounts/{$discountId}?include=products")
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'type', 'value_type', 'value', 'status', 'products' => [['discount_id', 'product_id']]]])
        ->assertJsonPath('data.id', $discountId);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/marketing/discounts:search success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $discountId = 1;

    $this->mockMarketingDiscountsApi()->allows([
        'searchDiscounts' => DiscountFactory::new()->makeResponseSearch([
            ['id' => $discountId],
            ['id' => $discountId + 1],
        ], 2),
    ]);

    $request = [
        'filter' => ['name_like' => 'скидка'],
    ];

    postJson('/api/v1/marketing/discounts:search', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'type', 'value_type', 'value', 'status']]])
        ->assertJsonPath('data.0.id', $discountId);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/marketing/discounts:mass-status-update success', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockMarketingDiscountsApi()->allows([
        'massDiscountsStatusUpdate' => new EmptyDataResponse(),
    ]);

    $request = MassDiscountsStatusUpdateRequestFactory::new()->make();

    postJson('/api/v1/marketing/discounts:mass-status-update', $request)
        ->assertOk();
});

test('GET /api/v1/marketing/discounts:meta success', function () {
    getJson('/api/v1/marketing/discounts:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});
