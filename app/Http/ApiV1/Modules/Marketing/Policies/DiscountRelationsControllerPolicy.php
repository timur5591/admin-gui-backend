<?php

namespace App\Http\ApiV1\Modules\Marketing\Policies;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class DiscountRelationsControllerPolicy
{
    use HandlesAuthorization;

    public function patchProducts(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DISCOUNT_DETAIL_EDIT,
        ]);
    }

    public function deleteProducts(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DISCOUNT_DETAIL_EDIT,
        ]);
    }
}
