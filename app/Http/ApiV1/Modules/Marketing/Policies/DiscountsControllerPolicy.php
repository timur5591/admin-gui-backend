<?php

namespace App\Http\ApiV1\Modules\Marketing\Policies;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class DiscountsControllerPolicy
{
    use HandlesAuthorization;

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DISCOUNT_LIST_READ,
            RightsAccessEnum::DISCOUNT_DETAIL_READ,
        ]);
    }

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DISCOUNT_DETAIL_READ,
        ]);
    }

    public function create(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DISCOUNT_CREATE,
        ]);
    }

    public function patch(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DISCOUNT_DETAIL_EDIT,
        ]);
    }

    public function delete(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DISCOUNT_DELETE,
        ]);
    }

    public function massStatusUpdate(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DISCOUNT_MASS_STATUS_UPDATE,
        ]);
    }
}
