<?php

namespace App\Http\ApiV1\Modules\Orders\Queries\Orders\IncludeLoaders;

use App\Domain\Common\Data\AsyncLoader;
use Ensi\BuClient\Api\StoresApi;
use Ensi\BuClient\Dto\PaginationTypeEnum;
use Ensi\BuClient\Dto\RequestBodyPagination;
use Ensi\BuClient\Dto\SearchStoresRequest;
use Ensi\BuClient\Dto\Store;
use GuzzleHttp\Promise\PromiseInterface;

class OrderStoresLoader implements AsyncLoader
{
    /** @var Store[] */
    public array $stores = [];
    public array $storeIds = [];
    public bool $load = false;

    public function __construct(protected StoresApi $storesApi)
    {
    }

    public function requestAsync(): ?PromiseInterface
    {
        if (!$this->load || !$this->storeIds) {
            return null;
        }

        $request = new SearchStoresRequest();
        $request->setFilter((object)['id' => $this->storeIds,]);
        $request->setPagination(
            (new RequestBodyPagination())
                ->setLimit(count($this->storeIds))
                ->setType(PaginationTypeEnum::CURSOR)
        );

        return $this->storesApi->searchStoresAsync($request);
    }

    public function processResponse($response)
    {
        $this->stores = $response->getData();
    }
}
