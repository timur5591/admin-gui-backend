<?php

namespace App\Http\ApiV1\Modules\Orders\Controllers\Orders;

use App\Http\ApiV1\Support\Controllers\Data\EnumData;
use App\Http\ApiV1\Support\Resources\EnumResource;
use Ensi\OmsClient\Dto\DeliveryStatusEnum;
use Ensi\OmsClient\Dto\OrderSourceEnum;
use Ensi\OmsClient\Dto\OrderStatusEnum;
use Ensi\OmsClient\Dto\PaymentMethodEnum;
use Ensi\OmsClient\Dto\PaymentStatusEnum;
use Ensi\OmsClient\Dto\ShipmentStatusEnum;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class EnumsController
{
    public function orderStatuses(): AnonymousResourceCollection
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(OrderStatusEnum::getDescriptions()));
    }

    public function orderSources(): AnonymousResourceCollection
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(OrderSourceEnum::getDescriptions()));
    }

    public function paymentMethods(): AnonymousResourceCollection
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(PaymentMethodEnum::getDescriptions()));
    }

    public function paymentStatuses(): AnonymousResourceCollection
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(PaymentStatusEnum::getDescriptions()));
    }

    public function deliveryStatuses(): AnonymousResourceCollection
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(DeliveryStatusEnum::getDescriptions()));
    }

    public function shipmentStatuses(): AnonymousResourceCollection
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(ShipmentStatusEnum::getDescriptions()));
    }
}
