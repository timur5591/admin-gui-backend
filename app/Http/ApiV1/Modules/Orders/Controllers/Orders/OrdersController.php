<?php

namespace App\Http\ApiV1\Modules\Orders\Controllers\Orders;

use App\Domain\Common\Data\Meta\Enum\Customers\CustomerUserEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Logistic\DeliveryMethodEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Logistic\DeliveryServiceEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Logistic\LogisticPointEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Logistic\LogisticTariffEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Orders\OrderSourceEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Orders\OrderStatusEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Orders\PaymentMethodEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Orders\PaymentStatusEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Orders\PaymentSystemEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Units\AdminUserEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Domain\Orders\Actions\Orders\AttachOrderFileAction;
use App\Domain\Orders\Actions\Orders\ChangeOrderDeliveryAction;
use App\Domain\Orders\Actions\Orders\ChangeOrderPaymentSystemAction;
use App\Domain\Orders\Actions\Orders\DeleteOrderFilesAction;
use App\Domain\Orders\Actions\Orders\PatchOrderAction;
use App\Http\ApiV1\Modules\Orders\Queries\Orders\OrdersQuery;
use App\Http\ApiV1\Modules\Orders\Requests\Orders\AttachOrderFileRequest;
use App\Http\ApiV1\Modules\Orders\Requests\Orders\ChangeOrderDeliveryRequest;
use App\Http\ApiV1\Modules\Orders\Requests\Orders\ChangeOrderPaymentSystemRequest;
use App\Http\ApiV1\Modules\Orders\Requests\Orders\DeleteOrderFilesRequest;
use App\Http\ApiV1\Modules\Orders\Requests\Orders\PatchOrderRequest;
use App\Http\ApiV1\Modules\Orders\Resources\Orders\OrderFilesResource;
use App\Http\ApiV1\Modules\Orders\Resources\Orders\OrdersResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;

class OrdersController
{
    public function search(OrdersQuery $query)
    {
        return OrdersResource::collectPage($query->get());
    }

    public function meta(
        CustomerUserEnumInfo    $customer,
        AdminUserEnumInfo       $adminUser,
        OrderSourceEnumInfo     $orderSource,
        OrderStatusEnumInfo     $orderStatus,
        PaymentStatusEnumInfo   $orderPaymentStatus,
        PaymentMethodEnumInfo   $orderPaymentMethod,
        PaymentSystemEnumInfo   $orderPaymentSystem,
        DeliveryMethodEnumInfo  $deliveryMethod,
        DeliveryServiceEnumInfo $deliveryService,
        LogisticTariffEnumInfo  $logisticTariff,
        LogisticPointEnumInfo   $unitsPvz,
    ) {
        // todo поля скрыты в рамках задачи #90291
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault(),
            Field::text('number', '№ заказа')->sort()->detailLink()->filterDefault(),
            Field::enum('source', 'Источник', $orderSource)->filterDefault(),
            Field::enum('customer_id', 'Покупатель', $customer)->listDefault()->filterDefault(),
//            (new EnumField('responsible_id', 'Ответственный', $adminUser))->listDefault()->filterDefault(),
//            (new EmailField('customer_email', 'Почта покупателя'))->sort()->filter('customer_email_like'),
//            new PriceField('cost', 'Сумма до скидок'),
            Field::price('price', 'Сумма')->listDefault()->filterDefault(),
//            new IntField('spent_bonus', 'Списано баллов'),
//            new IntField('added_bonus', 'Добавлено баллов'),
//            (new StringField('promo_code', 'Промокод'))->sort(),
            Field::enum('delivery_method', 'Метод доставки', $deliveryMethod),
//            new EnumField('delivery_service', 'Служба доставки', $deliveryService),
//            new EnumField('delivery_tariff_id', 'Тариф доставки', $logisticTariff),
//            new EnumField('delivery_point_id', 'ПВЗ', $unitsPvz),
            Field::string('delivery_address.address_string', 'Адрес доставки')->object(),
//            new PriceField('delivery_price', 'Цена доставки'),
//            new PriceField('delivery_cost', 'Цена доставки до скидок'),
            Field::text('receiver_name', 'ФИО получателя')->sort(),
            Field::phone('receiver_phone', 'Телефон получателя')->sort(),
            Field::email('receiver_email', 'Почта получателя')->sort(),
            Field::enum('status', 'Статус', $orderStatus)->listDefault()->filterDefault(),
            Field::datetime('status_at', 'Дата изменения статуса'),
            Field::enum('payment_status', 'Статус оплаты', $orderPaymentStatus),
            Field::datetime('payment_status_at', 'Дата изменения статуса оплаты'),
            Field::datetime('payed_at', 'Дата оплаты'),
            Field::datetime('payment_expires_at', 'Дата просрочки оплаты'),
            Field::enum('payment_method', 'Метод оплаты', $orderPaymentMethod),
            Field::enum('payment_system', 'Система оплаты', $orderPaymentSystem),
            // new UrlField('payment_link', 'Ссылка на оплату'),

            Field::text('payment_external_id', 'ID оплаты во внешней системе')->sort(),
            Field::boolean('is_expired', 'Заказ просрочен')->listDefault()->filterDefault(),
            Field::datetime('is_expired_at', 'Дата, когда заказ был просрочен'),
            Field::boolean('is_return', 'Заказ возвращен')->listDefault()->filterDefault(),
            Field::datetime('is_return_at', 'Дата изменения признака "Заказ возвращен"'),
            Field::boolean('is_partial_return', 'Заказ возвращен частично')->listDefault()->filterDefault(),
            Field::datetime('is_partial_return_at', 'Дата изменения признака "Заказ возвращен частично"'),
            Field::boolean('is_problem', 'Заказ проблемный')->listDefault()->filterDefault(),
            Field::datetime('is_problem_at', 'Дата изменения признака "Заказ проблемный"'),
            Field::text('problem_comment', 'Комментарий о проблеме'),
            Field::datetime('created_at', 'Дата создания')->listDefault()->filterDefault(),
            Field::datetime('updated_at', 'Дата обновления'),
            Field::text('delivery_comment', 'Комментарий к доставке'),
            Field::text('client_comment', 'Комментарий клиента'),
        ]);
    }

    public function get(int $id, OrdersQuery $ordersQuery)
    {
        return new OrdersResource($ordersQuery->find($id));
    }

    public function patch(int $id, PatchOrderAction $action, PatchOrderRequest $request)
    {
        return new OrdersResource($action->execute($id, $request->validated()));
    }

    public function changePaymentSystem(int $id, ChangeOrderPaymentSystemRequest $request, ChangeOrderPaymentSystemAction $action)
    {
        return new OrdersResource($action->execute($id, $request->getPaymentSystem()));
    }

    public function changeDelivery(int $id, ChangeOrderDeliveryRequest $request, ChangeOrderDeliveryAction $action)
    {
        return new OrdersResource($action->execute($id, $request->validated()));
    }

    public function attachFile(int $id, AttachOrderFileAction $action, AttachOrderFileRequest $request)
    {
        return new OrderFilesResource($action->execute($id, $request->getFile()));
    }

    public function deleteFiles(int $id, DeleteOrderFilesAction $action, DeleteOrderFilesRequest $request)
    {
        $action->execute($id, $request->getFileIds());

        return new EmptyResource();
    }
}
