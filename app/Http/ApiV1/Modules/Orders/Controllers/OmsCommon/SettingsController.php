<?php

namespace App\Http\ApiV1\Modules\Orders\Controllers\OmsCommon;

use App\Domain\Common\Data\Meta\Field;
use App\Domain\Orders\Actions\OmsCommon\PatchSeveralSettingsAction;
use App\Http\ApiV1\Modules\Orders\Requests\OmsCommon\PatchSeveralSettingsRequest;
use App\Http\ApiV1\Modules\Orders\Resources\OmsCommon\SettingResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Ensi\OmsClient\Api\CommonApi;

class SettingsController
{
    public function search(CommonApi $commonApi)
    {
        return SettingResource::collection($commonApi->searchSettings()->getData());
    }

    public function patchSeveral(PatchSeveralSettingsAction $action, PatchSeveralSettingsRequest $request)
    {
        return SettingResource::collection($action->execute($request->validated()));
    }

    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::keyword('code', 'Символьный код')->listDefault()->filterDefault()->sort(),
            Field::text('name', 'Название')->listDefault()->filterDefault()->sort(),
            Field::keyword('value', 'Значение')->listDefault()->filterDefault()->sort(),

            Field::datetime('created_at', 'Дата создания')->listDefault()->sort(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault()->sort(),
        ]);
    }
}
