<?php

namespace App\Http\ApiV1\Modules\Orders\Requests\Refunds;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Http\UploadedFile;

class AttachRefundFileRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'file' => ['required', 'file', 'max:2048'],
        ];
    }

    public function getFile(): UploadedFile
    {
        return $this->file('file');
    }
}
