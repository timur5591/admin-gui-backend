<?php

namespace App\Http\ApiV1\Modules\Orders\Requests\Refunds;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\OmsClient\Dto\OrderSourceEnum;
use Ensi\OmsClient\Dto\RefundStatusEnum;
use Illuminate\Validation\Rule;

class CreateRefundRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'user_comment' => ['required', 'string'],
            'source' => ['required', Rule::in(OrderSourceEnum::getAllowableEnumValues())],
            'manager_id' => ['integer', 'nullable'],
            'order_id' => ['required'],
            'responsible_id' => ['nullable', 'integer'],
            'status' => [Rule::in(RefundStatusEnum::getAllowableEnumValues())],
            'rejection_comment' => ['nullable', 'string'],

            'order_items' => ['required', 'array'],
            'order_items.*.id' => ['required', 'integer'],
            'order_items.*.qty' => ['required', 'numeric', 'gt:0'],

            'refund_reason_ids' => ['required', 'array'],
            'refund_reason_ids.*' => ['required'],
        ];
    }
}
