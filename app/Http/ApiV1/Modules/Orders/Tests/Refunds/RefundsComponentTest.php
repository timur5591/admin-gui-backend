<?php

use App\Domain\Catalog\Tests\Factories\Categories\CategoryFactory;
use App\Domain\Catalog\Tests\Factories\Classifiers\BrandFactory;
use App\Domain\Catalog\Tests\Factories\Offers\OfferFactory;
use App\Domain\Catalog\Tests\Factories\Offers\StockFactory;
use App\Domain\Catalog\Tests\Factories\Products\ProductFactory;
use App\Domain\Orders\Tests\Factories\Orders\OrderFactory;
use App\Domain\Orders\Tests\Factories\Orders\OrderItemFactory;
use App\Domain\Orders\Tests\Factories\Refunds\RefundFactory;
use App\Domain\Orders\Tests\Factories\Refunds\RefundReasonFactory;
use App\Http\ApiV1\Modules\Orders\Tests\Refunds\Factories\RefundReasonRequestFactory;
use App\Http\ApiV1\Modules\Orders\Tests\Refunds\Factories\RefundRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\PaginationFactory;
use Ensi\LaravelTestFactories\PromiseFactory;
use Ensi\OmsClient\Dto\RefundStatusEnum;
use Ensi\OmsClient\Dto\SearchRefundsRequest;
use Ensi\TestFactories\FakerProvider;

use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'orders', 'orders.refunds');

test('GET /api/v1/orders/refunds/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $refundId = 1;

    $this->mockOmsRefundsApi()->allows([
        'getRefund' => RefundFactory::new()->makeResponse(),
    ]);

    getJson("/api/v1/orders/refunds/$refundId")
        ->assertStatus(200);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/orders/refunds/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $refundId = 1;
    $refundStatus = RefundStatusEnum::getAllowableEnumValues()[0];

    $this->mockOmsRefundsApi()->allows([
        'patchRefund' => RefundFactory::new()->makeResponse(['id' => $refundId, 'status' => $refundStatus]),
    ]);

    patchJson("/api/v1/orders/refunds/$refundId", ['status' => $refundStatus])
        ->assertStatus(200)
        ->assertJsonPath('data.id', $refundId)
        ->assertJsonPath('data.status', $refundStatus);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/orders/refunds:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $refundId = 1;
    $refundStatus = RefundStatusEnum::getAllowableEnumValues()[0];

    $this->mockOmsRefundsApi()->allows([
        'searchRefunds' => RefundFactory::new()->makeResponseSearch([['id' => $refundId, 'status' => $refundStatus]]),
    ]);

    postJson("/api/v1/orders/refunds:search", ['pagination' => PaginationFactory::new()->makeRequestOffset()])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $refundId)
        ->assertJsonPath('data.0.status', $refundStatus);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/orders/refunds:search all includes 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $refundId = 1;
    $orderId = 1;
    $offerId = 1;
    $productId = 1;
    $categoryId = 1;
    $brandId = 1;
    $stockQty = 1000.4;

    $omsIncludes = [
        'order',
        'items',
        'reasons',
        'files',
    ];

    $order = OrderFactory::new()->make(['id' => $orderId]);
    $orderItem = OrderItemFactory::new()->make(['offer_id' => $offerId]);
    $refundsResponse = RefundFactory::new()
        ->withOrder($order)
        ->withItem($orderItem)
        ->withReason()
        ->withFile()
        ->makeResponseSearch([['id' => $refundId, 'order_id' => $orderId]]);
    $this->mockOmsRefundsApi()
        ->shouldReceive('searchRefunds')
        ->withArgs(function ($arg) use ($omsIncludes) {
            /** @var SearchRefundsRequest $arg */
            assertEquals($omsIncludes, $arg->getInclude());

            return true;
        })
        ->andReturn($refundsResponse);

    $this->mockOffersOffersApi()->allows([
        'searchOffersAsync' => PromiseFactory::make(OfferFactory::new()
            ->withStock(StockFactory::new()->make(['offer_id' => $offerId, 'qty' => $stockQty]))
            ->makeResponseSearch([['id' => $offerId, 'product_id' => $productId]])),
    ]);
    $category = CategoryFactory::new()->make(['id' => $categoryId]);
    $brand = BrandFactory::new()->make(['id' => $brandId]);
    $this->mockPimProductsApi()->allows([
        'searchProductsAsync' => PromiseFactory::make(ProductFactory::new()
            ->withImages()
            ->withCategory($category)
            ->withBrand($brand)
            ->makeResponseSearch([['id' => $productId]])),
    ]);

    postJson("/api/v1/orders/refunds:search", [
        'include' => array_merge($omsIncludes, [
            'items.product',
        ]),
    ])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $refundId)
        ->assertJsonPath('data.0.order.id', $orderId)
        ->assertJsonCount(1, 'data.0.items')
        ->assertJsonCount(1, 'data.0.reasons')
        ->assertJsonCount(1, 'data.0.files')
        ->assertJsonPath('data.0.items.0.product.id', $productId)
        ->assertJsonCount(1, 'data.0.items.0.product.images')
        ->assertJsonPath('data.0.items.0.product.category.id', $categoryId)
        ->assertJsonPath('data.0.items.0.product.brand.id', $brandId)
        ->assertJsonPath('data.0.items.0.stock.qty', $stockQty);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/orders/refunds 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $refundStatus = RefundStatusEnum::getAllowableEnumValues()[0];
    $refund = RefundRequestFactory::new()->make(['status' => $refundStatus]);

    $this->mockOmsRefundsApi()->allows([
        'createRefund' => RefundFactory::new()->makeResponse(['status' => $refundStatus]),
    ]);

    postJson("/api/v1/orders/refunds", $refund)
        ->assertStatus(200)
        ->assertJsonPath('data.status', $refundStatus);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/orders/refund-reasons 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $name = 'refund name';
    $code = 'REFUND_CODE';

    $refund = RefundReasonRequestFactory::new()->make([
        'name' => $name,
        'code' => $code,
    ]);

    $this->mockOmsEnumsApi()->allows([
        'createRefundReason' => RefundReasonFactory::new()->makeResponse([
            'name' => $name,
            'code' => $code,
        ]),
    ]);

    postJson("/api/v1/orders/refund-reasons", $refund)
        ->assertStatus(200)
        ->assertJsonPath('data.name', $name)
        ->assertJsonPath('data.code', $code);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/orders/refund-reasons/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $refundReasonId = 1;
    $name = 'new reason name';

    $this->mockOmsEnumsApi()->allows([
        'patchRefundReason' => RefundReasonFactory::new()->makeResponse(['id' => $refundReasonId, 'name' => $name]),
    ]);

    patchJson("/api/v1/orders/refund-reasons/$refundReasonId", ['name' => $name])
        ->assertStatus(200)
        ->assertJsonPath('data.id', $refundReasonId)
        ->assertJsonPath('data.name', $name);
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/orders/refund-reasons 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $id = 12345;
    $name = 'example name';

    $this->mockOmsEnumsApi()->allows([
        'getRefundReasons' => RefundReasonFactory::new()->makeResponseSearch(['id' => $id, 'name' => $name]),
    ]);

    getJson("/api/v1/orders/refund-reasons")
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $id)
        ->assertJsonPath('data.0.name', $name);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/orders/refunds:meta 200', function () {
    getJson("/api/v1/orders/refunds:meta")->assertStatus(200);
});
