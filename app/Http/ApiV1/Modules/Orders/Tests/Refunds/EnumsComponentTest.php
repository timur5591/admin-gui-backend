<?php

use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\OmsClient\Dto\RefundStatusEnum;

use function Pest\Laravel\getJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'orders', 'orders.enums');

test('GET /api/v1/orders/refund-statuses 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => RefundStatusEnum::NEW,
        'name' => RefundStatusEnum::getDescriptions()[RefundStatusEnum::NEW],
    ];

    getJson("/api/v1/orders/refund-statuses")
        ->assertStatus(200)
        ->assertJsonFragment($item);
});
