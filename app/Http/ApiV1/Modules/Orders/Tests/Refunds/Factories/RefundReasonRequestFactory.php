<?php

namespace App\Http\ApiV1\Modules\Orders\Tests\Refunds\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class RefundReasonRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'code' => $this->faker->text(10),
            'name' => $this->faker->text(10),
            'description' => $this->faker->nullable()->text(50),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
