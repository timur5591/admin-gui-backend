<?php

namespace App\Http\ApiV1\Modules\Orders\Tests\Refunds\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OmsClient\Dto\OrderSourceEnum;
use Ensi\OmsClient\Dto\RefundStatusEnum;

class RefundRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'order_id' => $this->faker->modelId(),
            'manager_id' => $this->faker->nullable()->modelId(),
            'responsible_id' => $this->faker->nullable()->modelId(),
            'source' => $this->faker->randomElement(OrderSourceEnum::getAllowableEnumValues()),
            'status' => $this->faker->randomElement(RefundStatusEnum::getAllowableEnumValues()),
            'is_partial' => $this->faker->boolean(),
            'user_comment' => $this->faker->text(50),
            'rejection_comment' => $this->faker->nullable()->text(50),
            'order_items' => [
                ['id' => $this->faker->modelId(), 'qty' => $this->faker->numberBetween(1, 10)],
            ],
            'refund_reason_ids' => [$this->faker->modelId()],
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
