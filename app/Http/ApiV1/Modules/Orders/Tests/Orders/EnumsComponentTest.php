<?php

use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\OmsClient\Dto\DeliveryStatusEnum;
use Ensi\OmsClient\Dto\OrderSourceEnum;
use Ensi\OmsClient\Dto\OrderStatusEnum;
use Ensi\OmsClient\Dto\PaymentMethodEnum;
use Ensi\OmsClient\Dto\PaymentStatusEnum;
use Ensi\OmsClient\Dto\ShipmentStatusEnum;

use function Pest\Laravel\getJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'orders', 'orders.enums');

test('GET /api/v1/orders/order-statuses 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => OrderStatusEnum::NEW,
        'name' => OrderStatusEnum::getDescriptions()[OrderStatusEnum::NEW],
    ];

    getJson("/api/v1/orders/order-statuses")
        ->assertStatus(200)
        ->assertJsonFragment($item);
});

test('GET /api/v1/orders/order-sources 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => OrderSourceEnum::SITE,
        'name' => OrderSourceEnum::getDescriptions()[OrderSourceEnum::SITE],
    ];

    getJson("/api/v1/orders/order-sources")
        ->assertStatus(200)
        ->assertJsonFragment($item);
});

test('GET /api/v1/orders/payment-methods 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => PaymentMethodEnum::ONLINE,
        'name' => PaymentMethodEnum::getDescriptions()[PaymentMethodEnum::ONLINE],
    ];

    getJson("/api/v1/orders/payment-methods")
        ->assertStatus(200)
        ->assertJsonFragment($item);
});

test('GET /api/v1/orders/payment-statuses 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => PaymentStatusEnum::NOT_PAID,
        'name' => PaymentStatusEnum::getDescriptions()[PaymentStatusEnum::NOT_PAID],
    ];

    getJson("/api/v1/orders/payment-statuses")
        ->assertStatus(200)
        ->assertJsonFragment($item);
});

test('GET /api/v1/orders/delivery-statuses 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => DeliveryStatusEnum::NEW,
        'name' => DeliveryStatusEnum::getDescriptions()[DeliveryStatusEnum::NEW],
    ];

    getJson("/api/v1/orders/delivery-statuses")
        ->assertStatus(200)
        ->assertJsonFragment($item);
});

test('GET /api/v1/orders/shipment-statuses 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => ShipmentStatusEnum::NEW,
        'name' => ShipmentStatusEnum::getDescriptions()[ShipmentStatusEnum::NEW],
    ];

    getJson("/api/v1/orders/shipment-statuses")
        ->assertStatus(200)
        ->assertJsonFragment($item);
});
