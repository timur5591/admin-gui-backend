<?php

namespace App\Http\ApiV1\Modules\Orders\Policies\Refunds;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class RefundReasonsControllerPolicy
{
    use HandlesAuthorization;

    public function create(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::REFUND_DETAIL_REASONS_CREATE,
        ]);
    }

    public function patch(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::REFUND_DETAIL_REASONS_EDIT,
        ]);
    }
}
