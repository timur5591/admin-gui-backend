<?php

namespace App\Http\ApiV1\Modules\Orders\Policies\Refunds;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class RefundsControllerPolicy
{
    use HandlesAuthorization;

    public function create(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::ORDER_DETAIL_REFUND_CREATE,
        ]);
    }

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::REFUND_LIST_READ,
        ]);
    }

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::REFUND_DETAIL_READ,
            RightsAccessEnum::REFUND_DETAIL_MAIN_READ,
            RightsAccessEnum::REFUND_DETAIL_PRODUCTS_READ,
            RightsAccessEnum::REFUND_DETAIL_ATTACHMENTS_READ,
        ]);
    }

    public function patch(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::REFUND_DETAIL_EDIT,
            RightsAccessEnum::REFUND_DETAIL_RESPONSIBLE_EDIT,
        ]);
    }

    public function attachFile(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::REFUND_DETAIL_ATTACHMENTS_WRITE,
            RightsAccessEnum::REFUND_DETAIL_EDIT,
        ]);
    }

    public function deleteFiles(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::REFUND_DETAIL_ATTACHMENTS_DELETE,
            RightsAccessEnum::REFUND_DETAIL_EDIT,
        ]);
    }
}
