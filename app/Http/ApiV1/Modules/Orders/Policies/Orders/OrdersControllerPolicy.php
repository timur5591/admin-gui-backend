<?php

namespace App\Http\ApiV1\Modules\Orders\Policies\Orders;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class OrdersControllerPolicy
{
    use HandlesAuthorization;

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::ORDER_LIST_READ,
        ]);
    }

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::ORDER_DETAIL_READ,
            RightsAccessEnum::ORDER_DETAIL_MAIN_READ,
            RightsAccessEnum::ORDER_DETAIL_CLIENT_READ,
            RightsAccessEnum::ORDER_DETAIL_PRODUCTS_READ,
            RightsAccessEnum::ORDER_DETAIL_DELIVERY_READ,
            RightsAccessEnum::ORDER_DETAIL_COMMENTS_READ,
            RightsAccessEnum::ORDER_DETAIL_ATTACHMENTS_READ,
        ]);
    }

    public function patch(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::ORDER_DETAIL_EDIT,
            RightsAccessEnum::ORDER_DETAIL_COMMENTS_EDIT,
            RightsAccessEnum::ORDER_DETAIL_PROBLEMATIC_EDIT,
            RightsAccessEnum::ORDER_DETAIL_DELIVERY_EDIT,
            RightsAccessEnum::ORDER_DETAIL_DELIVERY_COMMENTS_EDIT,
            RightsAccessEnum::ORDER_DETAIL_CLIENT_EDIT,
            RightsAccessEnum::ORDER_DETAIL_STATUS_EDIT,
        ]);
    }

    public function attachFile(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::ORDER_DETAIL_ATTACHMENTS_WRITE,
            RightsAccessEnum::ORDER_DETAIL_EDIT,
        ]);
    }

    public function deleteFiles(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::ORDER_DETAIL_ATTACHMENTS_DELETE,
            RightsAccessEnum::ORDER_DETAIL_EDIT,
        ]);
    }
}
