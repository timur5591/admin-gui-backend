<?php

namespace App\Http\ApiV1\Modules\Catalog\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class BrandRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'is_active' => $this->faker->boolean,
            'name' => $this->faker->text(20),
            'code' => $this->faker->word(),
            'description' => $this->faker->text(50),
            'logo_url' => $this->faker->nullable()->imageUrl,
            'preload_file_id' => $this->faker->numberBetween(1, 500),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
