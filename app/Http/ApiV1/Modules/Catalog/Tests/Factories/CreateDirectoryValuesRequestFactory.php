<?php

namespace App\Http\ApiV1\Modules\Catalog\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\PropertyTypeEnum;
use Faker\Generator;
use Tests\Helpers\Catalog\PropertyValueGenerator;

class CreateDirectoryValuesRequestFactory extends BaseApiFactory
{
    public PropertyValueGenerator $valueGenerator;

    private readonly ?string $type;

    public function __construct(Generator $faker)
    {
        parent::__construct($faker);

        $this->type = $this->faker->randomElement(PropertyTypeEnum::getAllowableEnumValues());

        $this->valueGenerator = PropertyValueGenerator::new($this->faker, $this->type);
    }

    protected function definition(): array
    {
        $items = [];
        $preloadedFile = ($this->type === PropertyTypeEnum::FILE || $this->type === PropertyTypeEnum::IMAGE);
        for ($i = 0; $i < $this->faker->randomDigitNotZero(); $i++) {
            $item = [
                'name' => $this->faker->sentence(3),
                'code' => $this->faker->slug(3),
            ];

            if ($preloadedFile) {
                $item['value'] = $this->valueGenerator->value();
            } else {
                $item['preload_file_id'] = $this->faker->randomDigit();
            }

            $items[] = $item;
        }

        return [
            'items' => $items,
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
