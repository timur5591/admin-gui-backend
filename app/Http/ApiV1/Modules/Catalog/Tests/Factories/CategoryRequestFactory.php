<?php

namespace App\Http\ApiV1\Modules\Catalog\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class CategoryRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'name' => $this->faker->sentence(2),
            'code' => $this->faker->slug(2),
            'is_inherits_properties' => $this->faker->boolean,
            'is_active' => $this->faker->boolean,
            'parent_id' => $this->faker->modelId(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
