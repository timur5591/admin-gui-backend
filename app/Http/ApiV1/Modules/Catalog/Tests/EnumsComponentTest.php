<?php

use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\PimClient\Dto\ProductTypeEnum;
use Ensi\PimClient\Dto\PropertyTypeEnum;

use function Pest\Laravel\getJson;

uses(ApiV1ComponentTestCase::class)->group('catalog', 'component');

test('GET /api/v1/catalog/products/product-types 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => ProductTypeEnum::PIECE,
        'name' => ProductTypeEnum::getDescriptions()[ProductTypeEnum::PIECE],
    ];

    getJson('/api/v1/catalog/products/product-types')
        ->assertStatus(200)
        ->assertJsonFragment($item);
});

test('GET /api/v1/catalog/properties/properties-types 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => PropertyTypeEnum::STRING,
        'name' => PropertyTypeEnum::getDescriptions()[PropertyTypeEnum::STRING],
    ];

    getJson('/api/v1/catalog/properties/properties-types')
        ->assertStatus(200)
        ->assertJsonFragment($item);
});
