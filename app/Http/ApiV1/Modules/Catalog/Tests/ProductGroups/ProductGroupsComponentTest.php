<?php

use App\Domain\Catalog\Tests\Factories\ProductGroups\ProductGroupBindResultFactory;
use App\Domain\Catalog\Tests\Factories\ProductGroups\ProductGroupFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\ProductGroupRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\EmptyDataResponse;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('catalog', 'component');

test('POST /api/v1/catalog/product-groups 200', function () {
    $id = 10;
    $this->mockPimProductGroupsApi()->allows([
        'createProductGroup' => ProductGroupFactory::new()->makeResponse(['id' => $id]),
    ]);
    $request = ProductGroupRequestFactory::new()->make();

    postJson('/api/v1/catalog/product-groups', $request)
        ->assertOk()
        ->assertJsonPath('data.id', $id);
});

test('POST /api/v1/catalog/product-groups 400', function () {
    $this->skipNextOpenApiRequestValidation();

    postJson('/api/v1/catalog/product-groups')
        ->assertStatus(400);
});

test('GET /api/v1/catalog/product-groups/{id} 200', function () {
    $id = 30;
    $this->mockPimProductGroupsApi()->allows([
        'getProductGroup' => ProductGroupFactory::new()
            ->withCategory()
            ->withMainProduct()
            ->withProducts(2)
            ->makeResponse(['id' => 30]),
    ]);

    getJson("/api/v1/catalog/product-groups/$id?include=category,main_product,products")
        ->assertOk()
        ->assertJsonPath('data.id', $id)
        ->assertJsonCount(2, 'data.products')
        ->assertJsonStructure(['data' => ['category' => ['id', 'name']]])
        ->assertJsonStructure(['data' => ['main_product' => ['id', 'name']]])
        ->assertJsonStructure(['data' => ['products' => [['id', 'name']]]]);
});

test('GET /api/v1/catalog/product-groups/{id} 404', function () {
    $this->mockPimProductGroupsApi()
        ->shouldReceive('getProductGroup')
        ->andThrowExceptions([new ApiException("NotFound", 404)]);

    getJson('/api/v1/catalog/product-groups/999')
        ->assertStatus(404);
});

test('PUT /api/v1/catalog/product-groups/{id} 200', function () {
    $id = 12;
    $this->mockPimProductGroupsApi()->allows([
        'replaceProductGroup' => ProductGroupFactory::new()->makeResponse(['id' => $id]),
    ]);
    $request = ProductGroupRequestFactory::new()->make();

    putJson("/api/v1/catalog/product-groups/$id", $request)
        ->assertOk()
        ->assertJsonPath('data.id', $id);
});

test('PUT /api/v1/catalog/product-groups/{id} 400', function () {
    $this->skipNextOpenApiRequestValidation();

    putJson('/api/v1/catalog/product-groups/1')
        ->assertStatus(400);
});

test('PUT /api/v1/catalog/product-groups/{id} 404', function () {
    $this->mockPimProductGroupsApi()
        ->shouldReceive('replaceProductGroup')
        ->andThrowExceptions([new ApiException("NotFound", 404)]);
    $request = ProductGroupRequestFactory::new()->make();

    putJson('/api/v1/catalog/product-groups/999', $request)
        ->assertStatus(404);
});

test('PATCH /api/v1/catalog/product-groups/{id} 200', function () {
    $id = 12;
    $this->mockPimProductGroupsApi()->allows([
        'patchProductGroup' => ProductGroupFactory::new()->makeResponse(['id' => $id]),
    ]);
    $request = ProductGroupRequestFactory::new()->make();

    patchJson("/api/v1/catalog/product-groups/$id", $request)
        ->assertOk()
        ->assertJsonPath('data.id', $id);
});

test('PATCH /api/v1/catalog/product-groups/{id} 400', function () {
    $request = ProductGroupRequestFactory::new()->make(['category_id' => 0]);
    $this->skipNextOpenApiRequestValidation();

    patchJson('/api/v1/catalog/product-groups/1', $request)
        ->assertStatus(400);
});

test('PATCH /api/v1/catalog/product-groups/{id} 404', function () {
    $this->mockPimProductGroupsApi()
        ->shouldReceive('patchProductGroup')
        ->andThrowExceptions([new ApiException("NotFound", 404)]);
    $request = ProductGroupRequestFactory::new()->make();

    patchJson('/api/v1/catalog/product-groups/999', $request)
        ->assertStatus(404);
});

test('DELETE /api/v1/catalog/product-groups/{id} 200', function () {
    $this->mockPimProductGroupsApi()->allows([
        'deleteProductGroup' => new EmptyDataResponse(),
    ]);

    deleteJson('/api/v1/catalog/product-groups/50')
        ->assertStatus(200);
});

test('DELETE /api/v1/catalog/product-groups/{id} 404', function () {
    $this->mockPimProductGroupsApi()
        ->shouldReceive('deleteProductGroup')
        ->andThrowExceptions([new ApiException("NotFound", 404)]);

    deleteJson('/api/v1/catalog/product-groups/999')
        ->assertStatus(404);
});

test('POST /api/v1/catalog/product-groups/{id}:bind-products 200', function () {
    $id = 22;
    $group = ProductGroupFactory::new()->withProducts(3)->make(['id' => $id]);
    $this->mockPimProductGroupsApi()->allows([
        'bindProductGroupProducts' => ProductGroupBindResultFactory::new()->withProductGroup($group)->makeResponseOne(),
    ]);

    $request = [
        'replace' => false,
        'products' => [
            ['id' => 100],
            ['vendor_code' => "A1000"],
            ['barcode' => "46071234567"],
        ],
    ];

    postJson("/api/v1/catalog/product-groups/$id:bind-products", $request)
        ->assertOk()
        ->assertJsonPath('data.product_group.id', $id)
        ->assertJsonCount(3, 'data.product_group.products')
        ->assertJsonStructure(['data' => ['product_group' => ['products' => [['id', 'name']]], 'product_errors']]);
});

test('POST /api/v1/catalog/product-groups/{id}:bind-products 200 with product errors', function () {
    $id = 22;
    $productId = 123;
    $group = ProductGroupFactory::new()->make(['id' => $id]);
    $this->mockPimProductGroupsApi()->allows([
        'bindProductGroupProducts' => ProductGroupBindResultFactory::new()
            ->withProductGroup($group)
            ->withProductErrors([$productId])
            ->makeResponseOne(),
    ]);

    $request = [
        'replace' => false,
        'products' => [
            ['id' => $productId],
            ['vendor_code' => "A1000"],
            ['barcode' => "46071234567"],
        ],
    ];

    postJson("/api/v1/catalog/product-groups/$id:bind-products", $request)
        ->assertOk()
        ->assertJsonPath('data.product_group.id', $id)
        ->assertJsonPath('data.product_errors.0', $productId)
        ->assertJsonCount(1, 'data.product_errors');
});

test('POST /api/v1/catalog/product-groups/{id}:bind-products 400', function () {
    $request = [
        'replace' => false,
        'products' => [[]],
    ];
    $this->skipNextOpenApiRequestValidation();

    postJson('/api/v1/catalog/product-groups/1:bind-products', $request)
        ->assertStatus(400);
});

test('POST /api/v1/catalog/product-groups/{id}:bind-products 404', function () {
    $this->mockPimProductGroupsApi()
        ->shouldReceive('bindProductGroupProducts')
        ->andThrowExceptions([new ApiException("NotFound", 404)]);

    postJson('/api/v1/catalog/product-groups/999:bind-products')
        ->assertStatus(404);
});

test('POST /api/v1/catalog/product-groups:mass-delete 200', function () {
    $propertyGroups = ProductGroupFactory::new()->makeSeveral(5);
    $this->mockPimProductGroupsApi()->allows([
        'deleteProductGroups' => new EmptyDataResponse(),
    ]);

    postJson('/api/v1/catalog/product-groups:mass-delete', ['ids' => $propertyGroups->pluck('id')])
        ->assertOk();
});

test('POST /api/v1/catalog/product-groups:mass-delete 400', function () {
    $this->skipNextOpenApiRequestValidation();
    postJson('/api/v1/catalog/product-groups:mass-delete')
        ->assertStatus(400);
});

test('GET /api/v1/catalog/product-groups:meta 200', function () {
    getJson('/api/v1/catalog/product-groups:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});

test('POST /api/v1/catalog/product-groups:search 200', function () {
    $this->mockPimProductGroupsApi()->allows([
        'searchProductGroups' => ProductGroupFactory::new()
            ->withProducts(3)
            ->makeResponseSearch([], 2),
    ]);

    $request = [
        'filter' => ['name' => 'foo'],
        'include' => ['products'],
    ];

    postJson('/api/v1/catalog/product-groups:search', $request)
        ->assertOk()
        ->assertJsonCount(2, 'data')
        ->assertJsonCount(3, 'data.0.products');
});

test('POST /api/v1/catalog/product-groups:search-one 200', function () {
    $id = 10;
    $this->mockPimProductGroupsApi()->allows([
        'searchOneProductGroup' => ProductGroupFactory::new()
            ->withProducts(3)
            ->makeResponse(['id' => $id]),
    ]);

    $request = [
        'filter' => ['id' => $id],
        'include' => ['products'],
    ];

    postJson('/api/v1/catalog/product-groups:search-one', $request)
        ->assertOk()
        ->assertJsonPath('data.id', $id)
        ->assertJsonCount(3, 'data.products');
});
