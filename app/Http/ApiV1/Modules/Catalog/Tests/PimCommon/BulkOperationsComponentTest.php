<?php

use App\Domain\Catalog\Tests\Factories\PimCommon\BulkOperationFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\TestFactories\FakerProvider;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'pim_common');

test('POST /api/v1/catalog/bulk-operations:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $processedIds = [1,2,3];
    $this->mockPimCommonApi()->allows([
        'searchBulkOperations' => BulkOperationFactory::new()
            ->withError()
            ->makeResponseSearch([['ids' => $processedIds]]),
    ]);

    $request = [
        'include' => ['errors'],
    ];

    postJson('/api/v1/catalog/bulk-operations:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.ids_string', implode(', ', $processedIds))
        ->assertJsonCount(1, 'data.0.errors');
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/catalog/bulk-operations:meta 200', function () {
    getJson('/api/v1/catalog/bulk-operations:meta')
        ->assertStatus(200)
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});
