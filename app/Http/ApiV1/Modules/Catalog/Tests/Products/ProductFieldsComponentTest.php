<?php

use App\Domain\Catalog\Tests\Factories\Products\ProductFieldFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\ProductFieldRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\CatalogFieldSettingsMaskEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class)->group('catalog', 'component');

test('POST /api/v1/catalog/products/fields:search 200', function () {
    $this->mockPimProductFieldsApi()->allows([
        'searchProductFields' => ProductFieldFactory::new()->makeResponseSearch([
            ['id' => 3],
            ['id' => 4],
        ], 2),
    ]);

    $request = [
        'filter' => ['name' => 'foo'],
    ];

    postJson('/api/v1/catalog/products/fields:search', $request)
        ->assertOk()
        ->assertJsonCount(2, 'data')
        ->assertJsonStructure(['data' => [['id', 'code', 'name']]])
        ->assertJsonPath('data.0.id', 3);
});

test('GET /api/v1/catalog/products/fields/{id} 200', function () {
    $this->mockPimProductFieldsApi()->allows([
        'getProductField' => ProductFieldFactory::new()->makeResponse(['id' => 5]),
    ]);

    getJson('/api/v1/catalog/products/fields/5')
        ->assertOk()
        ->assertJsonPath('data.id', 5);
});

test('GET /api/v1/catalog/products/fields/{id} expand edit mask', function () {
    $this->mockPimProductFieldsApi()->allows([
        'getProductField' => ProductFieldFactory::new()->makeResponse([
            'id' => 5,
            'edit_mask' => 3,
        ]),
    ]);

    getJson('/api/v1/catalog/products/fields/5')
        ->assertOk()
        ->assertJsonPath('data.edit_mask', [
            CatalogFieldSettingsMaskEnum::NAME->value,
            CatalogFieldSettingsMaskEnum::MODERATED->value,
        ]);
});

test('PATCH /api/v1/catalog/products/fields/{id} 200', function () {
    $request = ProductFieldRequestFactory::new()->make();

    $this->mockPimProductFieldsApi()->allows([
        'patchProductField' => ProductFieldFactory::new()->makeResponse(['id' => 10]),
    ]);

    patchJson('/api/v1/catalog/products/fields/10', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'code', 'edit_mask', 'is_required']])
        ->assertJsonPath('data.id', 10);
});

test('PATCH /api/v1/catalog/products/fields/{id} 400', function () {
    $request = ['name' => ''];

    patchJson('/api/v1/catalog/products/fields/15', $request)
        ->assertStatus(400);
});
