<?php

use App\Domain\Catalog\Tests\Factories\Offers\OfferFactory;
use App\Domain\Catalog\Tests\Factories\Products\ProductFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\TestFactories\FakerProvider;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class)->group('catalog', 'component');

test('POST /api/v1/catalog/products:search success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $this->mockPimProductsApi()->allows([
        'searchProducts' => ProductFactory::new()->makeResponseSearch([['id' => 38]]),
    ]);
    $this->mockOffersOffersApi()->allows([
        'searchOffers' => OfferFactory::new()->makeResponseSearch([['product_id' => 38, 'base_price' => 1440]]),
    ]);

    $request = [
        'filter' => ['name' => 'foo'],
    ];

    postJson('/api/v1/catalog/products:search', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'name', 'main_image_url', 'main_image_file', 'base_price']]])
        ->assertJsonPath('data.0.base_price', 1440);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/catalog/products:search with flag required attributes success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $productId = 38;

    $this->mockPimProductsApi()->allows([
        'searchProducts' => ProductFactory::new()
            ->withNoFilledRequiredAttributes(true)
            ->makeResponseSearch([['id' => $productId]]),
    ]);

    $this->mockOffersOffersApi()->allows([
        'searchOffers' => OfferFactory::new()->makeResponseSearch([['product_id' => $productId]]),
    ]);

    $request = [
        'filter' => ['id' => $productId],
    ];

    postJson('/api/v1/catalog/products:search', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'name', 'no_filled_required_attributes']]])
        ->assertJsonPath('data.0.no_filled_required_attributes', true);
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/catalog/products/{id} success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $productId = 514;
    $this->mockPimProductsApi()->allows([
        'getProduct' => ProductFactory::new()->makeResponse(['id' => $productId]),
    ]);
    $this->mockOffersOffersApi()->allows([
        'searchOffers' => OfferFactory::new()->makeResponseSearch([['product_id' => $productId, 'base_price' => 12270]]),
    ]);

    getJson("/api/v1/catalog/products/$productId")
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'base_price', 'main_image_url', 'main_image_file']])
        ->assertJsonPath('data.base_price', 12270);
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/catalog/products:meta success', function () {
    getJson('/api/v1/catalog/products:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']])
        ->assertJsonFragment(['code' => 'base_price', 'type' => 'price']);
});
