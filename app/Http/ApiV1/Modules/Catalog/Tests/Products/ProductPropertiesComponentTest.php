<?php

use App\Domain\Catalog\Tests\Factories\Categories\PropertyFactory;
use App\Domain\Catalog\Tests\Factories\Products\ProductPropertyValueFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\ProductPropertyValueRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\PimClient\Dto\PropertyTypeEnum;
use Ensi\TestFactories\FakerProvider;

use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class)->group('catalog', 'component');

test('PUT /api/v1/catalog/products/{id}/attributes success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $request = ProductPropertyValueRequestFactory::new()->makeRequest(2);

    $this->mockPimProductsApi()->allows([
        'replaceProductAttributes' => ProductPropertyValueFactory::new()->makeResponse(2),
    ]);

    putJson('/api/v1/catalog/products/105/attributes', $request)
        ->assertJsonStructure(['data' => [['property_id', 'value', 'type']]])
        ->assertOk();
})->with(FakerProvider::$optionalDataset);

test('PUT /api/v1/catalog/products/{id}/attributes type image success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $request = ProductPropertyValueRequestFactory::new()->makeRequest(2, ['type' => PropertyTypeEnum::IMAGE]);

    $this->mockPimProductsApi()->allows([
        'replaceProductAttributes' => ProductPropertyValueFactory::new()->makeResponse(2, ['type' => PropertyTypeEnum::IMAGE]),
    ]);

    putJson('/api/v1/catalog/products/105/attributes', $request)
        ->assertJsonStructure(['data' => [['property_id', 'value', 'url', 'type']]])
        ->assertOk();
})->with(FakerProvider::$optionalDataset);

test('PUT /api/v1/catalog/products/{id}/attributes type file success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $request = ProductPropertyValueRequestFactory::new()->makeRequest(2, ['type' => PropertyTypeEnum::FILE]);

    $this->mockPimProductsApi()->allows([
        'replaceProductAttributes' => ProductPropertyValueFactory::new()->makeResponse(2, ['type' => PropertyTypeEnum::FILE]),
    ]);

    putJson('/api/v1/catalog/products/105/attributes', $request)
        ->assertJsonStructure(['data' => [['property_id', 'value', 'url', 'type']]])
        ->assertOk();
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/catalog/products/{id}/attributes success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $request = ProductPropertyValueRequestFactory::new()->makeRequest(2);

    $this->mockPimProductsApi()->allows([
        'patchProductAttributes' => ProductPropertyValueFactory::new()->makeResponse(2),
    ]);

    patchJson('/api/v1/catalog/products/105/attributes', $request)
        ->assertJsonStructure(['data' => [['property_id', 'value', 'type']]])
        ->assertOk();
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/catalog/products/{id}/attributes preload file', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $this->mockPimProductsApi()->allows([
        'patchProductAttributes' => ProductPropertyValueFactory::new()->makeResponse(),
    ]);

    $request = ['attributes' => [
        [
            'property_id' => 105,
            'preload_file_id' => 12540,
        ],
    ]];

    patchJson('/api/v1/catalog/products/105/attributes', $request)
        ->assertOk();
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/catalog/products:common-attributes success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $this->mockPimProductsApi()->allows([
        'getProductsCommonAttributes' => PropertyFactory::new()->withDirectory()->makeResponseCommon(),
    ]);

    $request = [
        'filter' => ['name' => 'foo'],
    ];

    postJson('/api/v1/catalog/products:common-attributes', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [
            ['id', 'directory' => [['id', 'name', 'code', 'value']]],
        ]]);
})->with(FakerProvider::$optionalDataset);
