<?php

use App\Domain\Catalog\Tests\Factories\Categories\DirectoryValueFactory;
use App\Domain\Catalog\Tests\Factories\PreloadFileFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\CreateDirectoryValuesRequestFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\DirectoryValueRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\PimClient\Dto\EmptyDataResponse;
use Illuminate\Http\UploadedFile;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class)->group('catalog', 'component');

test('POST /api/v1/catalog/properties/{id}:add-directory success', function () {
    $request = DirectoryValueRequestFactory::new()->make();
    $this->mockPimPropertiesApi()->allows([
        'createDirectoryValue' => DirectoryValueFactory::new()->makeResponse(['id' => 22]),
    ]);

    postJson('/api/v1/catalog/properties/5:add-directory', $request)
        ->assertOk()
        ->assertJsonPath('data.id', 22);
});

test('PUT /api/v1/catalog/properties/directory/{id} success', function () {
    $request = DirectoryValueRequestFactory::new()->make();
    $this->mockPimPropertiesApi()->allows([
        'replaceDirectoryValue' => DirectoryValueFactory::new()->makeResponse(['id' => 15]),
    ]);

    putJson('/api/v1/catalog/properties/directory/15', $request)
        ->assertOk()
        ->assertJsonPath('data.id', 15);
});

test('DELETE /api/v1/catalog/properties/directory/{id} success', function () {
    $this->mockPimPropertiesApi()->allows([
        'deleteDirectoryValue' => new EmptyDataResponse(),
    ]);

    deleteJson('/api/v1/catalog/properties/directory/30')
        ->assertOk();
});

test('GET /api/v1/catalog/properties/directory/{id} success', function () {
    $this->mockPimPropertiesApi()->allows([
        'getDirectoryValue' => DirectoryValueFactory::new()->makeResponse(['id' => 45]),
    ]);

    getJson('/api/v1/catalog/properties/directory/45')
        ->assertOk()
        ->assertJsonPath('data.id', 45)
        ->assertJsonStructure(['data' => ['code', 'name', 'id', 'value']]);
});

test('GET /api/v1/catalog/properties/directory/{id} with file success', function () {
    $this->mockPimPropertiesApi()->allows([
        'getDirectoryValue' => DirectoryValueFactory::new()->withFile('https://ya.ru/images/1')->makeResponse(),
    ]);

    getJson('/api/v1/catalog/properties/directory/45')
        ->assertOk()
        ->assertJsonPath('data.url', 'https://ya.ru/images/1');
});

test('POST /api/v1/catalog/properties/directory:search success', function () {
    $this->mockPimPropertiesApi()->allows([
        'searchDirectoryValues' => DirectoryValueFactory::new()->makeResponseSearch([['id' => 51]]),
    ]);

    postJson('/api/v1/catalog/properties/directory:search', [])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', 51)
        ->assertJsonStructure(['data' => [['id', 'name', 'value', 'property_id']]]);
});

test('POST /api/v1/catalog/properties/directory:preload-image success', function () {
    $this->mockPimPropertiesApi()->allows([
        'preloadDirectoryValueImage' => PreloadFileFactory::new()->make(),
    ]);

    $file = UploadedFile::fake()->image('foo.png', 300, 300);

    postFile('/api/v1/catalog/properties/directory:preload-image', $file)
        ->assertOk()
        ->assertJsonStructure(['data' => ['preload_file_id', 'url']]);
});

test('POST /api/v1/catalog/properties/directory:preload-file success', function () {
    $this->mockPimPropertiesApi()->allows([
        'preloadDirectoryValueFile' => PreloadFileFactory::new()->make(),
    ]);

    $file = UploadedFile::fake()->create('foo.txt', kilobytes: 20);

    postFile('/api/v1/catalog/properties/directory:preload-file', $file)
        ->assertOk()
        ->assertJsonStructure(['data' => ['preload_file_id', 'url']]);
});

test('POST /api/v1/catalog/properties/{id}:mass-add-directory success', function () {
    $propertyId = 7;
    $request = CreateDirectoryValuesRequestFactory::new()->make();

    $this->mockPimPropertiesApi()->allows([
        'createDirectoryValues' => DirectoryValueFactory::new()->makeResponseBatch($request['items'], ['property_id' => $propertyId]),
    ]);

    postJson("/api/v1/catalog/properties/{$propertyId}:mass-add-directory", $request)
        ->assertOk()
        ->assertJsonCount(count($request['items']), 'data')
        ->assertJsonPath('data.0.property_id', $propertyId)
        ->assertJsonStructure(['data' => [['id', 'name', 'value', 'property_id']]]);
});
