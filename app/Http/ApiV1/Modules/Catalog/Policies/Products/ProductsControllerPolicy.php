<?php

namespace App\Http\ApiV1\Modules\Catalog\Policies\Products;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ProductsControllerPolicy
{
    use HandlesAuthorization;

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_LIST_READ,
        ]);
    }

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_READ,
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_MAIN_DATA_READ,
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_CONTENT_READ,
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_FEATURE_READ,
        ]);
    }
}
