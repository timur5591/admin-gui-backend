<?php

namespace App\Http\ApiV1\Modules\Catalog\Policies\Products;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ProductDraftsControllerPolicy
{
    use HandlesAuthorization;

    public function create(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_PRODUCT_CREATE,
        ]);
    }

    public function replace(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_WRITE,
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_ACTIVE_EDIT,
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_CONTENT_EDIT,
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_FEATURE_EDIT,
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_MAIN_DATA_EDIT,
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_PRICE_EDIT,
        ]);
    }

    public function delete(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_PRODUCT_DELETE,
        ]);
    }

    public function massPatch(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_MASS_PATCH,
        ]);
    }

    public function massPatchByQuery(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_MASS_PATCH,
        ]);
    }
}
