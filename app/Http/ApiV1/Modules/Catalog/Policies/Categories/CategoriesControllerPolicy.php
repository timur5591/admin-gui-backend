<?php

namespace App\Http\ApiV1\Modules\Catalog\Policies\Categories;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class CategoriesControllerPolicy
{
    use HandlesAuthorization;

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATEGORY_LIST_READ,
        ]);
    }

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATEGORY_DETAIL_READ,
        ]);
    }

    public function replace(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATEGORY_DETAIL_WRITE,
            RightsAccessEnum::PRODUCT_CATEGORY_DETAIL_EDIT,
            RightsAccessEnum::PRODUCT_CATEGORY_DETAIL_ACTIVE_EDIT,
        ]);
    }

    public function create(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATEGORY_CREATE,
        ]);
    }

    public function delete(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATEGORY_DELETE,
        ]);
    }

    public function massDelete(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATEGORY_DELETE,
        ]);
    }

    public function bindProperties(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATEGORY_DETAIL_ATTRIBUTES_WRITE,
            RightsAccessEnum::PRODUCT_CATEGORY_DETAIL_WRITE,
        ]);
    }
}
