<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Categories;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFilterTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderSortTrait;
use Ensi\PimClient\Api\CategoriesApi;
use Ensi\PimClient\Dto\CategoriesTreeRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class CategoriesTreeQuery extends QueryBuilder
{
    use QueryBuilderFilterTrait;
    use QueryBuilderSortTrait;

    public function __construct(Request $request, private CategoriesApi $api)
    {
        parent::__construct($request);
    }

    public function get(): Collection
    {
        $request = new CategoriesTreeRequest();
        $this->fillFilters($request);
        $this->fillSort($request);

        return collect($this->api->getCategoriesTree($request)->getData());
    }
}
