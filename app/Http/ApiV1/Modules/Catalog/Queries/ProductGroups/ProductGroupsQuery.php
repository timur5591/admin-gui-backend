<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\ProductGroups;

use App\Http\ApiV1\Modules\Catalog\Queries\PimQuery;
use App\Http\ApiV1\Support\Queries\QueryBuilderFirstTrait;
use Ensi\PimClient\Api\ProductGroupsApi;
use Ensi\PimClient\Dto\ProductGroupResponse;
use Ensi\PimClient\Dto\SearchProductGroupsRequest;
use Ensi\PimClient\Dto\SearchProductGroupsResponse;
use Illuminate\Http\Request;

class ProductGroupsQuery extends PimQuery
{
    use QueryBuilderFirstTrait;

    public function __construct(Request $request, private ProductGroupsApi $api)
    {
        parent::__construct($request, SearchProductGroupsRequest::class);
    }

    protected function searchById($id): ProductGroupResponse
    {
        return $this->api->getProductGroup($id, $this->getInclude());
    }

    protected function search($request): SearchProductGroupsResponse
    {
        return $this->api->searchProductGroups($request);
    }

    protected function requestFirstClass(): string
    {
        return SearchProductGroupsRequest::class;
    }

    protected function searchOne($request): ProductGroupResponse
    {
        return $this->api->searchOneProductGroup($request);
    }

    protected function getHttpInclude(): array
    {
        $httpIncludes = parent::getHttpInclude();
        $httpIncludes = array_merge($httpIncludes, ['main_product.images', 'main_product.attributes', 'products_count']);

        return array_values(array_unique($httpIncludes));
    }
}
