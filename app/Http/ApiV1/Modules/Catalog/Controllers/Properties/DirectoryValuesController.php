<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\Properties;

use App\Domain\Catalog\Actions\Properties\CreateDirectoryValueAction;
use App\Domain\Catalog\Actions\Properties\DeleteDirectoryValueAction;
use App\Domain\Catalog\Actions\Properties\MassCreateDirectoryValueAction;
use App\Domain\Catalog\Actions\Properties\PreloadDirectoryFileAction;
use App\Domain\Catalog\Actions\Properties\PreloadDirectoryImageAction;
use App\Domain\Catalog\Actions\Properties\ReplaceDirectoryValueAction;
use App\Http\ApiV1\Modules\Catalog\Queries\Properties\DirectoryValuesQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Properties\CreateDirectoryValueRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Properties\MassCreateDirectoryValueRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Properties\PreloadDirectoryValueFileRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Properties\PreloadDirectoryValueImageRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Properties\ReplaceDirectoryValueRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\PreloadFileResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Properties\DirectoryValuesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class DirectoryValuesController
{
    public function create(
        int $propertyId,
        CreateDirectoryValueRequest $request,
        CreateDirectoryValueAction $action
    ): DirectoryValuesResource {
        return new DirectoryValuesResource($action->execute($propertyId, $request->validated()));
    }

    public function massCreate(
        int $propertyId,
        MassCreateDirectoryValueRequest $request,
        MassCreateDirectoryValueAction $action
    ): AnonymousResourceCollection {
        return DirectoryValuesResource::collection($action->execute($propertyId, $request->validated()));
    }

    public function replace(
        int $directoryValueId,
        ReplaceDirectoryValueRequest $request,
        ReplaceDirectoryValueAction $action
    ): DirectoryValuesResource {
        return new DirectoryValuesResource($action->execute($directoryValueId, $request->validated()));
    }

    public function delete(int $directoryValueId, DeleteDirectoryValueAction $action): EmptyResource
    {
        $action->execute($directoryValueId);

        return new EmptyResource();
    }

    public function search(DirectoryValuesQuery $query): AnonymousResourceCollection
    {
        return DirectoryValuesResource::collectPage($query->get());
    }

    public function get(int $directoryValueId, DirectoryValuesQuery $query): DirectoryValuesResource
    {
        return new DirectoryValuesResource($query->find($directoryValueId));
    }

    public function preloadImage(PreloadDirectoryValueImageRequest $request, PreloadDirectoryImageAction $action): PreloadFileResource
    {
        return new PreloadFileResource($action->execute($request->image()));
    }

    public function preloadFile(PreloadDirectoryValueFileRequest $request, PreloadDirectoryFileAction $action): PreloadFileResource
    {
        return new PreloadFileResource($action->execute($request->file('file')));
    }
}
