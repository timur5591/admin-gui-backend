<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\PimCommon;

use App\Domain\Common\Data\Meta\Enum\Catalog\BulkOperationActionEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Catalog\BulkOperationEntityEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Catalog\BulkOperationStatusEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\PimCommon\BulkOperationsQuery;
use App\Http\ApiV1\Modules\Catalog\Resources\PimCommon\BulkOperationsResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class BulkOperationsController
{
    public function search(BulkOperationsQuery $query): AnonymousResourceCollection
    {
        return BulkOperationsResource::collectPage($query->get());
    }

    public function meta(
        BulkOperationActionEnumInfo $actions,
        BulkOperationEntityEnumInfo $entities,
        BulkOperationStatusEnumInfo $statuses,
    ): ModelMetaResource {
        return new ModelMetaResource([
            Field::id()->listDefault()->detailLink()->filterDefault(),
            Field::enum('action', 'Действие', $actions)->listDefault()->filterDefault(),
            Field::enum('entity', 'Сущность', $entities)->listDefault()->filterDefault(),
            Field::enum('status', 'Статус', $statuses)->listDefault()->filterDefault(),
            Field::string('ids_string', 'Обрабатываемые сущ-ти'),
            Field::string('created_by', 'Создано')->listDefault(),
            Field::datetime('created_at', 'Дата создания')->listDefault(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault(),
        ]);
    }
}
