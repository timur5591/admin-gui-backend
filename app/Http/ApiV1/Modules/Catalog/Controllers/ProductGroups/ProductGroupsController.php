<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\ProductGroups;

use App\Domain\Catalog\Actions\ProductGroups\BindProductGroupProductsAction;
use App\Domain\Catalog\Actions\ProductGroups\CreateProductGroupAction;
use App\Domain\Catalog\Actions\ProductGroups\DeleteManyProductGroupsAction;
use App\Domain\Catalog\Actions\ProductGroups\DeleteProductGroupAction;
use App\Domain\Catalog\Actions\ProductGroups\PatchProductGroupAction;
use App\Domain\Catalog\Actions\ProductGroups\ReplaceProductGroupAction;
use App\Domain\Common\Data\Meta\Enum\Catalog\CategoryEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Catalog\ProductDraftEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\ProductGroups\ProductGroupsQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\ProductGroups\BindProductGroupProductsRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\ProductGroups\CreateProductGroupRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\ProductGroups\PatchProductGroupRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\ProductGroups\ReplaceProductGroupRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\ProductGroups\ProductGroupsBindResultResource;
use App\Http\ApiV1\Modules\Catalog\Resources\ProductGroups\ProductGroupsResource;
use App\Http\ApiV1\Support\Requests\MassDeleteRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductGroupsController
{
    public function create(
        CreateProductGroupRequest $request,
        CreateProductGroupAction  $action
    ): ProductGroupsResource {
        return new ProductGroupsResource($action->execute($request->validated()));
    }

    public function get(int $groupId, ProductGroupsQuery $query): ProductGroupsResource
    {
        return new ProductGroupsResource($query->find($groupId));
    }

    public function replace(
        int                       $groupId,
        ReplaceProductGroupRequest $request,
        ReplaceProductGroupAction $action
    ): ProductGroupsResource {
        return new ProductGroupsResource($action->execute($groupId, $request->validated()));
    }

    public function patch(
        int $groupId,
        PatchProductGroupRequest $request,
        PatchProductGroupAction $action
    ): ProductGroupsResource {
        return new ProductGroupsResource($action->execute($groupId, $request->validated()));
    }

    public function delete(int $groupId, DeleteProductGroupAction $action): EmptyResource
    {
        $action->execute($groupId);

        return new EmptyResource();
    }

    public function massDelete(MassDeleteRequest $request, DeleteManyProductGroupsAction $action): EmptyResource
    {
        $action->execute($request->getIds());

        return new EmptyResource();
    }

    public function bindProducts(
        int $id,
        BindProductGroupProductsRequest $request,
        BindProductGroupProductsAction $action
    ): ProductGroupsBindResultResource {
        return ProductGroupsBindResultResource::make($action->execute($id, $request->validated()));
    }

    public function search(ProductGroupsQuery $query): AnonymousResourceCollection
    {
        return ProductGroupsResource::collectPage($query->get());
    }

    public function searchOne(ProductGroupsQuery $query): ProductGroupsResource
    {
        return ProductGroupsResource::make($query->first());
    }

    public function meta(
        CategoryEnumInfo $categoryEnumInfo,
        ProductDraftEnumInfo $productDraftEnumInfo
    ): ModelMetaResource {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::text('name', 'Название')->listDefault()->filterDefault()->sort(),
            Field::integer('products_count', 'Количество товаров')->listDefault()->resetFilter()->resetSort(),
            Field::photo('main_product_image', 'Изображение главного товара')->listDefault(),
            Field::enum('category_id', 'Название категории', $categoryEnumInfo)->resetFilter(),
            Field::enum('main_product_id', 'Название главного товара', $productDraftEnumInfo)->resetFilter(),
            Field::text('product_barcode', 'Штрихкод входящего в склейку товара')->listHide(),
            Field::text('product_vendor_code', 'Артикул входящего в склейку товара')->listHide(),
            Field::boolean('is_active', 'Активность')->listDefault()->filterDefault(),

            Field::datetime('created_at', 'Дата создания')->listDefault(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault(),
        ]);
    }
}
