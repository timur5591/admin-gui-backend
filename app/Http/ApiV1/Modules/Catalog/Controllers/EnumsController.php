<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers;

use App\Http\ApiV1\Support\Controllers\Data\EnumData;
use App\Http\ApiV1\Support\Resources\EnumResource;
use Ensi\PimClient\Dto\ProductTypeEnum;
use Ensi\PimClient\Dto\PropertyTypeEnum;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class EnumsController
{
    public function productTypes(): AnonymousResourceCollection
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(ProductTypeEnum::getDescriptions()));
    }

    public function propertyTypes(): AnonymousResourceCollection
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(PropertyTypeEnum::getDescriptions()));
    }
}
