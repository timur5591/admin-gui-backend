<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\Products;

use App\Domain\Common\Data\Meta\Enum\Catalog\BrandEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Catalog\CategoryEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Catalog\ProductTypeEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\Products\ProductsPimQuery;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductsResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductsController
{
    public function get(int $productId, ProductsPimQuery $query): ProductsResource
    {
        return new ProductsResource($query->find($productId));
    }

    public function search(ProductsPimQuery $query): AnonymousResourceCollection
    {
        return ProductsResource::collectPage($query->get());
    }

    public function meta(
        CategoryEnumInfo $categoryEnumInfo,
        ProductTypeEnumInfo $productTypeEnumInfo,
        BrandEnumInfo $brandEnumInfo
    ): ModelMetaResource {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::keyword('name', 'Наименование')->listDefault()->sort(),
            Field::string('barcode', 'Штрихкод')->listDefault()->filter()->filterDefault(),
            Field::string('code', 'Код товара')->listDefault()->filter(),
            Field::photo('main_image_file', 'Изображение')->listDefault(),
            Field::string('vendor_code', 'Артикул')->listDefault()->filter()->filterDefault(),
            Field::enum('type', 'Тип товара', $productTypeEnumInfo)->listDefault()->filter(),
            Field::boolean('is_adult', 'Товар 18+')->listDefault()->resetSort(),
            Field::price('base_price', 'Стоимость')->listDefault()->resetFilter()->resetSort(),
            Field::boolean('allow_publish', 'Публикация разрешена')->listDefault()->filter()->resetSort(),
            Field::datetime('created_at', 'Дата создания')->listDefault(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault(),

            Field::enum('category_id', 'Категория', $categoryEnumInfo)->filter()->filterDefault(),
            Field::enum('brand_id', 'Бренд', $brandEnumInfo)->filter()->filterDefault(),
            Field::string('external_id', 'Внешний код товара')->filter(),
            Field::float('weight', 'Масса нетто, кг')->resetFilter()->resetSort(),
            Field::float('weight_gross', 'Масса брутто, кг')->resetFilter()->resetSort(),
            Field::float('length', 'Длина, мм')->resetFilter()->resetSort(),
            Field::float('width', 'Ширина, мм')->resetFilter()->resetSort(),
            Field::float('height', 'Высота, мм')->resetFilter()->resetSort(),
            Field::boolean(
                'has_no_filled_required_attributes',
                'Есть ли у товара хотя бы один незаполненный обязательный атрибут'
            )->listHide()->resetSort(),
        ]);
    }
}
