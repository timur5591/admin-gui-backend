<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\Classifiers;

use App\Domain\Catalog\Actions\Classifiers\Brands\CreateBrandAction;
use App\Domain\Catalog\Actions\Classifiers\Brands\DeleteBrandAction;
use App\Domain\Catalog\Actions\Classifiers\Brands\DeleteBrandImageAction;
use App\Domain\Catalog\Actions\Classifiers\Brands\PatchBrandAction;
use App\Domain\Catalog\Actions\Classifiers\Brands\PreloadBrandImageAction;
use App\Domain\Catalog\Actions\Classifiers\Brands\ReplaceBrandAction;
use App\Domain\Catalog\Actions\Classifiers\Brands\SaveBrandImageAction;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\Classifiers\BrandsQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Classifiers\CreateBrandRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Classifiers\PatchBrandRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Classifiers\PreloadBrandImageRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Classifiers\ReplaceBrandRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Classifiers\UploadBrandImageRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Classifiers\BrandEnumValuesResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Classifiers\BrandsResource;
use App\Http\ApiV1\Modules\Catalog\Resources\PreloadFileResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Exception;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class BrandsController
{
    public function search(BrandsQuery $query): AnonymousResourceCollection
    {
        return BrandsResource::collectPage($query->get());
    }

    public function searchEnumValues(BrandsQuery $query): AnonymousResourceCollection
    {
        return BrandEnumValuesResource::collection($query->searchEnums());
    }

    public function create(
        CreateBrandRequest $request,
        CreateBrandAction $action
    ): BrandsResource {
        return new BrandsResource($action->execute($request->validated()));
    }

    public function get(
        int $id,
        BrandsQuery $query
    ): BrandsResource {
        return new BrandsResource($query->find($id));
    }

    public function replace(
        int $id,
        ReplaceBrandRequest $request,
        ReplaceBrandAction $action
    ): BrandsResource {
        return new BrandsResource($action->execute($id, $request->validated()));
    }

    public function patch(
        int $id,
        PatchBrandRequest $request,
        PatchBrandAction $action
    ): BrandsResource {
        return new BrandsResource($action->execute($id, $request->validated()));
    }

    public function delete(
        int $id,
        DeleteBrandAction $action
    ): EmptyResource {
        $action->execute($id);

        return new EmptyResource();
    }

    public function preloadImage(
        PreloadBrandImageRequest $request,
        PreloadBrandImageAction $action
    ): PreloadFileResource {
        return new PreloadFileResource($action->execute($request->file('file')));
    }

    public function uploadImage(
        int $id,
        UploadBrandImageRequest $request,
        SaveBrandImageAction $action
    ): BrandsResource {
        return new BrandsResource($action->execute($id, $request->file('file')));
    }

    public function deleteImage(
        int $id,
        DeleteBrandImageAction $action
    ): BrandsResource {
        return new BrandsResource($action->execute($id));
    }

    /**
     * @throws Exception
     */
    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id(name: 'Идентификатор')
                ->listDefault()
                ->filterDefault()
                ->sortDefault(direction: 'desc')
                ->detailLink(),
            Field::text('name', 'Наименование')
                ->sort()
                ->filterDefault()
                ->listDefault(),
            Field::text('code', 'Код шаблона')
                ->sort()
                ->filterDefault()
                ->listDefault(),
            Field::text('description', 'Описание')
                ->filterDefault(),
            Field::boolean('is_active', 'Активность')
                ->sort()
                ->filterMany()
                ->filterDefault()
                ->listDefault(),
            Field::datetime('created_at', 'Дата создания')
                ->sort()
                ->filterDefault()
                ->listDefault(),
            Field::datetime('updated_at', 'Дата обновления')
                ->sort()
                ->filterDefault()
                ->listDefault(),
        ]);
    }
}
