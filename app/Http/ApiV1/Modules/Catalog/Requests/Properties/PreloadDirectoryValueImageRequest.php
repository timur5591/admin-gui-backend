<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Properties;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Http\UploadedFile;

class PreloadDirectoryValueImageRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'file' => ['required', 'image', 'max:10240', 'dimensions:min_width=20,min_height=20,max_width=3840,max_height=2160'],
        ];
    }

    public function image(): UploadedFile
    {
        return $this->file('file');
    }
}
