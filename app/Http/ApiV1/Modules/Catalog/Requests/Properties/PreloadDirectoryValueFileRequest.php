<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Properties;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PreloadDirectoryValueFileRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'file' => ['required', 'file', 'max:10240'],
        ];
    }
}
