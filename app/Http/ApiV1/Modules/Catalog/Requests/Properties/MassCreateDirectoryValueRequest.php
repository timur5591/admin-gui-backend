<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Properties;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class MassCreateDirectoryValueRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'items' => 'array|required',
            'items.*.value' => ['required_without:items.*.preload_file_id'],
            'items.*.name' => ['required', 'string'],
            'items.*.code' => ['nullable', 'string'],
            'items.*.preload_file_id' => ['sometimes', 'integer'],
        ];
    }
}
