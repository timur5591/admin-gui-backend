<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Products;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class GetProductsCommonAttributesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'filter' => ['sometimes', 'array'],
        ];
    }

    public function getFilter(): array
    {
        return $this->validated()['filter'] ?? [];
    }
}
