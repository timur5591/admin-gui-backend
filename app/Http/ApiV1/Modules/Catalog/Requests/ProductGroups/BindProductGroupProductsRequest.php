<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\ProductGroups;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class BindProductGroupProductsRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'replace' => ['sometimes', 'boolean'],
            'products' => ['sometimes', 'array'],
            'products.*.id' => [
                'required_without_all:products.*.vendor_code,products.*.barcode',
                'integer',
                'min:1',
            ],

            'products.*.vendor_code' => [
                'prohibits:products.*.id,products.*.barcode',
                'string',
            ],

            'products.*.barcode' => [
                'prohibits:products.*.id,products.*.vendor_code',
                'string',
            ],
        ];
    }
}
