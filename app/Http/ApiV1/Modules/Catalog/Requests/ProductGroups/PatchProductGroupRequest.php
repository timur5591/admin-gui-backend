<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\ProductGroups;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchProductGroupRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['sometimes', 'string'],
            'category_id' => ['sometimes', 'integer', 'min:1'],
            'is_active' => ['sometimes', 'boolean'],
            'main_product_id' => ['sometimes', 'nullable', 'integer', 'min:1'],
        ];
    }
}
