<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\ProductGroups;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateProductGroupRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'category_id' => ['required', 'integer', 'min:1'],
            'is_active' => ['required', 'boolean'],
            'main_product_id' => ['sometimes', 'nullable', 'integer', 'min:1'],
        ];
    }
}
