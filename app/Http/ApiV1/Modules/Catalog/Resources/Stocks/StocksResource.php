<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Stocks;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\OffersClient\Dto\Stock;

/**
 * @mixin Stock
 */
class StocksResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'store_id' => $this->getStoreId(),
            'product_id' => $this->getProductId(),
            'offer_id' => $this->getOfferId(),
            'qty' => $this->getQty(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),
        ];
    }
}
