<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\PimCommon;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\BulkOperation;

/** @mixin BulkOperation */
class BulkOperationsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'action' => $this->getAction(),
            'entity' => $this->getEntity(),
            'status' => $this->getStatus(),
            'ids' => $this->getIds(),
            'ids_string' => implode(', ', $this->getIds()),
            'error_message' => $this->getErrorMessage(),
            'created_by' => $this->getCreatedBy(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),

            'errors' => BulkOperationErrorsResource::collection($this->whenNotNull($this->getErrors())),
        ];
    }
}
