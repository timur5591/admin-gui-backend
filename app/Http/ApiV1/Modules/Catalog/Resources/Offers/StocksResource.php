<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Offers;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\OffersClient\Dto\Stock;

/**
 * @mixin Stock
 */
class StocksResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'store_id' => $this->getStoreId(),
            'offer_id' => $this->getOfferId(),
            'product_id' => $this->getProductId(),
            'qty' => $this->getQty(),
        ];
    }
}
