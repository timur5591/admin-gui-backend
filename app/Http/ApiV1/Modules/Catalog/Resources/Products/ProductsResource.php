<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Products;

class ProductsResource extends BaseProductsResource
{
    protected function extraFields(): array
    {
        return [
            'main_image_url' => $this->getMainImageUrl(),
            'main_image_file' => $this->getMainImageFile()?->getUrl(),
        ];
    }
}
