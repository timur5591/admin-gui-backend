<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Products;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\ProductImage;

/**
 * @mixin ProductImage
 */
class ProductImagesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'sort' => $this->getSort(),
            'name' => $this->getName(),
            'is_external' => $this->isExternal(),
            'url' => $this->getActualUrl(),
        ];
    }

    private function isExternal(): bool
    {
        return $this->getFile() === null;
    }

    private function getActualUrl(): ?string
    {
        $file = $this->getFile();

        return $file !== null ? $this->fileUrl($file) : $this->getUrl();
    }
}
