<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\ProductGroups;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\ProductGroupsBindResult;

/**
 * @mixin ProductGroupsBindResult
 */
class ProductGroupsBindResultResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'product_group' => new ProductGroupsResource($this->getProductGroup()),
            'product_errors' => $this->getProductErrors(),
        ];
    }
}
