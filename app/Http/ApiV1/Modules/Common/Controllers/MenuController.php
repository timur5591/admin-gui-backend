<?php

namespace App\Http\ApiV1\Modules\Common\Controllers;

use App\Http\ApiV1\Modules\Common\Queries\MenuItemsQuery;
use App\Http\ApiV1\Modules\Common\Resources\MenuResource;

/**
 * Class MenuController
 * @package App\Http\ApiV1\Modules\Menu\Controllers
 */
class MenuController
{
    public function menu(MenuItemsQuery $query): MenuResource
    {
        return new MenuResource($query->getMenuItems());
    }
}
