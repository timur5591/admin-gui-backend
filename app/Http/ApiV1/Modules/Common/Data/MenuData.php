<?php

namespace App\Http\ApiV1\Modules\Common\Data;

use App\Http\ApiV1\OpenApiGenerated\Enums\MenuItemCodeEnum;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;

/**
 * Class MenuData
 * @package App\Http\ApiV1\Modules\Menu\Data
 */
class MenuData
{
    /**
     * @return array[]
     */
    public static function menu(): array
    {
        return [
            //Товары
            MenuItemCodeEnum::PRODUCTS_CODE->value => [
                'items' => [
                    //Каталог товаров
                    MenuItemCodeEnum::PRODUCTS_CATALOG_CODE->value => [
                        'rightsAccess' => RightsAccessEnum::PRODUCT_CATALOG_LIST_READ,
                    ],
                    //Предложения продавцов
                    MenuItemCodeEnum::PRODUCTS_OFFERS_CODE->value => [],
                    //Товарные склейки
                    MenuItemCodeEnum::PRODUCTS_PRODUCT_GROUPS_CODE->value => [
                        'rightsAccess' => RightsAccessEnum::PRODUCT_GROUP_LIST_READ,
                    ],
                    //Справочники
                    MenuItemCodeEnum::PRODUCTS_DIRECTORIES_CODE->value => [
                        'items' => [
                            //Бренды
                            MenuItemCodeEnum::PRODUCTS_DIRECTORIES_BRANDS_CODE->value => [],
                            //Категории
                            MenuItemCodeEnum::PRODUCTS_DIRECTORIES_CATEGORIES_CODE->value => [
                                'rightsAccess' => RightsAccessEnum::PRODUCT_CATEGORY_LIST_READ,
                            ],
                            //Товарные атрибуты
                            MenuItemCodeEnum::PRODUCTS_DIRECTORIES_PROPERTIES_CODE->value => [
                                'rightsAccess' => RightsAccessEnum::PRODUCT_ATTRIBUTES_LIST_READ,
                            ],
                            //Страны
                            MenuItemCodeEnum::PRODUCTS_DIRECTORIES_COUNTRIES_CODE->value => [],
                            //Производители
                            MenuItemCodeEnum::PRODUCTS_DIRECTORIES_MANUFACTURERS_CODE->value => [],
                            //Типы товаров
                            MenuItemCodeEnum::PRODUCTS_DIRECTORIES_PRODUCT_TYPES_CODE->value => [],
                        ],
                    ],
                ],
            ],

            //Заказы
            MenuItemCodeEnum::ORDERS_CODE->value => [
                'items' => [
                    //Список заказов
                    MenuItemCodeEnum::ORDERS_LIST_CODE->value => [
                        'rightsAccess' => RightsAccessEnum::ORDER_LIST_READ,
                    ],
                    //Возвраты
                    MenuItemCodeEnum::ORDERS_CARGOS_CODE->value => [
                        'rightsAccess' => RightsAccessEnum::REFUND_LIST_READ,
                    ],
                    //Настройки
                    MenuItemCodeEnum::ORDERS_STATUSES_CODE->value => [
                        'rightsAccess' => RightsAccessEnum::SETTING_LIST_READ,
                    ],
                ],
            ],

            //Заявки
            MenuItemCodeEnum::REQUESTS_CODE->value => [
                'items' => [
                    //Проверка товаров
                    MenuItemCodeEnum::REQUESTS_CHECK_CODE->value => [],
                    //Производство контента
                    MenuItemCodeEnum::REQUESTS_CONTENT_CODE->value => [],
                    //Изменение цен
                    MenuItemCodeEnum::REQUESTS_PRICES_CODE->value => [],
                ],
            ],

            //Контент
            MenuItemCodeEnum::CONTENT_CODE->value => [
                'items' => [
                    //Меню сайта
                    MenuItemCodeEnum::CONTENT_MENU_CODE->value => [],
                    //Управление страницами
                    MenuItemCodeEnum::CONTENT_LANDING_CODE->value => [],
                    //Управление контактами и соц. сетями
                    MenuItemCodeEnum::CONTENT_CONTACTS_CODE->value => [],
                    //Управление категориями
                    MenuItemCodeEnum::CONTENT_CATEGORIES_CODE->value => [],
                    //Подборки товаров
                    MenuItemCodeEnum::CONTENT_PRODUCT_GROUPS_CODE->value => [],
                    //Баннеры
                    MenuItemCodeEnum::CONTENT_BANNERS_CODE->value => [],
                    //Страницы
                    MenuItemCodeEnum::CONTENT_PAGES_CODE->value => [
                        'rightsAccess' => RightsAccessEnum::PAGE_LIST_READ,
                    ],
                    //Товарные шильдики
                    MenuItemCodeEnum::CONTENT_BADGES_CODE->value => [],
                    //Поисковые запросы
                    MenuItemCodeEnum::CONTENT_SEARCH_REQUESTS_CODE->value => [],
                    //Поисковые синонимы
                    MenuItemCodeEnum::CONTENT_SEARCH_SYNONYMS_CODE->value => [],
                    //Популярные бренды
                    MenuItemCodeEnum::CONTENT_POPULAR_BRANDS_CODE->value => [],
                    //Популярные товары
                    MenuItemCodeEnum::CONTENT_POPULAR_PRODUCTS_CODE->value => [],
                ],
            ],

            //Логистика
            MenuItemCodeEnum::LOGISTIC_CODE->value => [
                'items' => [
                    //Логистические операторы
                    MenuItemCodeEnum::LOGISTIC_DELIVERY_SERVICES_CODE->value => [],
                    //Стоимость доставки по регионам
                    MenuItemCodeEnum::LOGISTIC_DELIVERY_PRICES_CODE->value => [],
                    //Планировщик времени статусов
                    MenuItemCodeEnum::LOGISTIC_KPI_CODE->value => [],
                ],
            ],

            //Склады
            MenuItemCodeEnum::STORES_SELLER_STORES_CODE->value => [
            ],

            //Клиенты
            MenuItemCodeEnum::CUSTOMERS_CODE->value => [
            ],

            //Продавцы
            MenuItemCodeEnum::SELLER_CODE->value => [
                'items' => [
                    //Заявка на регистрацию
                    MenuItemCodeEnum::SELLER_LIST_REGISTRATION_CODE->value => [],
                    //Список продавцов
                    MenuItemCodeEnum::SELLER_LIST_ACTIVE_CODE->value => [],
                ],
            ],

            //Маркетинг
            MenuItemCodeEnum::MARKETING_CODE->value => [
                'items' => [
                    //Скидки
                    MenuItemCodeEnum::MARKETING_DISCOUNTS_CODE->value => [
                        'rightsAccess' => RightsAccessEnum::PROMO_CODE_LIST_READ,
                    ],
                    //Промокоды
                    MenuItemCodeEnum::MARKETING_PROMO_CODES_CODE->value => [
                        'rightsAccess' => RightsAccessEnum::DISCOUNT_LIST_READ,
                    ],
                ],
            ],

            //Отчеты
            MenuItemCodeEnum::REPORTS_CODE->value => [
            ],

            //Коммуникации
            MenuItemCodeEnum::COMMUNICATIONS_CODE->value => [
                'items' => [
                    //Непрочитанные сообщения
                    MenuItemCodeEnum::COMMUNICATIONS_MESSAGES_CODE->value => [],
                    //Сервисные уведомления
                    MenuItemCodeEnum::COMMUNICATIONS_NOTIFICATIONS_CODE->value => [],
                    //Массовая рассылка
                    MenuItemCodeEnum::COMMUNICATIONS_BROADCAST_CODE->value => [],
                    //Статусы
                    MenuItemCodeEnum::COMMUNICATIONS_STATUSES_CODE->value => [],
                    //Темы
                    MenuItemCodeEnum::COMMUNICATIONS_SUBJECTS_CODE->value => [],
                    //Типы
                    MenuItemCodeEnum::COMMUNICATIONS_TYPES_CODE->value => [],
                ],
            ],

            //Настройки
            MenuItemCodeEnum::SETTINGS_CODE->value => [
                'items' => [
                    //Пользователи и права
                    MenuItemCodeEnum::SETTINGS_USERS_CODE->value => [
                        'rightsAccess' => RightsAccessEnum::USER_LIST_READ,
                    ],
                    //Карточка организации
                    MenuItemCodeEnum::SETTINGS_ORGANIZATIONS_CODE->value => [],
                    //Роли пользователей
                    MenuItemCodeEnum::SETTINGS_USERS_ROLES->value => [
                        'rightsAccess' => RightsAccessEnum::ROLE_LIST_READ,
                    ],
                ],
            ],
        ];
    }
}
