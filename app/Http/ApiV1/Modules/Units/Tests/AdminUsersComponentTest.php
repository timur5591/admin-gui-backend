<?php

use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

//test('POST /api/v1/units/admin-users:search 200', function () {
//    postJson('/api/v1/units/admin-users:search')
//        ->assertStatus(200);
//});
//
//test('POST /api/v1/units/admin-users:search 401', function () {
//    postJson('/api/v1/units/admin-users:search')
//        ->assertStatus(401);
//});
//
//test('POST /api/v1/units/admin-users:search 400', function () {
//    postJson('/api/v1/units/admin-users:search')
//        ->assertStatus(400);
//});
//
//test('POST /api/v1/units/admin-users:search 403', function () {
//    postJson('/api/v1/units/admin-users:search')
//        ->assertStatus(403);
//});
//
//test('POST /api/v1/units/admin-user-enum-values:search 200', function () {
//    postJson('/api/v1/units/admin-user-enum-values:search')
//        ->assertStatus(200);
//});
//
//test('POST /api/v1/units/admin-user-enum-values:search 401', function () {
//    postJson('/api/v1/units/admin-user-enum-values:search')
//        ->assertStatus(401);
//});
//
//test('POST /api/v1/units/admin-users 200', function () {
//    postJson('/api/v1/units/admin-users')
//        ->assertStatus(200);
//});
//
//test('POST /api/v1/units/admin-users 400', function () {
//    postJson('/api/v1/units/admin-users')
//        ->assertStatus(400);
//});
//
//test('POST /api/v1/units/admin-users 401', function () {
//    postJson('/api/v1/units/admin-users')
//        ->assertStatus(401);
//});
//
//test('POST /api/v1/units/admin-users 403', function () {
//    postJson('/api/v1/units/admin-users')
//        ->assertStatus(403);
//});
//
//test('GET /api/v1/units/admin-users/{id} 200', function () {
//    getJson('/api/v1/units/admin-users/{id}')
//        ->assertStatus(200);
//});
//
//test('GET /api/v1/units/admin-users/{id} 401', function () {
//    getJson('/api/v1/units/admin-users/{id}')
//        ->assertStatus(401);
//});
//
//test('GET /api/v1/units/admin-users/{id} 400', function () {
//    getJson('/api/v1/units/admin-users/{id}')
//        ->assertStatus(400);
//});
//
//test('GET /api/v1/units/admin-users/{id} 403', function () {
//    getJson('/api/v1/units/admin-users/{id}')
//        ->assertStatus(403);
//});
//
//test('GET /api/v1/units/admin-users/{id} 404', function () {
//    getJson('/api/v1/units/admin-users/{id}')
//        ->assertStatus(404);
//});
//
//test('PATCH /api/v1/units/admin-users/{id} 200', function () {
//    patchJson('/api/v1/units/admin-users/{id}')
//        ->assertStatus(200);
//});
//
//test('PATCH /api/v1/units/admin-users/{id} 400', function () {
//    patchJson('/api/v1/units/admin-users/{id}')
//        ->assertStatus(400);
//});
//
//test('PATCH /api/v1/units/admin-users/{id} 401', function () {
//    patchJson('/api/v1/units/admin-users/{id}')
//        ->assertStatus(401);
//});
//
//test('PATCH /api/v1/units/admin-users/{id} 403', function () {
//    patchJson('/api/v1/units/admin-users/{id}')
//        ->assertStatus(403);
//});
//
//test('PATCH /api/v1/units/admin-users/{id} 404', function () {
//    patchJson('/api/v1/units/admin-users/{id}')
//        ->assertStatus(404);
//});
//
//test('POST /api/v1/units/admin-users/{id}:add-roles 200', function () {
//    postJson('/api/v1/units/admin-users/{id}:add-roles')
//        ->assertStatus(200);
//});
//
//test('POST /api/v1/units/admin-users/{id}:add-roles 401', function () {
//    postJson('/api/v1/units/admin-users/{id}:add-roles')
//        ->assertStatus(401);
//});
//
//test('POST /api/v1/units/admin-users/{id}:add-roles 400', function () {
//    postJson('/api/v1/units/admin-users/{id}:add-roles')
//        ->assertStatus(400);
//});
//
//test('POST /api/v1/units/admin-users/{id}:add-roles 403', function () {
//    postJson('/api/v1/units/admin-users/{id}:add-roles')
//        ->assertStatus(403);
//});
//
//test('POST /api/v1/units/admin-users/{id}:delete-role 200', function () {
//    postJson('/api/v1/units/admin-users/{id}:delete-role')
//        ->assertStatus(200);
//});
//
//test('POST /api/v1/units/admin-users/{id}:delete-role 401', function () {
//    postJson('/api/v1/units/admin-users/{id}:delete-role')
//        ->assertStatus(401);
//});
//
//test('POST /api/v1/units/admin-users/{id}:delete-role 400', function () {
//    postJson('/api/v1/units/admin-users/{id}:delete-role')
//        ->assertStatus(400);
//});
//
//test('POST /api/v1/units/admin-users/{id}:delete-role 403', function () {
//    postJson('/api/v1/units/admin-users/{id}:delete-role')
//        ->assertStatus(403);
//});
//
//test('POST /api/v1/units/admin-users/{id}:refresh-password-token 200', function () {
//    postJson('/api/v1/units/admin-users/{id}:refresh-password-token')
//        ->assertStatus(200);
//});
//
//test('POST /api/v1/units/admin-users/{id}:refresh-password-token 400', function () {
//    postJson('/api/v1/units/admin-users/{id}:refresh-password-token')
//        ->assertStatus(400);
//});
//
//test('POST /api/v1/units/admin-users/{id}:refresh-password-token 401', function () {
//    postJson('/api/v1/units/admin-users/{id}:refresh-password-token')
//        ->assertStatus(401);
//});
//
//test('POST /api/v1/units/admin-users/{id}:refresh-password-token 403', function () {
//    postJson('/api/v1/units/admin-users/{id}:refresh-password-token')
//        ->assertStatus(403);
//});
//
//test('POST /api/v1/units/admin-users/{id}:refresh-password-token 404', function () {
//    postJson('/api/v1/units/admin-users/{id}:refresh-password-token')
//        ->assertStatus(404);
//});
//
//test('POST /api/v1/units/admin-users/mass/change-active 200', function () {
//    postJson('/api/v1/units/admin-users/mass/change-active')
//        ->assertStatus(200);
//});
//
//test('POST /api/v1/units/admin-users/mass/change-active 400', function () {
//    postJson('/api/v1/units/admin-users/mass/change-active')
//        ->assertStatus(400);
//});
//
//test('POST /api/v1/units/admin-users/mass/change-active 401', function () {
//    postJson('/api/v1/units/admin-users/mass/change-active')
//        ->assertStatus(401);
//});
//
//test('POST /api/v1/units/admin-users/mass/change-active 403', function () {
//    postJson('/api/v1/units/admin-users/mass/change-active')
//        ->assertStatus(403);
//});
//
//test('POST /api/v1/units/admin-users/mass/change-active 404', function () {
//    postJson('/api/v1/units/admin-users/mass/change-active')
//        ->assertStatus(404);
//});
//
//test('POST /api/v1/units/admin-users/set-password 200', function () {
//    postJson('/api/v1/units/admin-users/set-password')
//        ->assertStatus(200);
//});
//
//test('POST /api/v1/units/admin-users/set-password 400', function () {
//    postJson('/api/v1/units/admin-users/set-password')
//        ->assertStatus(400);
//});
//
//test('POST /api/v1/units/admin-users/set-password 401', function () {
//    postJson('/api/v1/units/admin-users/set-password')
//        ->assertStatus(401);
//});
//
//test('POST /api/v1/units/admin-users/set-password 403', function () {
//    postJson('/api/v1/units/admin-users/set-password')
//        ->assertStatus(403);
//});
//
//test('POST /api/v1/units/admin-users/set-password 404', function () {
//    postJson('/api/v1/units/admin-users/set-password')
//        ->assertStatus(404);
//});
