<?php

namespace App\Http\ApiV1\Modules\Units\Tests\AdminUsers\Factories;

use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class AdminUserRoleRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'active' => $this->faker->boolean(),
            'title' => $this->faker->title(),
            'rights_access' => $this->faker->randomElements(
                RightsAccessEnum::getAllowableEnumValues(),
                $this->faker->numberBetween(1, 3)
            ),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
