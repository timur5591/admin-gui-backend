<?php

use App\Domain\Units\Tests\Factories\AdminUserFactory;
use App\Domain\Units\Tests\Factories\AdminUserRoleFactory;
use App\Domain\Units\Tests\Factories\RightsAccessFactory;
use App\Http\ApiV1\Modules\Units\Tests\AdminUsers\Factories\AdminUserRequestFactory;
use App\Http\ApiV1\Modules\Units\Tests\AdminUsers\Factories\AdminUserRoleRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\AdminAuthClient\Dto\EmptyDataResponse;
use Ensi\AdminAuthClient\Dto\RoleEnum;
use Ensi\LaravelTestFactories\PaginationFactory;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/units/admin-users:search 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $this->mockAdminAuthUsersApi()->allows([
        'searchUsers' => $usersSearchResponse = AdminUserFactory::new()->makeResponseSearch(),
    ]);

    postJson("/api/v1/units/admin-users:search", ['pagination' => PaginationFactory::new()->makeRequestOffset()])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $usersSearchResponse->getData()[0]->getId());
});

test('POST /api/v1/units/admin-user-enum-values:search 200', function ($key, $value) {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockAdminAuthUsersApi()->allows([
        'searchUsers' => AdminUserFactory::new()->makeResponseSearch(),
    ]);

    postJson("/api/v1/units/admin-user-enum-values:search", ['filter' => [$key => $value]])->assertStatus(200);
})->with([
    ['id', [1, 2]],
    ['query', 'name'],
]);

test('POST /api/v1/units/admin-users 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $userData = AdminUserRequestFactory::new()->make();
    $this->mockAdminAuthUsersApi()->allows([
        'createUser' => AdminUserFactory::new()->makeResponse($userData),
    ]);

    postJson("/api/v1/units/admin-users", $userData)
        ->assertStatus(200)
        ->assertJsonPath('data.email', $userData['email']);
});

test('GET /api/v1/units/admin-users/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $userData = AdminUserRequestFactory::new()->make(['id' => 1]);
    $this->mockAdminAuthUsersApi()->allows([
        'getUser' => AdminUserFactory::new()->makeResponse($userData),
    ]);

    getJson("/api/v1/units/admin-users/{$userData['id']}")
        ->assertStatus(200)
        ->assertJsonPath('data.email', $userData['email'])
        ->assertJsonPath('data.id', $userData['id']);
});

test('PATCH /api/v1/units/admin-users/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $userData = AdminUserRequestFactory::new()->make([
        'id' => 1,
        'active' => true,
    ]);
    $this->mockAdminAuthUsersApi()->allows([
        'patchUser' => AdminUserFactory::new()->makeResponse($userData),
    ]);

    patchJson("/api/v1/units/admin-users/{$userData['id']}", $userData)
        ->assertStatus(200)
        ->assertJsonPath('data.email', $userData['email'])
        ->assertJsonPath('data.id', $userData['id']);
});

test('POST /api/v1/units/admin-users/{id}:add-roles 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockAdminAuthUsersApi()->shouldReceive('addRolesToUser');

    $addRolesData = [
        'roles' => [RoleEnum::ADMIN],
    ];

    postJson("/api/v1/units/admin-users/1:add-roles", $addRolesData)
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test('POST /api/v1/units/admin-users/{id}:delete-role 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockAdminAuthUsersApi()->shouldReceive('deleteRoleFromUser');

    $deleteRoleData = [
        'role_id' => RoleEnum::ADMIN,
    ];

    postJson("/api/v1/units/admin-users/1:delete-role", $deleteRoleData)
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test('POST /api/v1/units/admin-users/{id}:refresh-password-token 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockAdminAuthUsersApi()->shouldReceive('refreshPasswordToken');

    postJson("/api/v1/units/admin-users/1:refresh-password-token")
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test('POST /api/v1/units/admin-users/mass/change-active mass activate 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockAdminAuthUsersApi()->shouldReceive('massChangeActive');

    $massChangeActiveData = [
        'user_ids' => [1, 2, 3],
        'active' => true,
    ];

    postJson("/api/v1/units/admin-users/mass/change-active", $massChangeActiveData)
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test('POST /api/v1/units/admin-users/mass/change-active mass deactivate 400', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockAdminAuthUsersApi()->shouldReceive('massChangeActive');

    $massChangeActiveData = [
        'user_ids' => [1, 2, 3],
        'active' => false,
    ];

    postJson("/api/v1/units/admin-users/mass/change-active", $massChangeActiveData)
        ->assertStatus(400)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "ValidationError");
});

test('POST /api/v1/units/admin-users/mass/change-active mass deactivate 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockAdminAuthUsersApi()->shouldReceive('massChangeActive');

    $massChangeActiveData = [
        'user_ids' => [1, 2, 3],
        'active' => false,
        'cause_deactivation' => 'Cause Deactivation',
    ];

    postJson("/api/v1/units/admin-users/mass/change-active", $massChangeActiveData)
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test('POST /api/v1/units/admin-users/set-password 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $this->mockAdminAuthUsersApi()->allows([
        'patchUser' => $userResponse = AdminUserFactory::new()->makeResponse(),
        'searchUsers' => AdminUserFactory::new()->makeResponseSearch([['id' => $userResponse->getData()->getId()]]),
    ]);

    $setPasswordData = [
        'token' => 'token',
        'password' => 'new password',
    ];

    postJson("/api/v1/units/admin-users/set-password", $setPasswordData)
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test('GET /api/v1/units/admin-users:meta success', function () {
    getJson('/api/v1/units/admin-users:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});

test('POST /api/v1/units/admin-user-roles:search 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $this->mockAdminAuthUserRolesApi()->allows([
        'searchRoles' => $rolesSearchResponse = AdminUserRoleFactory::new()->makeResponseSearch(),
    ]);

    postJson("/api/v1/units/admin-user-roles:search", ['pagination' => PaginationFactory::new()->makeRequestOffset()])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $rolesSearchResponse->getData()[0]->getId());
});

test('POST /api/v1/units/admin-user-roles 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $userRoleData = AdminUserRoleRequestFactory::new()->make();

    $this->mockAdminAuthUserRolesApi()->allows([
        'createRole' => AdminUserRoleFactory::new()->makeResponse($userRoleData),
    ]);

    postJson("/api/v1/units/admin-user-roles", $userRoleData)
        ->assertStatus(200)
        ->assertJsonPath('data.title', $userRoleData['title']);
});

test('GET /api/v1/units/admin-user-roles/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $userRoleData = AdminUserRoleRequestFactory::new()->make(['id' => 1]);

    $this->mockAdminAuthUserRolesApi()->allows([
        'getRole' => AdminUserRoleFactory::new()->makeResponse($userRoleData),
    ]);

    getJson("/api/v1/units/admin-user-roles/{$userRoleData['id']}")
        ->assertStatus(200)
        ->assertJsonPath('data.title', $userRoleData['title'])
        ->assertJsonPath('data.id', $userRoleData['id']);
});

test('PATCH /api/v1/units/admin-user-roles/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $userRoleData = AdminUserRoleRequestFactory::new()->make([
        'id' => 1,
        'active' => true,
    ]);

    $this->mockAdminAuthUserRolesApi()->allows([
        'patchRole' => AdminUserRoleFactory::new()->makeResponse($userRoleData),
    ]);

    patchJson("/api/v1/units/admin-user-roles/{$userRoleData['id']}", $userRoleData)
        ->assertStatus(200)
        ->assertJsonPath('data.title', $userRoleData['title'])
        ->assertJsonPath('data.id', $userRoleData['id']);
});

test('DELETE /api/v1/units/admin-user-roles/{id} success', function () {
    $this->mockAdminAuthUserRolesApi()->allows(['deleteRole' => new EmptyDataResponse()]);

    deleteJson('/api/v1/units/admin-user-roles/1')
        ->assertOk();
});

test('GET /api/v1/units/admin-user-roles:meta success', function () {
    getJson('/api/v1/units/admin-user-roles:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});


test('GET /api/v1/units/admin-users/right-access 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $this->mockAdminAuthEnumsApi()->allows([
        'getRightsAccess' => RightsAccessFactory::new()->makeResponseGetRightsAccess(),
    ]);

    getJson('/api/v1/units/admin-users/right-access')
        ->assertOk()
        ->assertJsonStructure([
            'data' => [
                ['section', 'items'],
            ],
        ]);
});
