<?php

namespace App\Http\ApiV1\Modules\Units\Controllers;

use App\Domain\Units\Actions\SellerUsers\AddRolesToSellerUserAction;
use App\Domain\Units\Actions\SellerUsers\CreateSellerUserAction;
use App\Domain\Units\Actions\SellerUsers\DeleteRoleFromSellerUserAction;
use App\Domain\Units\Actions\SellerUsers\PatchSellerUserAction;
use App\Http\ApiV1\Modules\Units\Queries\SellerUsers\SellerUsersQuery;
use App\Http\ApiV1\Modules\Units\Requests\SellerUsers\AddRolesToSellerUserRequest;
use App\Http\ApiV1\Modules\Units\Requests\SellerUsers\CreateOrReplaceSellerUserRequest;
use App\Http\ApiV1\Modules\Units\Requests\SellerUsers\DeleteRoleFromSellerUserRequest;
use App\Http\ApiV1\Modules\Units\Requests\SellerUsers\PatchSellerUserRequest;
use App\Http\ApiV1\Modules\Units\Resources\SellerUsersResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class SellerUsersController
{
    public function search(SellerUsersQuery $query): AnonymousResourceCollection
    {
        return SellerUsersResource::collectPage($query->get());
    }

    public function create(CreateOrReplaceSellerUserRequest $request, CreateSellerUserAction $action): SellerUsersResource
    {
        return new SellerUsersResource($action->execute($request->validated()));
    }

    public function get(int $sellerUserId, SellerUsersQuery $query): SellerUsersResource
    {
        return new SellerUsersResource($query->findOrFail($sellerUserId));
    }

    public function patch(
        int $sellerUserId,
        PatchSellerUserRequest $request,
        PatchSellerUserAction $action
    ): SellerUsersResource {
        return new SellerUsersResource($action->execute($sellerUserId, $request->validated()));
    }

    public function addRoles(
        int $sellerUserId,
        AddRolesToSellerUserRequest $request,
        AddRolesToSellerUserAction $action
    ): EmptyResource {
        $action->execute($sellerUserId, $request->validated());

        return new EmptyResource();
    }

    public function deleteRole(
        int $sellerUserId,
        DeleteRoleFromSellerUserRequest $request,
        DeleteRoleFromSellerUserAction $action
    ): EmptyResource {
        $action->execute($sellerUserId, $request->validated());

        return new EmptyResource();
    }
}
