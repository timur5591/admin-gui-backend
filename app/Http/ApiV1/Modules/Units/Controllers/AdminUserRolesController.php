<?php

namespace App\Http\ApiV1\Modules\Units\Controllers;

use App\Domain\Common\Data\Meta\Field;
use App\Domain\Units\Actions\AdminUsers\CreateAdminUserRoleAction;
use App\Domain\Units\Actions\AdminUsers\DeleteAdminUserRoleAction;
use App\Domain\Units\Actions\AdminUsers\GetRightsAccessAction;
use App\Domain\Units\Actions\AdminUsers\PatchAdminUserRoleAction;
use App\Http\ApiV1\Modules\Units\Queries\AdminUserRolesQuery;
use App\Http\ApiV1\Modules\Units\Requests\AdminUsers\CreateAdminUserRoleRequest;
use App\Http\ApiV1\Modules\Units\Requests\AdminUsers\PatchAdminUserRoleRequest;
use App\Http\ApiV1\Modules\Units\Resources\AdminUserRolesResource;
use App\Http\ApiV1\Modules\Units\Resources\RightsAccessDictionaryResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class AdminUserRolesController
{
    public function create(
        CreateAdminUserRoleRequest $request,
        CreateAdminUserRoleAction $action
    ): AdminUserRolesResource {
        return AdminUserRolesResource::make($action->execute($request->validated()));
    }

    public function patch(
        int $id,
        PatchAdminUserRoleRequest $request,
        PatchAdminUserRoleAction $action
    ): AdminUserRolesResource {
        return AdminUserRolesResource::make($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteAdminUserRoleAction $action)
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, AdminUserRolesQuery $query): AdminUserRolesResource
    {
        return AdminUserRolesResource::make($query->find($id));
    }

    public function search(AdminUserRolesQuery $query): AnonymousResourceCollection
    {
        return AdminUserRolesResource::collectPage($query->get());
    }

    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::boolean('active', 'Активность')->listDefault()->filterDefault()->resetSort(),
            Field::text('title', 'Наименование')->listDefault()->filterDefault()->sortDefault(),
            Field::datetime('created_at', 'Дата создания')->listDefault()->filterDefault(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault()->filterDefault(),
        ]);
    }

    public function getRightsAccess(GetRightsAccessAction $action): AnonymousResourceCollection
    {
        return RightsAccessDictionaryResource::collection($action->execute());
    }
}
