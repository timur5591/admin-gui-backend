<?php

namespace App\Http\ApiV1\Modules\Units\Queries\SellerUsers;

use App\Domain\Units\Data\SellerUserData;
use App\Http\ApiV1\Modules\Units\Queries\UnitsQuery;
use Ensi\AdminAuthClient\Api\UsersApi;
use Ensi\AdminAuthClient\ApiException as AdminAuthApiException;
use Ensi\AdminAuthClient\Dto\PaginationTypeEnum as PaginationTypeEnumAdminAuth;
use Ensi\AdminAuthClient\Dto\RequestBodyPagination as RequestBodyPaginationAdminAuth;
use Ensi\AdminAuthClient\Dto\SearchUsersRequest;
use Ensi\BuClient\Api\OperatorsApi;
use Ensi\BuClient\ApiException as BuApiException;
use Ensi\BuClient\Dto\Operator;
use Ensi\BuClient\Dto\OperatorResponse;
use Ensi\BuClient\Dto\SearchOperatorsRequest;
use Ensi\BuClient\Dto\SearchOperatorsResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class SellerUsersQuery extends UnitsQuery
{
    protected bool $withRoles = false;

    public function __construct(
        protected Request $request,
        protected OperatorsApi $operatorsApi,
        protected UsersApi $usersApi
    ) {
        parent::__construct($request, SearchOperatorsRequest::class);
    }

    /**
     * @throws BuApiException
     */
    protected function searchById($operatorId): OperatorResponse
    {
        return $this->operatorsApi->getOperator($operatorId);
    }

    /**
     * @throws BuApiException
     */
    protected function search($request): SearchOperatorsResponse
    {
        return $this->operatorsApi->searchOperators($request);
    }

    /**
     * @param $response
     * @return array
     */
    protected function convertGetToItems($response): array
    {
        /** @var Collection|Operator[] $operators */
        $operators = collect($response->getData());
        $users = $this->loadUsers($operators->pluck('user_id')->unique()->all());
        $operatorsData = [];
        foreach ($operators as $operator) {
            $operatorData = new SellerUserData($operator);

            $user = $users->get($operator->getUserId());
            if ($user) {
                $operatorData->setUser($user);
            }
            $operatorsData[] = $operatorData;
        }

        return $operatorsData;
    }

    protected function getHttpInclude(): array
    {
        $httpIncludes = parent::getHttpInclude();
        $includes = [];
        foreach ($httpIncludes as $include) {
            switch ($include) {
                case 'roles':
                    $this->withRoles();

                    break;
                default:
                    $includes[] = $include;
            }
        }

        return $includes;
    }

    public function withRoles(): self
    {
        $this->withRoles = true;

        return $this;
    }

    /**
     * @param  array  $userIds
     * @return Collection
     * @throws AdminAuthApiException
     */
    protected function loadUsers(array $userIds): Collection
    {
        if (!$userIds) {
            return collect();
        }

        $request = new SearchUsersRequest();
        $request->setFilter((object)[
            'id' => $userIds,
        ]);

        if ($this->withRoles) {
            $request->setInclude(['roles']);
        }

        $request->setPagination(
            (new RequestBodyPaginationAdminAuth())
                ->setLimit(count($userIds))
                ->setType(PaginationTypeEnumAdminAuth::CURSOR)
        );
        $response = $this->usersApi->searchUsers($request);

        return collect($response->getData())->keyBy('id');
    }

    /**
     * @param  int  $id
     * @return SellerUserData
     * @throws BuApiException
     * @throws AdminAuthApiException
     */
    public function findOrFail(int $id): SellerUserData
    {
        $operator = new SellerUserData($this->operatorsApi->getOperator($id, $this->getInclude())->getData());
        $users = $this->loadUsers([$operator->operator->getUserId()]);
        if ($user = $users->get($operator->operator->getUserId())) {
            $operator->setUser($user);
        }

        return $operator;
    }
}
