<?php

namespace App\Http\ApiV1\Modules\Units\Queries\Sellers;

use App\Domain\Units\Data\SellerData;
use App\Domain\Units\Data\SellerUserData;
use App\Http\ApiV1\Modules\Units\Queries\UnitsQuery;
use Ensi\AdminAuthClient\Api\UsersApi;
use Ensi\AdminAuthClient\Dto\PaginationTypeEnum as PaginationTypeEnumAdminAuth;
use Ensi\AdminAuthClient\Dto\RequestBodyPagination as RequestBodyPaginationAdminAuth;
use Ensi\AdminAuthClient\Dto\SearchUsersRequest;
use Ensi\BuClient\Api\OperatorsApi;
use Ensi\BuClient\Api\SellersApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\PaginationTypeEnum as PaginationTypeEnumBu;
use Ensi\BuClient\Dto\RequestBodyPagination as RequestBodyPaginationBu;
use Ensi\BuClient\Dto\SearchOperatorsRequest;
use Ensi\BuClient\Dto\SearchSellersRequest;
use Ensi\BuClient\Dto\SearchSellersResponse;
use Ensi\BuClient\Dto\Seller;
use Ensi\BuClient\Dto\SellerResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class SellersQuery extends UnitsQuery
{
    protected bool $withOwner = false;
    protected bool $withManager = false;

    public function __construct(
        Request $request,
        private SellersApi $sellersApi,
        private OperatorsApi $operatorsApi,
        private UsersApi $usersApi
    ) {
        parent::__construct($request, SearchSellersRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($sellerId): SellerResponse
    {
        return $this->sellersApi->getSeller($sellerId, $this->getInclude());
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchSellersResponse
    {
        return $this->sellersApi->searchSellers($request);
    }

    protected function convertGetToItems($response)
    {
        return $this->convertArray($response->getData());
    }

    protected function convertFindToItem($response)
    {
        return current($this->convertArray([$response->getData()]));
    }

    protected function convertArray(array $sellers): array
    {
        /** @var Collection|Seller[] $sellers */
        $sellers = collect($sellers);
        $owners = $this->loadOwners($sellers->pluck('id')->all());
        $managers = $this->loadManagers($sellers->pluck('manager_id')->all());
        $sellersData = [];
        foreach ($sellers as $seller) {
            $sellerData = new SellerData($seller);
            $owner = $owners->get($seller->getId());

            if ($owner) {
                $sellerData->owner = $owner;
            }

            $manager = $managers->get($seller->getManagerId());
            if ($manager) {
                $sellerData->manager = $manager;
            }
            $sellersData[] = $sellerData;
        }

        return $sellersData;
    }

    protected function getHttpInclude(): array
    {
        $httpIncludes = parent::getHttpInclude();

        $includes = [];
        foreach ($httpIncludes as $include) {
            switch ($include) {
                case 'owner':
                    $this->withOwner();

                    break;
                case 'manager':
                    $this->withManager();

                    break;
                default:
                    $includes[] = $include;
            }
        }

        return $includes;
    }

    public function withOwner(): self
    {
        $this->withOwner = true;

        return $this;
    }

    public function withManager(): self
    {
        $this->withManager = true;

        return $this;
    }

    protected function loadOwners(array $sellerIds): Collection
    {
        if (!$this->withOwner || !$sellerIds) {
            return collect();
        }
        $operators = $this->loadOperators($sellerIds);
        $users = $this->loadUsers($operators->pluck('user_id')->unique()->all());
        $ownersData = new Collection();
        foreach ($operators as $operator) {
            $ownerData = new SellerUserData($operator);
            $user = $users->get($operator->getUserId());
            if ($user) {
                $ownerData->setUser($user);
            }
            $ownersData->push($ownerData);
        }

        return $ownersData->keyBy(function ($item) {
            return $item->operator->getSellerId();
        });
    }

    protected function loadOperators(array $sellerIds): Collection
    {
        $request = new SearchOperatorsRequest();
        $request->setFilter((object)[
            'seller_id' => $sellerIds,
            'is_main' => true,
        ]);
        $request->setPagination(
            (new RequestBodyPaginationBu())
                ->setLimit(count($sellerIds))
                ->setType(PaginationTypeEnumBu::CURSOR)
        );
        $response = $this->operatorsApi->searchOperators($request);

        return collect($response->getData());
    }

    protected function loadUsers(array $userIds): Collection
    {
        if (!$userIds) {
            return collect();
        }

        $request = new SearchUsersRequest();
        $request->setFilter((object)[
            'id' => $userIds,
        ]);

        $request->setInclude(['roles']);

        $request->setPagination(
            (new RequestBodyPaginationAdminAuth())
                ->setLimit(count($userIds))
                ->setType(PaginationTypeEnumAdminAuth::CURSOR)
        );
        $response = $this->usersApi->searchUsers($request);

        return collect($response->getData())->keyBy('id');
    }

    private function loadManagers(array $managerIds): Collection
    {
        if (!$this->withManager || !$managerIds) {
            return collect();
        }

        return $this->loadUsers($managerIds);
    }
}
