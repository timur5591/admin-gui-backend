<?php

namespace App\Http\ApiV1\Modules\Units\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\AdminAuthClient\Dto\RightsAccess;
use Illuminate\Http\Request;

/**
 * @mixin RightsAccess
 */
class RightsAccessResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
        ];
    }
}
