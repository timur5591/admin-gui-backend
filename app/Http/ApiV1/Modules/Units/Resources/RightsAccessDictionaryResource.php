<?php

namespace App\Http\ApiV1\Modules\Units\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\AdminAuthClient\Dto\RightsAccessDictionary;
use Illuminate\Http\Request;

/**
 * @mixin RightsAccessDictionary
 */
class RightsAccessDictionaryResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'section' => $this->resource->getSection(),
            'items' => RightsAccessResource::collection($this->resource->getItems()),
        ];
    }
}
