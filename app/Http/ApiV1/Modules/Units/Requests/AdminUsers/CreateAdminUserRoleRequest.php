<?php

namespace App\Http\ApiV1\Modules\Units\Requests\AdminUsers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Validation\Rule;

class CreateAdminUserRoleRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'title' => ['required', 'string'],
            'active' => ['required', 'boolean'],
            'rights_access' => ['present', 'array'],
            'rights_access.*' => ['integer', Rule::in(RightsAccessEnum::getAllowableEnumValues())],
        ];
    }
}
