<?php

namespace App\Http\ApiV1\Modules\Units\Requests\AdminUsers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Validation\Rule;

class PatchAdminUserRoleRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'title' => ['string'],
            'active' => ['boolean'],
            'rights_access' => ['present', 'array'],
            'rights_access.*' => ['integer', Rule::in(RightsAccessEnum::getAllowableEnumValues())],
        ];
    }
}
