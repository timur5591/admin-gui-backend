<?php

namespace App\Http\ApiV1\Modules\Units\Requests\Stores;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchStoreWorkingRequest extends BaseFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'store_id' => ['sometimes' ,'required', 'integer'],
            'active' => ['nullable', 'bool'],
            'day' => ['sometimes', 'required', 'int'],
            'working_start_time' => ['nullable', 'string'],
            'working_end_time' => ['nullable', 'string'],
        ];
    }
}
