<?php

namespace App\Http\ApiV1\Modules\Units\Requests\Stores;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateOrReplaceStoreWorkingRequest extends BaseFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'store_id' => ['required', 'integer'],
            'active' => ['nullable', 'bool'],
            'day' => ['required', 'int'],
            'working_start_time' => ['nullable', 'string'],
            'working_end_time' => ['nullable', 'string'],
      ];
    }
}
