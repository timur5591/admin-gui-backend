<?php

namespace App\Http\ApiV1\Modules\Communication\Controllers;

use App\Domain\Communication\Actions\CreateBroadcastAction;
use App\Http\ApiV1\Modules\Communication\Requests\CreateBroadcastRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class BroadcastsController
{
    public function create(CreateBroadcastRequest $request, CreateBroadcastAction $action)
    {
        $action->execute($request->validated());

        return new EmptyResource();
    }
}
