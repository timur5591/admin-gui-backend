<?php

namespace App\Http\ApiV1\Modules\Communication\Controllers;

use App\Domain\Communication\Actions\DeleteFilesAction;
use App\Domain\Communication\Actions\UploadFileAction;
use App\Http\ApiV1\Modules\Communication\Requests\DeleteFileRequest;
use App\Http\ApiV1\Modules\Communication\Requests\UploadFileRequest;
use App\Http\ApiV1\Modules\Communication\Resources\FilesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class FilesController
{
    public function upload(UploadFileRequest $request, UploadFileAction $action)
    {
        $file = $request->file('file');

        return response()->json([
            'data' => new FilesResource($action->execute($file, $file->getClientOriginalName())),
        ]);
    }

    public function delete(DeleteFileRequest $request, DeleteFilesAction $action)
    {
        $fileIds = $request->get('files');
        $action->execute($fileIds);

        return new EmptyResource();
    }
}
