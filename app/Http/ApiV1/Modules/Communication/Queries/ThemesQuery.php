<?php

namespace App\Http\ApiV1\Modules\Communication\Queries;

use Ensi\CommunicationManagerClient\Api\ThemesApi;
use Ensi\CommunicationManagerClient\ApiException;
use Ensi\CommunicationManagerClient\Dto\SearchThemesRequest;
use Ensi\CommunicationManagerClient\Dto\SearchThemesResponse;
use Illuminate\Http\Request;

class ThemesQuery extends CommunicationQuery
{
    public function __construct(Request $request, private ThemesApi $api)
    {
        parent::__construct($request, SearchThemesRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchThemesResponse
    {
        return $this->api->searchThemes($request);
    }
}
