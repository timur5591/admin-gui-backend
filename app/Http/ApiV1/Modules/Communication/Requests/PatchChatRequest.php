<?php

namespace App\Http\ApiV1\Modules\Communication\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchChatRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'theme' => ['string'],
            'type_id' => ['integer'],
        ];
    }
}
