<?php

namespace App\Http\ApiV1\Modules\Communication\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\CommunicationManagerClient\Dto\UserTypeEnum;
use Illuminate\Validation\Rule;

class CreateChatRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'theme' => ['required', 'string'],
            'type_id' => ['required', 'integer'],
            'user_type' => ['required', Rule::in(UserTypeEnum::getAllowableEnumValues())],
            'user_id' => ['required', 'integer'],
        ];
    }
}
