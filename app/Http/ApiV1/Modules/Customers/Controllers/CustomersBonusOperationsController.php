<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\BonusOperations\CreateBonusOperationsAction;
use App\Domain\Customers\Actions\BonusOperations\DeleteBonusOperationsAction;
use App\Domain\Customers\Actions\BonusOperations\ReplaceBonusOperationsAction;
use App\Http\ApiV1\Modules\Customers\Queries\CustomersBonusOperationsQuery;
use App\Http\ApiV1\Modules\Customers\Requests\BonusOperations\CreateBonusOperationsRequest;
use App\Http\ApiV1\Modules\Customers\Requests\BonusOperations\ReplaceBonusOperationsRequest;
use App\Http\ApiV1\Modules\Customers\Resources\BonusOperationResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CustomersBonusOperationsController
{
    public function search(CustomersBonusOperationsQuery $query): AnonymousResourceCollection
    {
        return BonusOperationResource::collectPage($query->get());
    }

    public function create(
        CreateBonusOperationsRequest $request,
        CreateBonusOperationsAction  $action
    ): BonusOperationResource {
        return new BonusOperationResource($action->execute($request->validated()));
    }

    public function get(int $bonusOperationId, CustomersBonusOperationsQuery $query): BonusOperationResource
    {
        return new BonusOperationResource($query->searchById($bonusOperationId));
    }

    public function patch(
        int                           $bonusOperationId,
        ReplaceBonusOperationsRequest $request,
        ReplaceBonusOperationsAction  $action
    ): BonusOperationResource {
        return new BonusOperationResource($action->execute($bonusOperationId, $request->validated()));
    }

    public function delete(int $bonusOperationId, DeleteBonusOperationsAction $action): EmptyResource
    {
        $action->execute($bonusOperationId);

        return new EmptyResource();
    }
}
