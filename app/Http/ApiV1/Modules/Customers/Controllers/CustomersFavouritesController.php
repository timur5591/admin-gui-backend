<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\Favourites\ClearCustomerFavouritesAction;
use App\Domain\Customers\Actions\Favourites\CreateCustomerFavouritesAction;
use App\Domain\Customers\Actions\Favourites\DeleteCustomerFavouritesAction;
use App\Http\ApiV1\Modules\Customers\Queries\CustomersFavouritesQuery;
use App\Http\ApiV1\Modules\Customers\Requests\Favourites\ClearCustomerFavouritesRequest;
use App\Http\ApiV1\Modules\Customers\Requests\Favourites\CreateOrDeleteCustomerFavouritesRequest;
use App\Http\ApiV1\Modules\Customers\Resources\CustomersFavouritesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CustomersFavouritesController
{
    public function search(CustomersFavouritesQuery $query): AnonymousResourceCollection
    {
        return CustomersFavouritesResource::collectPage($query->get());
    }

    public function create(
        CreateOrDeleteCustomerFavouritesRequest $request,
        CreateCustomerFavouritesAction          $action
    ): CustomersFavouritesResource {
        return new CustomersFavouritesResource($action->execute($request->validated()));
    }

    public function delete(
        CreateOrDeleteCustomerFavouritesRequest $request,
        DeleteCustomerFavouritesAction          $action
    ): EmptyResource {
        $action->execute($request->validated());

        return new EmptyResource();
    }

    public function clear(ClearCustomerFavouritesRequest $request, ClearCustomerFavouritesAction $action): EmptyResource
    {
        $action->execute($request->validated());

        return new EmptyResource();
    }
}
