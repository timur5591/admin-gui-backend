<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\Addresses\CreateCustomerAddressAction;
use App\Domain\Customers\Actions\Addresses\DeleteCustomerAddressAction;
use App\Domain\Customers\Actions\Addresses\ReplaceCustomerAddressAction;
use App\Domain\Customers\Actions\Addresses\SetDefaultCustomerAddressAction;
use App\Http\ApiV1\Modules\Customers\Queries\CustomersAddressQuery;
use App\Http\ApiV1\Modules\Customers\Requests\Addresses\CreateOrReplaceCustomerAddressRequest;
use App\Http\ApiV1\Modules\Customers\Resources\CustomersAddressesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class CustomersAddressController
 * @package App\Http\ApiV1\Modules\Customers\Controllers
 */
class CustomersAddressController
{
    /**
     * @param CustomersAddressQuery $query
     * @return AnonymousResourceCollection
     */
    public function search(CustomersAddressQuery $query): AnonymousResourceCollection
    {
        return CustomersAddressesResource::collectPage($query->get());
    }

    /**
     * @param CreateOrReplaceCustomerAddressRequest $request
     * @param CreateCustomerAddressAction $action
     * @return CustomersAddressesResource
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function create(
        CreateOrReplaceCustomerAddressRequest $request,
        CreateCustomerAddressAction           $action
    ): CustomersAddressesResource {
        return new CustomersAddressesResource($action->execute($request->validated()));
    }

    /**
     * @param int $addressId
     * @param CustomersAddressQuery $query
     * @return CustomersAddressesResource
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function get(int $addressId, CustomersAddressQuery $query): CustomersAddressesResource
    {
        return new CustomersAddressesResource($query->searchById($addressId));
    }

    /**
     * @param int $addressId
     * @param CreateOrReplaceCustomerAddressRequest $request
     * @param ReplaceCustomerAddressAction $action
     * @return CustomersAddressesResource
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function replace(
        int                                   $addressId,
        CreateOrReplaceCustomerAddressRequest $request,
        ReplaceCustomerAddressAction          $action
    ): CustomersAddressesResource {
        return new CustomersAddressesResource($action->execute($addressId, $request->validated()));
    }

    /**
     * @param int $addressId
     * @param DeleteCustomerAddressAction $action
     * @return EmptyResource
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function delete(int $addressId, DeleteCustomerAddressAction $action): EmptyResource
    {
        $action->execute($addressId);

        return new EmptyResource();
    }

    /**
     * @param int $addressId
     * @param SetDefaultCustomerAddressAction $action
     * @return EmptyResource
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function setDefault(int $addressId, SetDefaultCustomerAddressAction $action): EmptyResource
    {
        $action->execute($addressId);

        return new EmptyResource();
    }
}
