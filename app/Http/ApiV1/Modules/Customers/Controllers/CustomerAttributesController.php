<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\Attributes\CreateCustomerAttributeAction;
use App\Domain\Customers\Actions\Attributes\DeleteCustomerAttributeAction;
use App\Domain\Customers\Actions\Attributes\ReplaceCustomerAttributeAction;
use App\Http\ApiV1\Modules\Customers\Queries\CustomersAttributeQuery;
use App\Http\ApiV1\Modules\Customers\Requests\Attributes\CreateOrReplaceCustomerAttributeRequest;
use App\Http\ApiV1\Modules\Customers\Resources\CustomersAttributesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CustomerAttributesController
{
    /**
     * @param CustomersAttributeQuery $query
     * @return AnonymousResourceCollection
     */
    public function search(CustomersAttributeQuery $query): AnonymousResourceCollection
    {
        return CustomersAttributesResource::collectPage($query->get());
    }

    /**
     * @param CreateOrReplaceCustomerAttributeRequest $request
     * @param CreateCustomerAttributeAction $action
     * @return CustomersAttributesResource
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function create(
        CreateOrReplaceCustomerAttributeRequest $request,
        CreateCustomerAttributeAction           $action
    ): CustomersAttributesResource {
        return new CustomersAttributesResource($action->execute($request->validated()));
    }

    /**
     * @param int $statusId
     * @param CustomersAttributeQuery $query
     * @return CustomersAttributesResource
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function get(int $statusId, CustomersAttributeQuery $query): CustomersAttributesResource
    {
        return new CustomersAttributesResource($query->searchById($statusId));
    }

    /**
     * @param int $statusId
     * @param CreateOrReplaceCustomerAttributeRequest $request
     * @param ReplaceCustomerAttributeAction $action
     * @return CustomersAttributesResource
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function patch(
        int                                  $statusId,
        CreateOrReplaceCustomerAttributeRequest $request,
        ReplaceCustomerAttributeAction          $action
    ): CustomersAttributesResource {
        return new CustomersAttributesResource($action->execute($statusId, $request->validated()));
    }

    /**
     * @param int $statusId
     * @param DeleteCustomerAttributeAction $action
     * @return EmptyResource
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function delete(int $statusId, DeleteCustomerAttributeAction $action): EmptyResource
    {
        $action->execute($statusId);

        return new EmptyResource();
    }
}
