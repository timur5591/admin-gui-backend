<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\ProductSubscribes\ClearCustomerProductSubscribesAction;
use App\Domain\Customers\Actions\ProductSubscribes\CreateCustomerProductSubscribesAction;
use App\Domain\Customers\Actions\ProductSubscribes\DeleteCustomerProductSubscribesAction;
use App\Http\ApiV1\Modules\Customers\Queries\CustomersProductSubscribesQuery;
use App\Http\ApiV1\Modules\Customers\Requests\ProductSubcribes\ClearCustomerProductSubscribesRequest;
use App\Http\ApiV1\Modules\Customers\Requests\ProductSubcribes\CreateOrDeleteCustomerProductSubscribesRequest;
use App\Http\ApiV1\Modules\Customers\Resources\CustomersProductSubscribesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CustomersProductSubscribesController
{
    public function search(CustomersProductSubscribesQuery $query): AnonymousResourceCollection
    {
        return CustomersProductSubscribesResource::collectPage($query->get());
    }

    public function create(
        CreateOrDeleteCustomerProductSubscribesRequest $request,
        CreateCustomerProductSubscribesAction          $action
    ): CustomersProductSubscribesResource {
        return new CustomersProductSubscribesResource($action->execute($request->validated()));
    }

    public function delete(
        CreateOrDeleteCustomerProductSubscribesRequest $request,
        DeleteCustomerProductSubscribesAction          $action
    ): EmptyResource {
        $action->execute($request->validated());

        return new EmptyResource();
    }

    public function clear(
        ClearCustomerProductSubscribesRequest $request,
        ClearCustomerProductSubscribesAction  $action
    ): EmptyResource {
        $action->execute($request->validated());

        return new EmptyResource();
    }
}
