<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\Customers\CreateCustomerAction;
use App\Domain\Customers\Actions\Customers\DeleteAvatarAction;
use App\Domain\Customers\Actions\Customers\DeleteCustomerAction;
use App\Domain\Customers\Actions\Customers\DeleteCustomerPersonalDataAction;
use App\Domain\Customers\Actions\Customers\ReplaceCustomerAction;
use App\Domain\Customers\Actions\Customers\UploadAvatarAction;
use App\Http\ApiV1\Modules\Customers\Queries\CustomersQuery;
use App\Http\ApiV1\Modules\Customers\Requests\Customers\CreateOrReplaceCustomerRequest;
use App\Http\ApiV1\Modules\Customers\Requests\Customers\UploadAvatarRequest;
use App\Http\ApiV1\Modules\Customers\Resources\CustomerEnumValueResource;
use App\Http\ApiV1\Modules\Customers\Resources\CustomersResource;
use App\Http\ApiV1\Support\Requests\CommonFilterEnumValuesRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Ensi\CustomerAuthClient\ApiException as ApiExceptionCustomerAuth;
use Ensi\CustomersClient\ApiException as ApiExceptionCustomers;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CustomersController
{
    public function search(CustomersQuery $query): AnonymousResourceCollection
    {
        return CustomersResource::collectPage($query->get());
    }

    public function searchEnumValues(CommonFilterEnumValuesRequest $request, CustomersQuery $query): AnonymousResourceCollection
    {
        return CustomerEnumValueResource::collection($query->searchEnums());
    }

    /**
     * @throws ApiExceptionCustomerAuth
     * @throws ApiExceptionCustomers
     */
    public function create(CreateOrReplaceCustomerRequest $request, CreateCustomerAction $action): CustomersResource
    {
        return new CustomersResource($action->execute($request->validated()));
    }

    /**
     * @throws ApiExceptionCustomerAuth
     * @throws ApiExceptionCustomers
     */
    public function get(int $customerId, CustomersQuery $query): CustomersResource
    {
        return new CustomersResource($query->searchById($customerId));
    }

    /**
     * @throws ApiExceptionCustomerAuth
     * @throws ApiExceptionCustomers
     */
    public function replace(int $customerId, CreateOrReplaceCustomerRequest $request, ReplaceCustomerAction $action): CustomersResource
    {
        return new CustomersResource($action->execute($customerId, $request->validated()));
    }

    /**
     * @throws ApiExceptionCustomers
     */
    public function delete(int $customerId, DeleteCustomerAction $action): EmptyResource
    {
        $action->execute($customerId);

        return new EmptyResource();
    }

    /**
     * @throws ApiExceptionCustomers
     */
    public function deletePersonalData(int $customerId, DeleteCustomerPersonalDataAction $action): EmptyResource
    {
        $action->execute($customerId);

        return new EmptyResource();
    }

    /**
     * @throws ApiExceptionCustomerAuth
     * @throws ApiExceptionCustomers
     */
    public function uploadAvatar(int $customerId, UploadAvatarRequest $request, UploadAvatarAction $action): CustomersResource
    {
        return new CustomersResource($action->execute($customerId, $request->file('file')));
    }

    /**
     * @throws ApiExceptionCustomerAuth
     * @throws ApiExceptionCustomers
     */
    public function deleteAvatar(int $customerId, DeleteAvatarAction $action): CustomersResource
    {
        return new CustomersResource($action->execute($customerId));
    }
}
