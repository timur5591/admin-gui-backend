<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use Ensi\CrmClient\Api\BonusOperationsApi;
use Ensi\CrmClient\Dto\BonusOperation;
use Ensi\CrmClient\Dto\SearchBonusOperationsRequest;
use Ensi\CrmClient\Dto\SearchBonusOperationsResponse;
use Illuminate\Http\Request;

class CustomersBonusOperationsQuery extends CrmQuery
{
    use QueryBuilderFindTrait;

    /**
     * CustomersBonusOperationsQuery constructor.
     * @param Request $httpRequest
     * @param BonusOperationsApi $bonusOperationsApi
     */
    public function __construct(
        protected Request          $httpRequest,
        protected BonusOperationsApi $bonusOperationsApi
    ) {
        parent::__construct($httpRequest, SearchBonusOperationsRequest::class);
    }

    /**
     * @param $request
     * @return SearchBonusOperationsResponse
     * @throws \Ensi\CrmClient\ApiException
     */
    protected function search($request): SearchBonusOperationsResponse
    {
        return $this->bonusOperationsApi->searchBonusOperation($request);
    }

    /**
     * @param $id
     * @return BonusOperation
     * @throws \Ensi\CrmClient\ApiException
     */
    public function searchById($id): BonusOperation
    {
        return $this->bonusOperationsApi->getBonusOperation($id)->getData();
    }
}
