<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use Ensi\CrmClient\Api\PreferencesApi;
use Ensi\CrmClient\Dto\SearchPreferencesRequest;
use Ensi\CrmClient\Dto\SearchPreferencesResponse;
use Illuminate\Http\Request;

class CustomersPreferencesQuery extends CrmQuery
{
    /**
     * CustomersPreferencesQuery constructor.
     * @param Request $httpRequest
     * @param PreferencesApi $preferencesApi
     */
    public function __construct(
        protected Request        $httpRequest,
        protected PreferencesApi $preferencesApi
    ) {
        parent::__construct($httpRequest, SearchPreferencesRequest::class);
    }

    /**
     * @param $request
     * @return SearchPreferencesResponse
     * @throws \Ensi\CrmClient\ApiException
     */
    protected function search($request): SearchPreferencesResponse
    {
        return $this->preferencesApi->searchPreferences($request);
    }
}
