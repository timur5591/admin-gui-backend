<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use Ensi\CrmClient\Api\ProductSubscribesApi;
use Ensi\CrmClient\Dto\SearchProductSubscribesRequest;
use Ensi\CrmClient\Dto\SearchProductSubscribesResponse;
use Illuminate\Http\Request;

class CustomersProductSubscribesQuery extends CrmQuery
{
    public function __construct(
        protected Request              $httpRequest,
        protected ProductSubscribesApi $productSubscribesApi
    ) {
        parent::__construct($httpRequest, SearchProductSubscribesRequest::class);
    }

    protected function search($request): SearchProductSubscribesResponse
    {
        return $this->productSubscribesApi->searchProductSubscribe($request);
    }
}
