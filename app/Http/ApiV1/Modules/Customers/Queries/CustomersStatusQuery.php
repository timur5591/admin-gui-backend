<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use Ensi\CustomersClient\Api\StatusesApi;
use Ensi\CustomersClient\Dto\CustomerStatuses;
use Ensi\CustomersClient\Dto\SearchCustomerStatusesRequest;
use Ensi\CustomersClient\Dto\SearchCustomerStatusesResponse;
use Illuminate\Http\Request;

class CustomersStatusQuery extends CrmQuery
{
    use QueryBuilderFindTrait;

    /**
     * CustomerStatusQuery constructor.
     * @param Request $httpRequest
     * @param StatusesApi $statusesApi
     */
    public function __construct(
        protected Request     $httpRequest,
        protected StatusesApi $statusesApi
    ) {
        parent::__construct($httpRequest, SearchCustomerStatusesRequest::class);
    }

    /**
     * @param $request
     * @return SearchCustomerStatusesResponse
     * @throws \Ensi\CustomersClient\ApiException
     */
    protected function search($request): SearchCustomerStatusesResponse
    {
        return $this->statusesApi->searchCustomerStatuses($request);
    }

    /**
     * @param int $id
     * @return CustomerStatuses
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function searchById($id): CustomerStatuses
    {
        return $this->statusesApi->getCustomerStatus($id)->getData();
    }
}
