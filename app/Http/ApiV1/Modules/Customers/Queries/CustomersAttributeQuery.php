<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use Ensi\CustomersClient\Api\AttributesApi;
use Ensi\CustomersClient\Dto\CustomerAttributes;
use Ensi\CustomersClient\Dto\SearchCustomerAttributesRequest;
use Ensi\CustomersClient\Dto\SearchCustomerAttributesResponse;
use Illuminate\Http\Request;

class CustomersAttributeQuery extends CrmQuery
{
    use QueryBuilderFindTrait;

    /**
     * CustomersAttributeQuery constructor.
     * @param Request $httpRequest
     * @param AttributesApi $attributesApi
     */
    public function __construct(
        protected Request       $httpRequest,
        protected AttributesApi $attributesApi
    ) {
        parent::__construct($httpRequest, SearchCustomerAttributesRequest::class);
    }

    /**
     * @param $id
     * @return CustomerAttributes
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function searchById($id): CustomerAttributes
    {
        return $this->attributesApi->getCustomerAttribute($id)->getData();
    }

    /**
     * @param $request
     * @return SearchCustomerAttributesResponse
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function search($request): SearchCustomerAttributesResponse
    {
        return $this->attributesApi->searchCustomerAttributes($request);
    }
}
