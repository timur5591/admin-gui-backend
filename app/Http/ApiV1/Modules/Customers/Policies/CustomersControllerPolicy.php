<?php

namespace App\Http\ApiV1\Modules\Customers\Policies;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class CustomersControllerPolicy
{
    use HandlesAuthorization;

    public function deletePersonalData(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::CUSTOMER_DELETE_PERSONAL_DATA,
        ]);
    }
}
