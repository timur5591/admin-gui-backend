<?php

namespace App\Http\ApiV1\Modules\Customers\Requests\Users;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

/**
 * Class CreateCustomerUserRequest
 * @package App\Http\ApiV1\Modules\Customers\Requests\Users
 */
class CreateCustomerUserRequest extends BaseFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'login' => ['required'],
            'active' => ['boolean'],
            'password' => ['required'],
        ];
    }
}
