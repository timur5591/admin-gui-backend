<?php

namespace App\Http\ApiV1\Support\Resources;

use Ensi\LogisticClient\Dto\Address;
use Illuminate\Http\Request;

/**
 * Class AddressResource
 * @package App\Http\ApiV1\Support\Resources
 *
 * @mixin Address
 */
class AddressResource extends BaseJsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'address_string' => $this->getAddressString(),
            'post_index' => $this->getPostIndex(),
            'country_code' => $this->getCountryCode(),
            'region' => $this->getRegion(),
            'region_guid' => $this->getRegionGuid(),
            'area' => $this->getArea(),
            'area_guid' => $this->getAreaGuid(),
            'city' => $this->getCity(),
            'city_guid' => $this->getCityGuid(),
            'street' => $this->getStreet(),
            'house' => $this->getHouse(),
            'block' => $this->getBlock(),
            'flat' => $this->getFlat(),
            'floor' => $this->getFloor(),
            'porch' => $this->getPorch(),
            'intercom' => $this->getIntercom(),
            'geo_lat' => $this->getGeoLat(),
            'geo_lon' => $this->getGeoLon(),
        ];
    }
}
