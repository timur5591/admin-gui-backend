<?php

namespace App\Http\ApiV1\Support\Tests;

use App\Domain\Auth\Models\Tests\Factories\UserFactory;
use App\Http\Middleware\UserHasRightsAccess;

class ApiV1ComponentWithRightsTestCase extends ApiV1ComponentTestCase
{
    public function setRights(array $rolesAccess)
    {
        $user = UserFactory::new()->make([
            'rights_access' => $rolesAccess,
        ]);

        auth()->setUser($user);
    }

    public function disableMiddlewareForAllTests()
    {
        $this->withMiddleware(UserHasRightsAccess::class);
    }
}
