<?php

namespace App\Http\ApiV1\Support\Tests;

use Ensi\LaravelOpenApiTesting\ValidatesAgainstOpenApiSpec;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\ComponentTestCase;

abstract class ApiV1ComponentTestCase extends ComponentTestCase
{
    use ValidatesAgainstOpenApiSpec;
    use WithoutMiddleware;

    protected function getOpenApiDocumentPath(): string
    {
        return public_path('api-docs/v1/index.yaml');
    }
}
