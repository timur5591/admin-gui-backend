<?php

namespace App\Domain\Auth\Models\Tests\Factories;

use Ensi\AdminAuthClient\Dto\CreateTokenResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Lcobucci\JWT\Configuration;

class TokenFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $configuration = Configuration::forUnsecuredSigner();

        return [
            'access_token' => $configuration->builder()
                ->identifiedBy($this->faker->md5())
                ->getToken($configuration->signer(), $configuration->signingKey())
                ->toString(),
            'refresh_token' => $configuration->builder()
                ->identifiedBy($this->faker->md5())
                ->getToken($configuration->signer(), $configuration->signingKey())
                ->toString(),
            'expires_in' => $this->faker->randomNumber(),
        ];
    }

    public function make(array $extra = []): CreateTokenResponse
    {
        return new CreateTokenResponse($this->makeArray($extra));
    }
}
