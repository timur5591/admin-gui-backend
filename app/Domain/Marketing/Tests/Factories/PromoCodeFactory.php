<?php

namespace App\Domain\Marketing\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\MarketingClient\Dto\PromoCode;
use Ensi\MarketingClient\Dto\PromoCodeResponse;
use Ensi\MarketingClient\Dto\PromoCodeStatusEnum;
use Ensi\MarketingClient\Dto\SearchPromoCodesResponse;

class PromoCodeFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $counter = $this->faker->nullable()->numberBetween(1);
        $startData = $this->faker->nullable()->dateTime();

        return [
            'id' => $this->faker->modelId(),
            'name' => $this->faker->text(50),
            'code' => $this->faker->text(50),
            'counter' => $counter,
            'current_counter' => $counter ? $this->faker->nullable()->numberBetween(0, $counter) : null,
            'start_date' => $startData,
            'end_date' => $startData ?
                $this->faker->nullable()->dateTimeBetween($startData, '+5 days') :
                $this->faker->nullable()->dateTime(),
            'status' => $this->faker->randomElement(PromoCodeStatusEnum::getAllowableEnumValues()),
            'discount_id' => $this->faker->modelId(),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): PromoCode
    {
        return new PromoCode($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): PromoCodeResponse
    {
        return new PromoCodeResponse(['data' => $this->make($extra),]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchPromoCodesResponse
    {
        return $this->generateResponseSearch(SearchPromoCodesResponse::class, $extras, $count, $pagination);
    }
}
