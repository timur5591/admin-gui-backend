<?php

namespace App\Domain\Marketing\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\MarketingClient\Dto\DiscountProduct;
use Ensi\MarketingClient\Dto\DiscountProductResponse;
use Ensi\MarketingClient\Dto\SearchDiscountProductsResponse;

class DiscountProductFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'discount_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): DiscountProduct
    {
        return new DiscountProduct($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): DiscountProductResponse
    {
        return new DiscountProductResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchDiscountProductsResponse
    {
        return $this->generateResponseSearch(SearchDiscountProductsResponse::class, $extras, $count, $pagination);
    }
}
