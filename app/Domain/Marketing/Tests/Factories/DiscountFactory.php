<?php

namespace App\Domain\Marketing\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\MarketingClient\Dto\Discount;
use Ensi\MarketingClient\Dto\DiscountProduct;
use Ensi\MarketingClient\Dto\DiscountResponse;
use Ensi\MarketingClient\Dto\DiscountStatusEnum;
use Ensi\MarketingClient\Dto\DiscountTypeEnum;
use Ensi\MarketingClient\Dto\DiscountValueTypeEnum;
use Ensi\MarketingClient\Dto\SearchDiscountsResponse;

class DiscountFactory extends BaseApiFactory
{
    protected array $products = [];

    protected function definition(): array
    {
        $startData = $this->faker->dateTime();

        $definition = [
            'id' => $this->faker->modelId(),
            'type' => $this->faker->randomElement(DiscountTypeEnum::getAllowableEnumValues()),
            'name' => $this->faker->text(50),
            'value_type' => $this->faker->randomElement(DiscountValueTypeEnum::getAllowableEnumValues()),
            'value' => $this->faker->numberBetween(1),
            'status' => $this->faker->randomElement(DiscountStatusEnum::getAllowableEnumValues()),
            'start_date' => $startData,
            'end_date' => $startData ?
                $this->faker->nullable()->dateTimeBetween($startData, '+5 days') :
                $this->faker->nullable()->dateTime(),
            'promo_code_only' => $this->faker->boolean(),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if ($this->products) {
            $definition['products'] = $this->products;
        }

        return $definition;
    }

    public function withProducts(?DiscountProduct $product = null): self
    {
        $this->products[] = $product ?: DiscountProductFactory::new()->make();

        return $this;
    }

    public function make(array $extra = []): Discount
    {
        return new Discount($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): DiscountResponse
    {
        return new DiscountResponse([
            'data' => $this->make($extra),
        ]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchDiscountsResponse
    {
        return $this->generateResponseSearch(SearchDiscountsResponse::class, $extras, $count, $pagination);
    }
}
