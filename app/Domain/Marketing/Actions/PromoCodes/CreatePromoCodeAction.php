<?php

namespace App\Domain\Marketing\Actions\PromoCodes;

use App\Domain\Marketing\Data\Discounts\PromoCodeData;
use Ensi\MarketingClient\Api\PromoCodesApi;
use Ensi\MarketingClient\Dto\PromoCodeForCreate;

class CreatePromoCodeAction
{
    public function __construct(protected PromoCodesApi $api)
    {
    }

    public function execute(array $data): PromoCodeData
    {
        $request = new PromoCodeForCreate($data);

        return new PromoCodeData($this->api->createPromoCode($request)->getData());
    }
}
