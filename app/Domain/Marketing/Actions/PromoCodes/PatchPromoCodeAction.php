<?php

namespace App\Domain\Marketing\Actions\PromoCodes;

use App\Domain\Marketing\Data\Discounts\PromoCodeData;
use Ensi\MarketingClient\Api\PromoCodesApi;
use Ensi\MarketingClient\Dto\PromoCodeForPatch;

class PatchPromoCodeAction
{
    public function __construct(protected PromoCodesApi $api)
    {
    }

    public function execute(int $id, array $data): PromoCodeData
    {
        $request = new PromoCodeForPatch($data);

        return new PromoCodeData($this->api->patchPromoCode($id, $request)->getData());
    }
}
