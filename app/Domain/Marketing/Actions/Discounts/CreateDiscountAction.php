<?php

namespace App\Domain\Marketing\Actions\Discounts;

use App\Domain\Marketing\Data\Discounts\DiscountData;
use Ensi\MarketingClient\Api\DiscountsApi;
use Ensi\MarketingClient\Dto\DiscountForCreate;

class CreateDiscountAction
{
    public function __construct(protected DiscountsApi $api)
    {
    }

    public function execute(array $data): DiscountData
    {
        $request = new DiscountForCreate($data);

        return new DiscountData($this->api->createDiscount($request)->getData());
    }
}
