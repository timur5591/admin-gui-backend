<?php

namespace App\Domain\Marketing\Actions\Discounts;

use Ensi\MarketingClient\Api\DiscountsApi;

class DeleteDiscountAction
{
    public function __construct(protected DiscountsApi $api)
    {
    }

    public function execute(int $id): void
    {
        $this->api->deleteDiscount($id);
    }
}
