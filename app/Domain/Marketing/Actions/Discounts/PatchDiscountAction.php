<?php

namespace App\Domain\Marketing\Actions\Discounts;

use App\Domain\Marketing\Data\Discounts\DiscountData;
use Ensi\MarketingClient\Api\DiscountsApi;
use Ensi\MarketingClient\Dto\DiscountForPatch;

class PatchDiscountAction
{
    public function __construct(protected DiscountsApi $api)
    {
    }

    public function execute(int $id, array $data): DiscountData
    {
        $request = new DiscountForPatch($data);

        return new DiscountData($this->api->patchDiscount($id, $request)->getData());
    }
}
