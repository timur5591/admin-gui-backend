<?php

namespace App\Domain\Contents\Tests\Pages\Factories;

use Ensi\CmsClient\Dto\Page;
use Ensi\CmsClient\Dto\PageResponse;
use Ensi\CmsClient\Dto\SearchPagesResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class PageFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $dateTime = $this->faker->dateTime();

        return [
            'id' => $this->faker->modelId(),
            'name' => $this->faker->words(3, true),
            'slug' => $this->faker->slug(),
            'content' => $this->faker->randomHtml(),
            'is_active' => $this->faker->boolean(),
            'active_from' => $dateTime,
            'active_to' => $dateTime->add(new \DateInterval('P1D')),
            'updated_at' => $this->faker->dateTime(),
            'created_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): Page
    {
        return new Page($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): PageResponse
    {
        return new PageResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchPagesResponse
    {
        return $this->generateResponseSearch(SearchPagesResponse::class, $extras, $count, $pagination);
    }
}
