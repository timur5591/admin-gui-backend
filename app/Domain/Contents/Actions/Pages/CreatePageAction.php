<?php

namespace App\Domain\Contents\Actions\Pages;

use Ensi\CmsClient\Api\PagesApi;
use Ensi\CmsClient\ApiException;
use Ensi\CmsClient\Dto\Page;
use Ensi\CmsClient\Dto\PageForCreate;

class CreatePageAction
{
    public function __construct(protected PagesApi $pagesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): Page
    {
        $request = new PageForCreate($fields);
        $responsePagesApi = $this->pagesApi->createPage($request);

        return $responsePagesApi->getData();
    }
}
