<?php

namespace App\Domain\Contents\Actions\Pages;

use Ensi\CmsClient\Api\PagesApi;
use Ensi\CmsClient\ApiException;
use Ensi\CmsClient\Dto\Page;
use Ensi\CmsClient\Dto\PageForPatch;

class PatchPageAction
{
    public function __construct(protected PagesApi $pagesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): Page
    {
        $request = new PageForPatch($fields);
        $responsePagesApi = $this->pagesApi->patchPage($id, $request);

        return $responsePagesApi->getData();
    }
}
