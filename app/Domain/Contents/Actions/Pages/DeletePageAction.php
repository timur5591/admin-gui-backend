<?php

namespace App\Domain\Contents\Actions\Pages;

use Ensi\CmsClient\Api\PagesApi;
use Ensi\CmsClient\ApiException;

class DeletePageAction
{
    public function __construct(protected PagesApi $pagesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id): void
    {
        $this->pagesApi->deletePage($id);
    }
}
