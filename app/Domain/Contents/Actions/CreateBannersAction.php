<?php

namespace App\Domain\Contents\Actions;

use Ensi\CmsClient\Api\BannersApi;
use Ensi\CmsClient\Dto\Banner;
use Ensi\CmsClient\Dto\BannerForCreate;

class CreateBannersAction
{
    public function __construct(
        private BannersApi $bannersApi
    ) {
    }

    public function execute(array $data): Banner
    {
        $requestBannersApi = new BannerForCreate($data);
        $responseBannersApi = $this->bannersApi->createBanners($requestBannersApi);

        return $responseBannersApi->getData();
    }
}
