<?php

namespace App\Domain\Contents\Actions;

use Ensi\CmsClient\Api\ProductCategoriesApi;
use Ensi\CmsClient\Dto\MassDeleteProductCategoriesRequest;

class MassDeleteProductCategoriesAction
{
    public function __construct(
        private ProductCategoriesApi $productCategoriesApi
    ) {
    }

    public function execute(array $data): void
    {
        $massDeleteProductCategoriesRequest = new MassDeleteProductCategoriesRequest($data);
        $this->productCategoriesApi->massDeleteProductCategory($massDeleteProductCategoriesRequest);
    }
}
