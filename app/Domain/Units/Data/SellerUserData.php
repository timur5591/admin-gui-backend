<?php

namespace App\Domain\Units\Data;

use Ensi\AdminAuthClient\Dto\User;
use Ensi\BuClient\Dto\Operator;

/**
 * Class SellerUserData
 * @package App\Domain\Units\Data
 */
class SellerUserData
{
    public ?User $user = null;

    /**
     * SellerUserData constructor.
     * @param  Operator  $operator
     */
    public function __construct(public Operator $operator)
    {
    }

    /**
     * @param  User|null  $user
     * @return $this
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
