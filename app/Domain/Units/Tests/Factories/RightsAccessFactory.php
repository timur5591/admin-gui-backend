<?php

namespace App\Domain\Units\Tests\Factories;

use Ensi\AdminAuthClient\Dto\RightsAccess;
use Ensi\AdminAuthClient\Dto\RightsAccessDictionary;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\AdminAuthClient\Dto\RightsAccessResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class RightsAccessFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomElement(RightsAccessEnum::getAllowableEnumValues()),
            'title' => $this->faker->title(),
        ];
    }

    public function make(array $extra = [])
    {
        return new RightsAccess($this->makeArray($extra));
    }

    public function makeResponseGetRightsAccess(): RightsAccessResponse
    {
        return new RightsAccessResponse([
            'data' => [
                new RightsAccessDictionary([
                    'section' => $this->faker->title(),
                    'items' => [$this->make()],
                ]),
            ],
        ]);
    }
}
