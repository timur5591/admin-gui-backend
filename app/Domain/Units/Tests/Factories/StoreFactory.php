<?php

namespace App\Domain\Units\Tests\Factories;

use Ensi\BuClient\Dto\SearchStoresResponse;
use Ensi\BuClient\Dto\Store;
use Ensi\LaravelTestFactories\BaseApiFactory;

class StoreFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'seller_id' => $this->faker->modelId(),
            'xml_id' => $this->faker->word(),
            'active' => $this->faker->boolean(),
            'name' => $this->faker->name(),
            'address' => StoreAddressFactory::new()->make(),
            'timezone' => $this->faker->timezone(),
            'created_at' => $this->faker->date(), // todo dateTime()
            'updated_at' => $this->faker->date(), // todo dateTime()
        ];
    }

    public function make(array $extra = []): Store
    {
        return new Store($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchStoresResponse
    {
        return $this->generateResponseSearch(SearchStoresResponse::class, $extras, $count, $pagination);
    }
}
