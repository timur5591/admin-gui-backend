<?php

namespace App\Domain\Units\Actions\SellerUsers;

use Ensi\AdminAuthClient\Dto\DeleteRoleFromUserRequest;

class DeleteRoleFromSellerUserAction extends SellerUserAction
{
    public function execute(int $operatorId, array $data): void
    {
        $operator = $this->operatorsApi->getOperator($operatorId)->getData();

        $request = new DeleteRoleFromUserRequest($data);
        $this->usersApi->deleteRoleFromUser($operator->getUserId(), $request);
    }
}
