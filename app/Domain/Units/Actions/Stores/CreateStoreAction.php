<?php

namespace App\Domain\Units\Actions\Stores;

use Ensi\BuClient\Api\StoresApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\Store;
use Ensi\BuClient\Dto\StoreForCreate;

class CreateStoreAction
{
    public function __construct(
        private StoresApi $storesApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): Store
    {
        $store = new StoreForCreate($fields);

        return $this->storesApi->createStore($store)->getData();
    }
}
