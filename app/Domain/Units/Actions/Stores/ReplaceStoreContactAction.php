<?php

namespace App\Domain\Units\Actions\Stores;

use Ensi\BuClient\Api\StoreContactsApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\StoreContact;
use Ensi\BuClient\Dto\StoreContactForReplace;

class ReplaceStoreContactAction
{
    public function __construct(
        private StoreContactsApi $storeContactsApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): StoreContact
    {
        $storeContact = new StoreContactForReplace($fields);

        return $this->storeContactsApi->replaceStoreContact($id, $storeContact)->getData();
    }
}
