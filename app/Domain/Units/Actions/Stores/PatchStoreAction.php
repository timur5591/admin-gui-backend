<?php

namespace App\Domain\Units\Actions\Stores;

use Ensi\BuClient\Api\StoresApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\Store;
use Ensi\BuClient\Dto\StoreForPatch;

class PatchStoreAction
{
    public function __construct(
        private StoresApi $storesApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): Store
    {
        $store = new StoreForPatch($fields);

        return $this->storesApi->patchStore($id, $store)->getData();
    }
}
