<?php

namespace App\Domain\Units\Actions\Stores;

use Ensi\BuClient\Api\StorePickupTimesApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\StorePickupTime;
use Ensi\BuClient\Dto\StorePickupTimeForCreate;

class CreateStorePickupTimeAction
{
    public function __construct(
        private StorePickupTimesApi $storePickupTimesApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): StorePickupTime
    {
        $storePickupTime = new StorePickupTimeForCreate($fields);

        return $this->storePickupTimesApi->createStorePickupTime($storePickupTime)->getData();
    }
}
