<?php

namespace App\Domain\Units\Actions\Stores;

use Ensi\BuClient\Api\StorePickupTimesApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\StorePickupTime;
use Ensi\BuClient\Dto\StorePickupTimeForReplace;

class ReplaceStorePickupTimeAction
{
    public function __construct(
        private StorePickupTimesApi $storePickupTimesApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): StorePickupTime
    {
        $storePickupTime = new StorePickupTimeForReplace($fields);

        return $this->storePickupTimesApi->replaceStorePickupTime($id, $storePickupTime)->getData();
    }
}
