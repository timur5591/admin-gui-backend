<?php

namespace App\Domain\Units\Actions\Stores;

use Ensi\BuClient\Api\StoreWorkingsApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\StoreWorking;
use Ensi\BuClient\Dto\StoreWorkingForPatch;

class PatchStoreWorkingAction
{
    public function __construct(
        private StoreWorkingsApi $storeWorkingsApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): StoreWorking
    {
        $storeWorking = new StoreWorkingForPatch($fields);

        return $this->storeWorkingsApi->patchStoreWorking($id, $storeWorking)->getData();
    }
}
