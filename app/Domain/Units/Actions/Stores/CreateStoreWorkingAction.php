<?php

namespace App\Domain\Units\Actions\Stores;

use Ensi\BuClient\Api\StoreWorkingsApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\StoreWorking;
use Ensi\BuClient\Dto\StoreWorkingForCreate;

class CreateStoreWorkingAction
{
    public function __construct(
        private StoreWorkingsApi $storeWorkingsApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): StoreWorking
    {
        $storeWorking = new StoreWorkingForCreate($fields);

        return $this->storeWorkingsApi->createStoreWorking($storeWorking)->getData();
    }
}
