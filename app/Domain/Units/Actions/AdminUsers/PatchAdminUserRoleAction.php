<?php

namespace App\Domain\Units\Actions\AdminUsers;

use Ensi\AdminAuthClient\Api\RolesApi as AdminUserRolesApi;
use Ensi\AdminAuthClient\Dto\PatchRoleRequest as PatchAdminUserRoleRequest;
use Ensi\AdminAuthClient\Dto\Role as AdminUserRole;

class PatchAdminUserRoleAction
{
    public function __construct(
        protected AdminUserRolesApi $adminUserRolesApi,
    ) {
    }

    public function execute(int $id, array $data): AdminUserRole
    {
        $request = new PatchAdminUserRoleRequest($data);

        return $this->adminUserRolesApi->patchRole($id, $request)->getData();
    }
}
