<?php

namespace App\Domain\Units\Actions\AdminUsers;

use Ensi\AdminAuthClient\Api\UsersApi as AdminUsersApi;
use Ensi\AdminAuthClient\Dto\AddRolesToUserRequest as AddRolesToAdminUserRequest;

class AddRolesToAdminUserAction
{
    public function __construct(
        protected AdminUsersApi $adminUsersApi,
    ) {
    }

    public function execute(int $id, array $data): void
    {
        $request = new AddRolesToAdminUserRequest($data);
        $this->adminUsersApi->addRolesToUser($id, $request);
    }
}
