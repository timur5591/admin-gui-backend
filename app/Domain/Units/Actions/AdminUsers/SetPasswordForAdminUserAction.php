<?php

namespace App\Domain\Units\Actions\AdminUsers;

use App\Http\ApiV1\Modules\Units\Resources\AdminUsersResource;
use Ensi\AdminAuthClient\Api\UsersApi as AdminUsersApi;
use Ensi\AdminAuthClient\Dto\PatchUserRequest;
use Ensi\AdminAuthClient\Dto\SearchUsersRequest;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class SetPasswordForAdminUserAction
{
    public function __construct(
        protected AdminUsersApi $adminUsersApi,
    ) {
    }

    public function execute(array $data): void
    {
        $request = new SearchUsersRequest([
            'filter' => [
                'password_token' => $data['token'],
            ],
        ]);
        $searchResult = $this->adminUsersApi->searchUsers($request)->getData();
        if (empty($searchResult)) {
            throw new BadRequestHttpException('Ссылка установки пароля недействительна. Для генерации новой обратитесь к администратору');
        }
        $user = AdminUsersResource::make($searchResult[0]);
        $request = new PatchUserRequest([
            'password' => $data['password'],
        ]);

        $this->adminUsersApi->patchUser($user->getId(), $request);
    }
}
