<?php

namespace App\Domain\Units\Actions\AdminUsers;

use Ensi\AdminAuthClient\Api\RolesApi as AdminUserRolesApi;

class DeleteAdminUserRoleAction
{
    public function __construct(
        protected AdminUserRolesApi $adminUserRolesApi,
    ) {
    }

    public function execute(int $id): void
    {
        $this->adminUserRolesApi->deleteRole($id);
    }
}
