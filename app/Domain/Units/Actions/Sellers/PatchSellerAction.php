<?php

namespace App\Domain\Units\Actions\Sellers;

use App\Domain\Units\Data\SellerData;
use Ensi\BuClient\Api\SellersApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\SellerForPatch;

class PatchSellerAction
{
    public function __construct(
        private SellersApi $sellersApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): SellerData
    {
        $seller = new SellerForPatch($fields);

        return new SellerData($this->sellersApi->patchSeller($id, $seller)->getData());
    }
}
