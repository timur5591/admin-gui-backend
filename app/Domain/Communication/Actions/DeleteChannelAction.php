<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\ChannelsApi;

/**
 * Class DeleteChannelAction
 * @package App\Domain\Communication\Actions
 */
class DeleteChannelAction
{
    public function __construct(protected ChannelsApi $channelsApi)
    {
    }

    public function execute(int $channelId)
    {
        return $this->channelsApi->deleteChannel($channelId)->getData();
    }
}
