<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\TypesApi;
use Ensi\CommunicationManagerClient\ApiException;

class DeleteTypeAction
{
    public function __construct(
        private TypesApi $typesApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $typeId)
    {
        return $this->typesApi->deleteType($typeId);
    }
}
