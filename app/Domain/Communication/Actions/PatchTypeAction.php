<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\TypesApi;
use Ensi\CommunicationManagerClient\ApiException;
use Ensi\CommunicationManagerClient\Dto\Type;
use Ensi\CommunicationManagerClient\Dto\TypeForPatch;

class PatchTypeAction
{
    public function __construct(
        private TypesApi $typesApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $typeId, array $fields): Type
    {
        $type = new TypeForPatch($fields);

        return $this->typesApi->patchType($typeId, $type)->getData();
    }
}
