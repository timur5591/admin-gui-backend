<?php

namespace App\Domain\Logistic\Tests\CargoOrders\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticCargoOrderStatusEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\LogisticClient\Dto\CargoOrders;
use Ensi\LogisticClient\Dto\CargoOrdersResponse;
use Ensi\LogisticClient\Dto\SearchCargoOrdersResponse;

class CargoOrdersFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'cargo_id' => $this->faker->modelId(),

            'timeslot_id' => $this->faker->uuid(),
            'timeslot_from' => $this->faker->dateTime(),
            'timeslot_to' => $this->faker->dateTime(),

            'cdek_intake_number' => $this->faker->unique()->numerify('######'),
            'external_id' => $this->faker->unique()->numerify('######'),
            'error_external_id' => $this->faker->text(20),
            'date' => $this->faker->dateTime(),
            'status' => $this->faker->randomElement(array_column(LogisticCargoOrderStatusEnum::cases(), 'value')),
        ];
    }

    public function make(array $extra = []): CargoOrders
    {
        return new CargoOrders($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): CargoOrdersResponse
    {
        return new CargoOrdersResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchCargoOrdersResponse
    {
        return $this->generateResponseSearch(SearchCargoOrdersResponse::class, $extras, $count, $pagination);
    }
}
