<?php

namespace App\Domain\Logistic\Tests\DeliveryServices\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\LogisticClient\Dto\MetroStation;

class MetroStationFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
            'metro_line_id' => $this->faker->modelId(),
            'name' => $this->faker->text(20),
            'city_guid' => $this->faker->uuid(),
        ];
    }

    public function make(array $extra = []): MetroStation
    {
        return new MetroStation($this->makeArray($extra));
    }
}
