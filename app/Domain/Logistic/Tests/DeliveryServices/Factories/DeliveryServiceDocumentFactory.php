<?php

namespace App\Domain\Logistic\Tests\DeliveryServices\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryServiceEnum;
use Ensi\LaravelEnsiFilesystem\Models\Tests\Factories\EnsiFileFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\LogisticClient\Dto\DeliveryService;
use Ensi\LogisticClient\Dto\DeliveryServiceDocument;
use Ensi\LogisticClient\Dto\DeliveryServiceDocumentResponse;
use Ensi\LogisticClient\Dto\File;
use Ensi\LogisticClient\Dto\SearchDeliveryServiceDocumentsResponse;

class DeliveryServiceDocumentFactory extends BaseApiFactory
{
    protected ?DeliveryService $deliveryService = null;

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->modelId(),
            'delivery_service_id' => $this->faker->randomElement(array_column(LogisticDeliveryServiceEnum::cases(), 'value')),
            'name' => $this->faker->text(20),
            'file' => new File(EnsiFileFactory::new()->make()),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if ($this->deliveryService) {
            $definition['delivery_service'] = $this->deliveryService;
        }

        return $definition;
    }

    public function make(array $extra = []): DeliveryServiceDocument
    {
        return new DeliveryServiceDocument($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): DeliveryServiceDocumentResponse
    {
        return new DeliveryServiceDocumentResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchDeliveryServiceDocumentsResponse
    {
        return $this->generateResponseSearch(SearchDeliveryServiceDocumentsResponse::class, $extras, $count, $pagination);
    }

    public function withDeliveryService(?DeliveryService $deliveryService = null): self
    {
        $this->deliveryService = $deliveryService ?: DeliveryServiceFactory::new()->make();

        return $this;
    }
}
