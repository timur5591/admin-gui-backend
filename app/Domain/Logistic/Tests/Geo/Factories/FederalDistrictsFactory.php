<?php

namespace App\Domain\Logistic\Tests\Geo\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\LogisticClient\Dto\FederalDistrict;
use Ensi\LogisticClient\Dto\FederalDistrictResponse;
use Ensi\LogisticClient\Dto\Region;
use Ensi\LogisticClient\Dto\SearchFederalDistrictsResponse;

class FederalDistrictsFactory extends BaseApiFactory
{
    protected array $regions = [];

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->modelId(),
            'name' => $this->faker->name(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if ($this->regions) {
            $definition['regions'] = $this->regions;
        }

        return $definition;
    }

    public function make(array $extra = []): FederalDistrict
    {
        return new FederalDistrict($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): FederalDistrictResponse
    {
        return new FederalDistrictResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchFederalDistrictsResponse
    {
        return $this->generateResponseSearch(SearchFederalDistrictsResponse::class, $extras, $count, $pagination);
    }

    public function withRegion(?Region $region = null): self
    {
        $this->regions[] = $region ?: RegionsFactory::new()->make();

        return $this;
    }
}
