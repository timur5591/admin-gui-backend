<?php

namespace App\Domain\Logistic\Tests\DeliveryKpis\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\LogisticClient\Dto\DeliveryKpiPpt;
use Ensi\LogisticClient\Dto\DeliveryKpiPptResponse;
use Ensi\LogisticClient\Dto\SearchDeliveryKpiPptResponse;

class DeliveryKpiPptFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'ppt' => $this->faker->randomNumber(),
            'seller_id' => $this->faker->modelId(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): DeliveryKpiPpt
    {
        return new DeliveryKpiPpt($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): DeliveryKpiPptResponse
    {
        return new DeliveryKpiPptResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchDeliveryKpiPptResponse
    {
        return $this->generateResponseSearch(SearchDeliveryKpiPptResponse::class, $extras, $count, $pagination);
    }
}
