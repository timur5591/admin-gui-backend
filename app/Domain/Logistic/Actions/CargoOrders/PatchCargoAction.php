<?php

namespace App\Domain\Logistic\Actions\CargoOrders;

use App\Domain\Logistic\Data\CargoData;
use Ensi\LogisticClient\Api\CargoOrdersApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\PatchCargoRequest;

class PatchCargoAction
{
    public function __construct(protected CargoOrdersApi $cargoOrdersApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $cargoId, array $fields): CargoData
    {
        return new CargoData($this->cargoOrdersApi->patchCargo($cargoId, new PatchCargoRequest($fields))->getData());
    }
}
