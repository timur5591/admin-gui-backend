<?php

namespace App\Domain\Logistic\Actions\CargoOrders;

use App\Domain\Logistic\Data\CargoData;
use Ensi\LogisticClient\Api\CargoOrdersApi;
use Ensi\LogisticClient\ApiException;

class CancelCargoAction
{
    public function __construct(protected CargoOrdersApi $cargoOrdersApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $cargoId): CargoData
    {
        return new CargoData($this->cargoOrdersApi->cancelCargo($cargoId)->getData());
    }
}
