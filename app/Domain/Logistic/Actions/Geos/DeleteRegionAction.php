<?php

namespace App\Domain\Logistic\Actions\Geos;

use Ensi\LogisticClient\Api\GeosApi;
use Ensi\LogisticClient\ApiException;

/**
 * Class DeleteRegionAction
 * @package App\Domain\Logistic\Actions\Geos
 */
class DeleteRegionAction
{
    protected GeosApi $geosApi;

    /**
     * DeleteRegionAction constructor.
     * @param  GeosApi  $geosApi
     */
    public function __construct(GeosApi $geosApi)
    {
        $this->geosApi = $geosApi;
    }

    /**
     * @param  int  $regionId
     * @throws ApiException
     */
    public function execute(int $regionId): void
    {
        $this->geosApi->deleteRegion($regionId);
    }
}
