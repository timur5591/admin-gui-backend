<?php

namespace App\Domain\Logistic\Actions\Geos;

use Ensi\LogisticClient\Api\GeosApi;
use Ensi\LogisticClient\ApiException;

/**
 * Class DeleteFederalDistrictAction
 * @package App\Domain\Logistic\Actions\Geos
 */
class DeleteFederalDistrictAction
{
    protected GeosApi $geosApi;

    /**
     * DeleteFederalDistrictAction constructor.
     * @param  GeosApi  $geosApi
     */
    public function __construct(GeosApi $geosApi)
    {
        $this->geosApi = $geosApi;
    }

    /**
     * @param  int  $federalDistrictId
     * @throws ApiException
     */
    public function execute(int $federalDistrictId): void
    {
        $this->geosApi->deleteFederalDistrict($federalDistrictId);
    }
}
