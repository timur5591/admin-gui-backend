<?php

namespace App\Domain\Logistic\Actions\DeliveryPrices;

use Ensi\LogisticClient\Api\DeliveryPricesApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\DeliveryPrice;
use Ensi\LogisticClient\Dto\PatchDeliveryPriceRequest;

/**
 * Class PatchDeliveryPriceAction
 * @package App\Domain\DeliveryPrices\Actions
 */
class PatchDeliveryPriceAction
{
    public function __construct(protected DeliveryPricesApi $deliveryPricesApi)
    {
    }

    /**
     * @param  int  $deliveryPriceId
     * @param  array  $fields
     * @return DeliveryPrice
     * @throws ApiException
     */
    public function execute(int $deliveryPriceId, array $fields): DeliveryPrice
    {
        $request = new PatchDeliveryPriceRequest();

        if (isset($fields['federal_district_id'])) {
            $request->setFederalDistrictId($fields['federal_district_id']);
        }
        if (isset($fields['region_id'])) {
            $request->setRegionId($fields['region_id']);
        }
        if (isset($fields['region_guid'])) {
            $request->setRegionGuid($fields['region_guid']);
        }
        if (isset($fields['delivery_service'])) {
            $request->setDeliveryService($fields['delivery_service']);
        }
        if (isset($fields['delivery_method'])) {
            $request->setDeliveryMethod($fields['delivery_method']);
        }
        if (isset($fields['price'])) {
            $request->setPrice($fields['price']);
        }

        return $this->deliveryPricesApi->patchDeliveryPrice($deliveryPriceId, $request)->getData();
    }
}
