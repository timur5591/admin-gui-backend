<?php

namespace App\Domain\Logistic\Actions\DeliveryServiceDocument;

use App\Domain\Logistic\Data\DeliveryServiceDocumentData;
use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Dto\CreateDeliveryServiceDocumentRequest;

class CreateDeliveryServiceDocumentAction
{
    public function __construct(protected DeliveryServicesApi $deliveryServicesApi)
    {
    }

    public function execute(array $fields): DeliveryServiceDocumentData
    {
        return new DeliveryServiceDocumentData($this->deliveryServicesApi->createDeliveryServiceDocument(
            new CreateDeliveryServiceDocumentRequest($fields)
        )->getData());
    }
}
