<?php

namespace App\Domain\Logistic\Actions\DeliveryServiceDocument;

use App\Domain\Logistic\Data\DeliveryServiceDocumentData;
use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Dto\PatchDeliveryServiceDocumentRequest;

class PatchDeliveryServiceDocumentAction
{
    public function __construct(protected DeliveryServicesApi $deliveryServicesApi)
    {
    }

    public function execute(int $deliveryServiceDocumentId, array $fields): DeliveryServiceDocumentData
    {
        return new DeliveryServiceDocumentData($this->deliveryServicesApi->patchDeliveryServiceDocument(
            $deliveryServiceDocumentId,
            new PatchDeliveryServiceDocumentRequest($fields)
        )->getData());
    }
}
