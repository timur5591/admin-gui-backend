<?php

namespace App\Domain\Logistic\Actions\DeliveryServices;

use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Dto\DeliveryService;
use Ensi\LogisticClient\Dto\PatchDeliveryServiceRequest;

class PatchDeliveryServiceAction
{
    public function __construct(protected DeliveryServicesApi $deliveryServicesApi)
    {
    }

    public function execute(int $deliveryServiceId, array $fields): DeliveryService
    {
        return $this->deliveryServicesApi
            ->patchDeliveryService($deliveryServiceId, new PatchDeliveryServiceRequest($fields))->getData();
    }
}
