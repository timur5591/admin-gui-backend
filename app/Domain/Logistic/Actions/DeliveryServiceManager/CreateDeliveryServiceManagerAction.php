<?php

namespace App\Domain\Logistic\Actions\DeliveryServiceManager;

use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Dto\CreateDeliveryServiceManagerRequest;
use Ensi\LogisticClient\Dto\DeliveryServiceManager;

class CreateDeliveryServiceManagerAction
{
    public function __construct(protected DeliveryServicesApi $deliveryServicesApi)
    {
    }

    public function execute(array $fields): DeliveryServiceManager
    {
        return $this->deliveryServicesApi->createDeliveryServiceManager(
            new CreateDeliveryServiceManagerRequest($fields)
        )->getData();
    }
}
