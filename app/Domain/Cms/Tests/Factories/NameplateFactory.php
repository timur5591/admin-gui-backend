<?php

namespace App\Domain\Cms\Tests\Factories;

use Ensi\CmsClient\Dto\Nameplate;
use Ensi\CmsClient\Dto\NameplateResponse;
use Ensi\CmsClient\Dto\SearchNameplatesResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class NameplateFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),

            'name' => $this->faker->text(50),
            'code' => $this->faker->unique()->text(50),

            'background_color' => $this->faker->hexColor(),
            'text_color' => $this->faker->hexColor(),

            'is_active' => $this->faker->boolean(),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): Nameplate
    {
        return new Nameplate($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): NameplateResponse
    {
        return new NameplateResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchNameplatesResponse
    {
        return $this->generateResponseSearch(SearchNameplatesResponse::class, $extras, $count, $pagination);
    }
}
