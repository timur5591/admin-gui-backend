<?php

namespace App\Domain\Cms\Actions\Nameplates;

use Ensi\CmsClient\Api\NameplatesApi;
use Ensi\CmsClient\Dto\Nameplate;
use Ensi\CmsClient\Dto\NameplateForCreate;

class CreateNameplateAction
{
    public function __construct(protected NameplatesApi $api)
    {
    }

    public function execute(array $fields): Nameplate
    {
        $request = new NameplateForCreate($fields);

        return $this->api->createNameplate($request)->getData();
    }
}
