<?php

namespace App\Domain\Cms\Actions\Nameplates;

use Ensi\CmsClient\Api\NameplatesApi;
use Ensi\CmsClient\Dto\Nameplate;
use Ensi\CmsClient\Dto\NameplateForPatch;

class PatchNameplateAction
{
    public function __construct(protected NameplatesApi $api)
    {
    }

    public function execute(int $id, array $fields): Nameplate
    {
        $request = new NameplateForPatch($fields);

        return $this->api->patchNameplate($id, $request)->getData();
    }
}
