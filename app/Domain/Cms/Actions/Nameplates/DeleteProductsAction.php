<?php

namespace App\Domain\Cms\Actions\Nameplates;

use Ensi\CmsClient\Api\NameplatesApi;
use Ensi\CmsClient\Dto\DeleteProductsRequest;

class DeleteProductsAction
{
    public function __construct(protected NameplatesApi $api)
    {
    }

    public function execute(int $nameplateId, array $productIds): void
    {
        $request = new DeleteProductsRequest(['ids' => $productIds]);

        $this->api->deleteNameplateProducts($nameplateId, $request);
    }
}
