<?php

namespace App\Domain\Cms\Actions\Products;

use Ensi\CmsClient\Api\ProductsApi;
use Ensi\CmsClient\Dto\DeleteNameplatesRequest;

class DeleteNameplatesAction
{
    public function __construct(protected ProductsApi $api)
    {
    }

    public function execute(int $productId, array $nameplateIds): void
    {
        $request = new DeleteNameplatesRequest(['ids' => $nameplateIds]);

        $this->api->deleteProductNameplates($productId, $request);
    }
}
