<?php

namespace App\Domain\Catalog\Actions\Products;

use App\Domain\Catalog\Actions\Offers\SetDefaultOfferDataAction;
use App\Domain\Catalog\Data\Products\ProductData;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\ReplaceProductRequest;
use Illuminate\Support\Arr;

class ReplaceProductAction
{
    public function __construct(
        private readonly ProductsApi $api,
        private readonly SetDefaultOfferDataAction $offerAction
    ) {
    }

    public function execute(int $productId, array $fields): ProductData
    {
        $request = new ReplaceProductRequest($fields);

        $product = $this->api->replaceProduct($productId, $request)->getData();

        $offer = $this->offerAction->execute($productId, Arr::only($fields, ['base_price']));

        return new ProductData($product, $offer);
    }
}
