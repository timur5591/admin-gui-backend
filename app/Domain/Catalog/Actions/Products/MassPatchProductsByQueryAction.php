<?php

namespace App\Domain\Catalog\Actions\Products;

use App\Domain\Catalog\Actions\Offers\SetDefaultOfferDataAction;
use App\Domain\Catalog\Actions\Offers\UpsertManyOffersByQueryAction;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\MassPatchProductsByQueryRequest;
use Illuminate\Support\Arr;

class MassPatchProductsByQueryAction
{
    public function __construct(
        private readonly ProductsApi $productsApi,
        private readonly UpsertManyOffersByQueryAction $offerAction,
    ) {
    }

    public function execute(array $productsFilter, array $fields, array $attributes): void
    {
        $productFields = Arr::except($fields, ['base_price']);
        $offerFields = Arr::only($fields, ['base_price']);

        if (!empty($productFields) || !empty($attributes)) {
            $request = new MassPatchProductsByQueryRequest([
                'filter' => $productsFilter,
                'fields' => $productFields,
                'attributes' => $attributes,
            ]);

            $this->productsApi->massPatchProductsByQuery($request);
        }

        if (!empty($offerFields)) {
            $this->setDefaultOffersData($productsFilter, $offerFields);
        }
    }

    private function setDefaultOffersData(array $productsFilter, array $fields): void
    {
        $this->offerAction->execute(
            productsFilter: $productsFilter,
            sellerId: SetDefaultOfferDataAction::DEFAULT_SELLER_ID,
            fields: $fields
        );
    }
}
