<?php

namespace App\Domain\Catalog\Actions\Products;

use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\ProductImage;
use Ensi\PimClient\Dto\ProductImageUploadRequest;

class UploadProductImageAction
{
    public function __construct(private readonly ProductsApi $api)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $productId, array $fields): ProductImage
    {
        $request = new ProductImageUploadRequest($fields);

        return $this->api->uploadProductImage($productId, $request)->getData();
    }
}
