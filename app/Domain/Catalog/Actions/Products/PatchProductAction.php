<?php

namespace App\Domain\Catalog\Actions\Products;

use App\Domain\Catalog\Actions\Offers\SetDefaultOfferDataAction;
use App\Domain\Catalog\Data\Products\ProductData;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\PatchProductRequest;
use Illuminate\Support\Arr;

class PatchProductAction
{
    public function __construct(
        private readonly ProductsApi $api,
        private readonly SetDefaultOfferDataAction $offerAction
    ) {
    }

    public function execute(int $productId, array $fields): ProductData
    {
        $request = new PatchProductRequest($fields);

        $product = $this->api->patchProduct($productId, $request)->getData();

        $offer = $this->offerAction->execute($productId, Arr::only($fields, ['base_price']));

        return new ProductData($product, $offer);
    }
}
