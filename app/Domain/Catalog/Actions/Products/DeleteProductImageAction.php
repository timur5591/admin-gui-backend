<?php

namespace App\Domain\Catalog\Actions\Products;

use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\ProductImageDeleteRequest;

class DeleteProductImageAction
{
    public function __construct(private readonly ProductsApi $api)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $productId, int $fileId): void
    {
        $request = (new ProductImageDeleteRequest())->setFileId($fileId);

        $this->api->deleteProductImage($productId, $request);
    }
}
