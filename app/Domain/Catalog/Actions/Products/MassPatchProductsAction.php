<?php

namespace App\Domain\Catalog\Actions\Products;

use App\Domain\Catalog\Actions\Offers\SetDefaultOfferDataAction;
use App\Domain\Catalog\Actions\Offers\UpsertManyOffersAction;
use App\Domain\Catalog\Data\Offers\UpsertOffersData;
use App\Domain\Catalog\Data\Products\MassPatchProductsResult;
use Ensi\OffersClient\Dto\MassOperationResultData as OffersResult;
use Ensi\OffersClient\Dto\OfferSaleStatusEnum;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\MassOperationResultData as ProductsResult;
use Ensi\PimClient\Dto\MassOperationResultDataErrors;
use Ensi\PimClient\Dto\MassPatchProductsRequest;
use Ensi\PimClient\Dto\SearchProductDraftsRequest;
use Illuminate\Support\Arr;

class MassPatchProductsAction
{
    private ?array $actualProductIds;

    public function __construct(
        private readonly ProductsApi $productsApi,
        private readonly UpsertManyOffersAction $offerAction,
    ) {
    }

    public function execute(array $productIds, array $fields, array $attributes): MassPatchProductsResult
    {
        $productFields = Arr::except($fields, ['base_price']);
        $offerFields = Arr::only($fields, ['base_price']);

        if (!empty($productFields) || !empty($attributes)) {
            $request = new MassPatchProductsRequest([
                'ids' => $productIds,
                'fields' => $productFields,
                'attributes' => $attributes,
            ]);
            $productsResult = $this->productsApi->massPatchProducts($request)->getData();

            $this->fillActualProducts($productsResult);
        }

        if (!empty($offerFields)) {
            $productIds = $this->actualProductIds ?? $this->verifyProductIds($productIds);

            if (!empty($productIds)) {
                $offersResult = $this->setDefaultOffersData($productIds, $offerFields);
            }
        }

        return new MassPatchProductsResult(
            productsResult: $productsResult ?? null,
            offersResult: $offersResult ?? null
        );
    }

    private function setDefaultOffersData(array $productIds, array $fields): OffersResult
    {
        $data = new UpsertOffersData();
        $data->sellerId = SetDefaultOfferDataAction::DEFAULT_SELLER_ID;
        $data->productIds = $productIds;
        $data->fields = [
            'sale_status' => OfferSaleStatusEnum::ON_SALE,
            ...$fields,
        ];

        return $this->offerAction->execute($data);
    }

    private function fillActualProducts(ProductsResult $result): void
    {
        $this->actualProductIds = array_merge(
            array_map(fn (MassOperationResultDataErrors $error) => $error->getId(), $result->getErrors()),
            $result->getProcessed()
        );
    }

    private function verifyProductIds(array $ids): array
    {
        $request = new SearchProductDraftsRequest();
        $request->setFilter((object)['id' => $ids]);

        $products = $this->productsApi->searchProductDrafts($request)->getData();

        $this->actualProductIds = collect($products)
            ->pluck('id')
            ->all();

        return $this->actualProductIds;
    }
}
