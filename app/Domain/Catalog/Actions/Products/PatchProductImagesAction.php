<?php

namespace App\Domain\Catalog\Actions\Products;

use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\PatchProductImagesRequest;

class PatchProductImagesAction
{
    public function __construct(private ProductsApi $api)
    {
    }

    public function execute(int $productId, array $images): array
    {
        $request = new PatchProductImagesRequest(['images' => $images]);

        return $this->api->patchProductImages($productId, $request)->getData();
    }
}
