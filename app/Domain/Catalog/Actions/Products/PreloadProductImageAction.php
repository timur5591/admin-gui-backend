<?php

namespace App\Domain\Catalog\Actions\Products;

use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\PreloadFileData;
use Illuminate\Http\UploadedFile;

class PreloadProductImageAction
{
    public function __construct(private readonly ProductsApi $api)
    {
    }

    public function execute(UploadedFile $file): PreloadFileData
    {
        return $this->api->preloadProductImage($file)->getData();
    }
}
