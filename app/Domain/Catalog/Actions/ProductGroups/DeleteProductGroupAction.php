<?php

namespace App\Domain\Catalog\Actions\ProductGroups;

use Ensi\PimClient\Api\ProductGroupsApi;

class DeleteProductGroupAction
{
    public function __construct(private ProductGroupsApi $api)
    {
    }

    public function execute(int $groupId): void
    {
        $this->api->deleteProductGroup($groupId);
    }
}
