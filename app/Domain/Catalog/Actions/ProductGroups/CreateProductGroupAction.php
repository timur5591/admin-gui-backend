<?php

namespace App\Domain\Catalog\Actions\ProductGroups;

use Ensi\PimClient\Api\ProductGroupsApi;
use Ensi\PimClient\Dto\CreateProductGroupRequest;
use Ensi\PimClient\Dto\ProductGroup;

class CreateProductGroupAction
{
    public function __construct(private ProductGroupsApi $api)
    {
    }

    public function execute(array $fields): ProductGroup
    {
        $request = new CreateProductGroupRequest($fields);

        return $this->api->createProductGroup($request)->getData();
    }
}
