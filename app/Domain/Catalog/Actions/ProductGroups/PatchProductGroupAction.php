<?php

namespace App\Domain\Catalog\Actions\ProductGroups;

use Ensi\PimClient\Api\ProductGroupsApi;
use Ensi\PimClient\Dto\PatchProductGroupRequest;
use Ensi\PimClient\Dto\ProductGroup;

class PatchProductGroupAction
{
    public function __construct(private ProductGroupsApi $api)
    {
    }

    public function execute(int $groupId, array $fields): ProductGroup
    {
        $request = new PatchProductGroupRequest($fields);

        return $this->api->patchProductGroup($groupId, $request)->getData();
    }
}
