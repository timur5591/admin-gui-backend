<?php

namespace App\Domain\Catalog\Actions\ProductGroups;

use Ensi\PimClient\Api\ProductGroupsApi;
use Ensi\PimClient\Dto\ProductGroup;
use Ensi\PimClient\Dto\ReplaceProductGroupRequest;

class ReplaceProductGroupAction
{
    public function __construct(private ProductGroupsApi $api)
    {
    }

    public function execute(int $groupId, array $fields): ProductGroup
    {
        $request = new ReplaceProductGroupRequest($fields);

        return $this->api->replaceProductGroup($groupId, $request)->getData();
    }
}
