<?php

namespace App\Domain\Catalog\Actions\ProductGroups;

use Ensi\PimClient\Api\ProductGroupsApi;
use Ensi\PimClient\Dto\RequestBodyMassOperation;

class DeleteManyProductGroupsAction
{
    public function __construct(private ProductGroupsApi $api)
    {
    }

    public function execute(array $propertyGroupsId): void
    {
        $request = new RequestBodyMassOperation($propertyGroupsId);

        $this->api->deleteProductGroups($request);
    }
}
