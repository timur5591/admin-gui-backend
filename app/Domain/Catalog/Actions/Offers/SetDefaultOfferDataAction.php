<?php

namespace App\Domain\Catalog\Actions\Offers;

use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\OfferSaleStatusEnum;
use Ensi\OffersClient\Dto\SearchOffersRequest;

class SetDefaultOfferDataAction
{
    public const DEFAULT_SELLER_ID = 0;

    public function __construct(
        private readonly OffersApi $api,
        private readonly CreateOfferAction $createAction,
        private readonly PatchOfferAction $patchAction
    ) {
    }

    public function execute(int $productId, array $fields): Offer
    {
        $offer = $this->findOffer($productId);

        return $offer !== null
            ? $this->updateOffer($offer->getId(), $fields)
            : $this->createOffer($productId, $fields);
    }

    private function findOffer(int $productId): ?Offer
    {
        $filter = ['product_id' => $productId, 'seller_id' => self::DEFAULT_SELLER_ID];
        $request = new SearchOffersRequest();
        $request->setFilter((object)$filter);

        $response = $this->api->searchOffers($request)->getData();

        return empty($response) ? null : $response[0];
    }

    private function createOffer(int $productId, array $fields): Offer
    {
        return $this->createAction->execute([
            'product_id' => $productId,
            'seller_id' => self::DEFAULT_SELLER_ID,
            'sale_status' => OfferSaleStatusEnum::ON_SALE,
            'base_price' => $fields['base_price'] ?? null,
        ]);
    }

    private function updateOffer(int $offerId, array $fields): Offer
    {
        return $this->patchAction->execute($offerId, $fields);
    }
}
