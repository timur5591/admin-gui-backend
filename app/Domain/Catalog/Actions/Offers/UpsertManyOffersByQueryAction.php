<?php

namespace App\Domain\Catalog\Actions\Offers;

use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Dto\MassUpsertOffersByQueryRequest;

class UpsertManyOffersByQueryAction
{
    public function __construct(private OffersApi $api)
    {
    }

    public function execute(array $productsFilter, int $sellerId, array $fields): void
    {
        $request = new MassUpsertOffersByQueryRequest([
            'products_filter' => $productsFilter,
            'seller_id' => $sellerId,
            'fields' => $fields,
        ]);

        $this->api->massUpsertOffersByQuery($request);
    }
}
