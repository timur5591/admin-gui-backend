<?php

namespace App\Domain\Catalog\Actions\Offers;

use App\Domain\Catalog\Data\Offers\UpsertOffersData;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Dto\MassOperationResultData;
use Ensi\OffersClient\Dto\MassUpsertOffersRequest;

class UpsertManyOffersAction
{
    public function __construct(private OffersApi $api)
    {
    }

    public function execute(UpsertOffersData $data): MassOperationResultData
    {
        $request = new MassUpsertOffersRequest([
            'seller_id' => $data->sellerId,
            'product_ids' => $data->productIds,
            'fields' => $data->fields,
        ]);

        return $this->api->massUpsertOffers($request)->getData();
    }
}
