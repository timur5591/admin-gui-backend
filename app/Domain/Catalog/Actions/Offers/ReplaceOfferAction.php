<?php

namespace App\Domain\Catalog\Actions\Offers;

use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\ReplaceOfferRequest;

class ReplaceOfferAction
{
    public function __construct(private OffersApi $api)
    {
    }

    public function execute(int $offerId, array $fields): Offer
    {
        $request = new ReplaceOfferRequest($fields);

        return $this->api->replaceOffer($offerId, $request)->getData();
    }
}
