<?php

namespace App\Domain\Catalog\Actions\Properties;

use Ensi\PimClient\Api\PropertiesApi;
use Ensi\PimClient\Dto\PreloadFileData;
use Illuminate\Http\UploadedFile;

class PreloadDirectoryFileAction
{
    public function __construct(private readonly PropertiesApi $api)
    {
    }

    public function execute(UploadedFile $file): PreloadFileData
    {
        return $this->api->preloadDirectoryValueFile($file)->getData();
    }
}
