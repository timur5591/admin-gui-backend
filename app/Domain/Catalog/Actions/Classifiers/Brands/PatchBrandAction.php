<?php

namespace App\Domain\Catalog\Actions\Classifiers\Brands;

use Ensi\PimClient\Api\BrandsApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\Brand;
use Ensi\PimClient\Dto\PatchBrandRequest;

class PatchBrandAction
{
    public function __construct(
        private readonly BrandsApi $brandsApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $brandId, array $fields): Brand
    {
        $request = new PatchBrandRequest($fields);

        return $this->brandsApi->patchBrand($brandId, $request)->getData();
    }
}
