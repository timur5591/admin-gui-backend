<?php

namespace App\Domain\Catalog\Tests\Factories\Categories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\DirectoryValue;
use Ensi\PimClient\Dto\DirectoryValueResponse;
use Ensi\PimClient\Dto\File;
use Ensi\PimClient\Dto\MassDirectoryValueResponse;
use Ensi\PimClient\Dto\SearchDirectoryValuesResponse;
use Faker\Generator;
use Tests\Helpers\Catalog\PropertyValueGenerator;

class DirectoryValueFactory extends BaseApiFactory
{
    private PropertyValueGenerator $valueGenerator;
    public ?File $file = null;

    public function __construct(Generator $faker)
    {
        parent::__construct($faker);

        $this->valueGenerator = PropertyValueGenerator::new($this->faker);
    }

    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'property_id' => $this->propertyId ?? $this->faker->modelId(),
            'name' => $this->faker->sentence(3),
            'code' => $this->faker->slug(3),
            'value' => $this->valueGenerator->value(),
            'type' => $this->valueGenerator->type(),
            'file' => $this->notNull($this->file),
        ];
    }

    public function make(array $extra = []): DirectoryValue
    {
        return new DirectoryValue($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): DirectoryValueResponse
    {
        return new DirectoryValueResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchDirectoryValuesResponse
    {
        return $this->generateResponseSearch(SearchDirectoryValuesResponse::class, $extras, $count, $pagination);
    }

    public function makeResponseBatch(array $batch, array $extra = []): MassDirectoryValueResponse
    {
        $data = [];
        foreach ($batch as $item) {
            $data[] = $this->make($this->makeArray(array_merge($item, $extra)));
        }

        return new MassDirectoryValueResponse([
            'data' => $data,
        ]);
    }

    public function withFile(?string $url = null): self
    {
        $url ??= $this->faker->url;
        $file = new File(['url' => $url]);

        return $this->immutableSet('file', $file);
    }
}
