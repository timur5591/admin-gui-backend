<?php

namespace App\Domain\Catalog\Tests\Factories\Products;

use App\Domain\Catalog\Tests\Factories\Categories\CategoryFactory;
use App\Domain\Catalog\Tests\Factories\Classifiers\BrandFactory;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\Brand;
use Ensi\PimClient\Dto\Category;
use Ensi\PimClient\Dto\File;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\ProductImage;
use Ensi\PimClient\Dto\ProductResponse;
use Ensi\PimClient\Dto\ProductTypeEnum;
use Ensi\PimClient\Dto\SearchProductsResponse;

class ProductFactory extends BaseApiFactory
{
    protected array $productImages = [];
    protected ?Category $category = null;
    protected ?Brand $brand = null;
    protected ?bool $noFilledRequiredAttributes = null;

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->modelId(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),

            'external_id' => $this->faker->uuid(),
            'category_id' => $this->faker->modelId(),
            'brand_id' => $this->faker->modelId(),

            'name' => $this->faker->sentence(3),
            'code' => $this->faker->slug(),
            'description' => $this->faker->text(50),
            'type' => $this->faker->randomElement(ProductTypeEnum::getAllowableEnumValues()),
            'allow_publish' => $this->faker->boolean,
            'vendor_code' => $this->faker->numerify('######'),
            'barcode' => $this->faker->ean13(),

            'weight' => $this->faker->randomFloat(4),
            'weight_gross' => $this->faker->randomFloat(4),
            'length' => $this->faker->randomNumber(),
            'width' => $this->faker->randomNumber(),
            'height' => $this->faker->randomNumber(),
            'is_adult' => $this->faker->boolean(),

            ...$this->getSomeImageField(),
        ];

        if ($this->productImages) {
            $definition['images'] = $this->productImages;
        }
        if ($this->category) {
            $definition['category'] = $this->category;
        }
        if ($this->brand) {
            $definition['brand'] = $this->brand;
        }

        if (!is_null($this->noFilledRequiredAttributes)) {
            $definition['no_filled_required_attributes'] = $this->noFilledRequiredAttributes;
        }

        return $definition;
    }

    public function make(array $extra = []): Product
    {
        return new Product($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): ProductResponse
    {
        return new ProductResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchProductsResponse
    {
        return $this->generateResponseSearch(SearchProductsResponse::class, $extras, $count, $pagination);
    }

    protected function getSomeImageField(): array
    {
        $mainImageFile = null;
        $mainImageUrl = $this->faker->nullable()->imageUrl;
        if (!$mainImageUrl) {
            $mainImageFile = new File(EnsiFile::factory()->make());
        }

        return [
            'main_image_url' => $mainImageUrl,
            'main_image_file' => $mainImageFile,
        ];
    }

    public function withImages(?ProductImage $productImage = null): self
    {
        $this->productImages[] = $productImage ?: ProductImageFactory::new()->make();

        return $this;
    }

    public function withCategory(?Category $category = null): self
    {
        $this->category = $category ?: CategoryFactory::new()->make();

        return $this;
    }

    public function withBrand(?Brand $brand = null): self
    {
        $this->brand = $brand ?: BrandFactory::new()->make();

        return $this;
    }

    public function withNoFilledRequiredAttributes(?bool $noFilledRequiredAttributes = null): self
    {
        $this->noFilledRequiredAttributes = !is_null($noFilledRequiredAttributes)
            ? $noFilledRequiredAttributes
            : $this->faker->boolean;

        return $this;
    }
}
