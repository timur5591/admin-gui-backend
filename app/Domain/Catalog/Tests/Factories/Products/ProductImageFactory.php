<?php

namespace App\Domain\Catalog\Tests\Factories\Products;

use Ensi\LaravelEnsiFilesystem\Models\Tests\Factories\EnsiFileFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\File;
use Ensi\PimClient\Dto\ProductImage;
use Ensi\PimClient\Dto\ProductImageResponse;
use Ensi\PimClient\Dto\ProductImagesResponse;

class ProductImageFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $isExternal = $this->faker->boolean;

        return [
            'id' => $this->faker->modelId(),
            'sort' => $this->faker->numberBetween(1, 500),
            'name' => $this->faker->nullable()->sentence(3),
            'url' => $this->when($isExternal, $this->faker->url),
            'file' => $this->when(!$isExternal, fn () => new File(EnsiFileFactory::new()->make())),
        ];
    }

    public function make(array $extra = []): ProductImage
    {
        return new ProductImage($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): ProductImageResponse
    {
        return new ProductImageResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSeveral(int $count = 1, array $extra = []): ProductImagesResponse
    {
        return new ProductImagesResponse([
            'data' => $this->makeSeveral($count, $extra)->all(),
        ]);
    }
}
