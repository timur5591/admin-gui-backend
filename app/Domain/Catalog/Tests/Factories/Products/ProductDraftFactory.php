<?php

namespace App\Domain\Catalog\Tests\Factories\Products;

use App\Domain\Catalog\Tests\Factories\Categories\CategoryFactory;
use App\Domain\Catalog\Tests\Factories\Classifiers\BrandFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\Brand;
use Ensi\PimClient\Dto\Category;
use Ensi\PimClient\Dto\ProductDraft;
use Ensi\PimClient\Dto\ProductDraftResponse;
use Ensi\PimClient\Dto\ProductTypeEnum;
use Ensi\PimClient\Dto\SearchProductDraftsResponse;
use Ensi\TestFactories\FactoryMissingValue;
use Illuminate\Support\Collection;

class ProductDraftFactory extends BaseApiFactory
{
    public ?Brand $brand = null;
    public ?Category $category = null;
    public ?Collection $attributes = null;
    public ?Collection $images = null;

    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),

            'external_id' => $this->faker->uuid(),
            'category_id' => $this->faker->modelId(),
            'brand_id' => $this->faker->modelId(),

            'name' => $this->faker->sentence(3),
            'code' => $this->faker->slug(),
            'description' => $this->faker->text(50),
            'type' => $this->faker->randomElement(ProductTypeEnum::getAllowableEnumValues()),
            'allow_publish' => $this->faker->boolean,
            'vendor_code' => $this->faker->numerify('######'),
            'barcode' => $this->faker->ean13(),

            'weight' => $this->faker->randomFloat(4),
            'weight_gross' => $this->faker->randomFloat(4),
            'length' => $this->faker->randomNumber(),
            'width' => $this->faker->randomNumber(),
            'height' => $this->faker->randomNumber(),
            'is_adult' => $this->faker->boolean(),

            'brand' => $this->notNull($this->brand),
            'category' => $this->notNull($this->category),
            'attributes' => $this->executeNested($this->attributes, new FactoryMissingValue()),
            'images' => $this->executeNested($this->images, new FactoryMissingValue()),
        ];
    }

    public function make(array $extra = []): ProductDraft
    {
        return new ProductDraft($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): ProductDraftResponse
    {
        return new ProductDraftResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchProductDraftsResponse
    {
        return $this->generateResponseSearch(SearchProductDraftsResponse::class, $extras, $count, $pagination);
    }

    public function withBrand(): self
    {
        return $this->immutableSet('brand', BrandFactory::new()->make());
    }

    public function withCategory(): self
    {
        return $this->immutableSet('category', CategoryFactory::new()->make());
    }

    public function withAttributes(int $count = 1): self
    {
        $attributes = Collection::times($count, fn () => ProductPropertyValueFactory::new());

        return $this->immutableSet('attributes', $attributes);
    }

    public function withImages(int $count = 1): self
    {
        $images = Collection::times($count, fn () => ProductImageFactory::new());

        return $this->immutableSet('images', $images);
    }
}
