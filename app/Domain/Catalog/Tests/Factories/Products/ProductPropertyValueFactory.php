<?php

namespace App\Domain\Catalog\Tests\Factories\Products;

use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\File;
use Ensi\PimClient\Dto\ProductAttributesResponse;
use Ensi\PimClient\Dto\ProductAttributeValue;
use Ensi\PimClient\Dto\PropertyTypeEnum;
use Faker\Generator;
use Tests\Helpers\Catalog\PropertyValueGenerator;

class ProductPropertyValueFactory extends BaseApiFactory
{
    private PropertyValueGenerator $valueGenerator;
    public ?File $file = null;

    public function __construct(Generator $faker)
    {
        parent::__construct($faker);

        $this->valueGenerator = PropertyValueGenerator::new($this->faker);
    }

    protected function definition(): array
    {
        return [
            'property_id' => $this->faker->randomNumber(),
            'value' => $this->valueGenerator->value(),
            'name' => $this->faker->nullable()->sentence,
            'type' => $this->valueGenerator->type(),
            'directory_value_id' => $this->faker->nullable()->modelId(),
        ];
    }

    public function make(array $extra = []): ProductAttributeValue
    {
        return new ProductAttributeValue($this->makeArray($extra));
    }

    public function makeResponse(int $count = 1, array $extra = []): ProductAttributesResponse
    {
        return new ProductAttributesResponse([
            'data' => $this->makeSeveral($count, $extra)
                ->map(function (ProductAttributeValue $attributeValue) {
                    if (in_array($attributeValue->getType(), [PropertyTypeEnum::IMAGE, PropertyTypeEnum::FILE])) {
                        $attributeValue->setFile(new File(EnsiFile::factory()->make()));
                    }

                    return $attributeValue;
                })
                ->all(),
        ]);
    }

    public static function generateValue(string $type, Generator $faker): mixed
    {
        return PropertyValueGenerator::new($faker, $type)->value();
    }
}
