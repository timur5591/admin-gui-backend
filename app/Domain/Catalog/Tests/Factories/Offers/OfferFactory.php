<?php

namespace App\Domain\Catalog\Tests\Factories\Offers;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\OfferResponse;
use Ensi\OffersClient\Dto\OfferSaleStatusEnum;
use Ensi\OffersClient\Dto\SearchOffersResponse;
use Ensi\OffersClient\Dto\Stock;

class OfferFactory extends BaseApiFactory
{
    protected array $stocks = [];

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
            'seller_id' => $this->faker->modelId(),
            'external_id' => $this->faker->numerify('##-##'),
            'sale_status' => $this->faker->randomElement(OfferSaleStatusEnum::getAllowableEnumValues()),
            'storage_address' => $this->faker->nullable()->address,
            'base_price' => $this->faker->numberBetween(0, 100_000),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if ($this->stocks) {
            $definition['stocks'] = $this->stocks;
        }

        return $definition;
    }

    public function make(array $extra = []): Offer
    {
        return new Offer($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): OfferResponse
    {
        return new OfferResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchOffersResponse
    {
        return $this->generateResponseSearch(SearchOffersResponse::class, $extras, $count, $pagination);
    }

    public function withStock(?Stock $stock = null): self
    {
        $this->stocks[] = $stock ?: StockFactory::new()->make();

        return $this;
    }
}
