<?php

namespace App\Domain\Catalog\Tests\Factories\Offers;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OffersClient\Dto\SearchStocksResponse;
use Ensi\OffersClient\Dto\Stock;

class StockFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'store_id' => $this->faker->modelId(),
            'offer_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
            'qty' => $this->faker->randomFloat(4),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): Stock
    {
        return new Stock($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchStocksResponse
    {
        return $this->generateResponseSearch(SearchStocksResponse::class, $extras, $count, $pagination);
    }
}
