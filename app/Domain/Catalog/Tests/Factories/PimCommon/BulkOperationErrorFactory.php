<?php

namespace App\Domain\Catalog\Tests\Factories\PimCommon;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\BulkOperationError;

class BulkOperationErrorFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'operation_id' => $this->faker->modelId(),
            'entity_id' => $this->faker->modelId(),
            'message' => $this->faker->text(100),
        ];
    }

    public function make(array $extra = []): BulkOperationError
    {
        return new BulkOperationError($this->makeArray($extra));
    }
}
