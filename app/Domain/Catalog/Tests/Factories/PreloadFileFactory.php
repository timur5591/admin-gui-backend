<?php

namespace App\Domain\Catalog\Tests\Factories;

use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\File;
use Ensi\PimClient\Dto\PreloadFile;
use Ensi\PimClient\Dto\PreloadFileData;

class PreloadFileFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'preload_file_id' => $this->faker->randomNumber(5),
            'file' => new File(EnsiFile::factory()->make()),
        ];
    }

    public function make(array $extra = []): PreloadFile
    {
        $data = $this->makeArray($extra);

        return new PreloadFile(['data' => new PreloadFileData($data)]);
    }
}
