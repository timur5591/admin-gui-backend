<?php

namespace App\Domain\Catalog\Data\Offers;

class UpsertOffersData
{
    public int $sellerId;
    public array $productIds;
    public array $fields;
}
