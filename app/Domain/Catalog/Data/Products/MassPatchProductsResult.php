<?php

namespace App\Domain\Catalog\Data\Products;

use Ensi\OffersClient\Dto\MassOperationResultData as OffersResult;
use Ensi\PimClient\Dto\MassOperationResultData as ProductsResult;

class MassPatchProductsResult
{
    public function __construct(
        public readonly ?ProductsResult $productsResult,
        public readonly ?OffersResult $offersResult,
    ) {
    }
}
