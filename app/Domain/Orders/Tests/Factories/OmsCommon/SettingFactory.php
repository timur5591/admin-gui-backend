<?php

namespace App\Domain\Orders\Tests\Factories\OmsCommon;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OmsClient\Dto\Setting;
use Ensi\OmsClient\Dto\SettingsResponse;

class SettingFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'code' => $this->faker->unique()->text(50),
            'name' => $this->faker->text(50),
            'value' => $this->faker->numerify('##'),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): Setting
    {
        return new Setting($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = []): SettingsResponse
    {
        return new SettingsResponse([
            'data' => [$this->make($extra)],
        ]);
    }
}
