<?php

namespace App\Domain\Orders\Tests\Factories\Orders;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OmsClient\Dto\OrderSource;
use Ensi\OmsClient\Dto\OrderSourcesResponse;

class OrderSourceFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'name' => $this->faker->text(20),
        ];
    }

    public function make(array $extra = []): OrderSource
    {
        return new OrderSource($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = []): OrderSourcesResponse
    {
        return new OrderSourcesResponse([
            'data' => [$this->make($extra)],
        ]);
    }
}
