<?php

namespace App\Domain\Orders\Tests\Factories\Orders;

use Ensi\LaravelEnsiFilesystem\Models\Tests\Factories\EnsiFileFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OmsClient\Dto\File;
use Ensi\OmsClient\Dto\OrderAttachFileResponse;
use Ensi\OmsClient\Dto\OrderFile;

class OrderFileFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'order_id' => $this->faker->modelId(),
            'file' => new File(EnsiFileFactory::new()->make()),
            'original_name' => $this->faker->word() . '.' . $this->faker->fileExtension(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): OrderFile
    {
        return new OrderFile($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): OrderAttachFileResponse
    {
        return new OrderAttachFileResponse(['data' => $this->make($extra)]);
    }
}
