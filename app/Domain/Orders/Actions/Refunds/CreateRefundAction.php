<?php

namespace App\Domain\Orders\Actions\Refunds;

use App\Domain\Orders\Data\Refunds\RefundData;
use Ensi\OmsClient\Api\RefundsApi;
use Ensi\OmsClient\Dto\RefundForCreate;
use Ensi\PimClient\ApiException;

class CreateRefundAction
{
    public function __construct(
        private RefundsApi $refundsApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): RefundData
    {
        $request = new RefundForCreate($fields);

        $refund = $this->refundsApi->createRefund($request)->getData();

        return new RefundData($refund);
    }
}
