<?php

namespace App\Domain\Orders\Data\Refunds;

use App\Domain\Catalog\Data\Offers\OfferData;
use App\Domain\Orders\Data\Orders\OrderData;
use App\Domain\Orders\Data\Orders\OrderItemData;
use Ensi\OmsClient\Dto\Refund;
use Ensi\PimClient\Dto\Product;

class RefundData
{
    /** @var OfferData[] */
    public array $offersData = [];
    /** @var Product[] */
    public array $products = [];

    public function __construct(public Refund $refund)
    {
    }

    public function getOrder(): ?OrderData
    {
        if (is_null($this->refund->getOrder())) {
            return null;
        }

        return new OrderData($this->refund->getOrder());
    }

    public function getOrderItems(): ?array
    {
        if (is_null($this->refund->getItems())) {
            return null;
        }

        $orderItemsData = [];
        $offersData = collect($this->offersData)->keyBy('offer.id');
        $products = collect($this->products)->keyBy('id');
        foreach ($this->refund->getItems() as $orderItem) {
            $orderItemData = new OrderItemData($orderItem);

            $orderItemData->offerData = $offersData->get($orderItem->getOfferId());
            if ($orderItemData->offerData) {
                $orderItemData->product = $products->get($orderItemData->offerData->offer->getProductId());
            }

            $orderItemsData[] = $orderItemData;
        }

        return $orderItemsData;
    }
}
