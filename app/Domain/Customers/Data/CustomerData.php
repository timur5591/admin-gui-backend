<?php

namespace App\Domain\Customers\Data;

use Ensi\CustomerAuthClient\Dto\User;
use Ensi\CustomersClient\Dto\Customer;

/**
 * Class CustomerData
 * @package App\Domain\Customers\Data
 */
class CustomerData
{
    public ?User $user = null;

    /**
     * CustomerData constructor.
     * @param Customer $customer
     */
    public function __construct(public Customer $customer)
    {
    }

    /**
     * @param User|null $user
     * @return $this
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
