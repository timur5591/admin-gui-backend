<?php

namespace App\Domain\Customers\Actions\Statuses;

use Ensi\CustomersClient\Api\StatusesApi;

abstract class CustomerStatusAction
{
    /**
     * CustomerAddressAction constructor.
     * @param StatusesApi $statusApi
     */
    public function __construct(protected StatusesApi $statusApi)
    {
    }
}
