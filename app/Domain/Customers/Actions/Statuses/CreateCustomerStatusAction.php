<?php

namespace App\Domain\Customers\Actions\Statuses;

use Ensi\CustomersClient\Dto\CustomerStatuses;
use Ensi\CustomersClient\Dto\CustomerStatusForCreateOrReplace;

class CreateCustomerStatusAction extends CustomerStatusAction
{
    /**
     * @param array $fields
     * @return CustomerStatuses
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function execute(array $fields): CustomerStatuses
    {
        return $this->statusApi->createCustomerStatus(new CustomerStatusForCreateOrReplace($fields))->getData();
    }
}
