<?php

namespace App\Domain\Customers\Actions\BonusOperations;

use Ensi\CrmClient\Dto\BonusOperation;
use Ensi\CrmClient\Dto\BonusOperationForCreate;

class CreateBonusOperationsAction extends BonusOperationsAction
{
    /**
     * @param array $fields
     * @return BonusOperation
     * @throws \Ensi\CrmClient\ApiException
     */
    public function execute(array $fields): BonusOperation
    {
        return $this->bonusOperationsApi->createBonusOperation(new BonusOperationForCreate($fields))->getData();
    }
}
