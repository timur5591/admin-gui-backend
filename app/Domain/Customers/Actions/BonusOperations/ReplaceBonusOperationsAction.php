<?php

namespace App\Domain\Customers\Actions\BonusOperations;

use Ensi\CrmClient\Dto\BonusOperation;
use Ensi\CrmClient\Dto\BonusOperationForPatch;

class ReplaceBonusOperationsAction extends BonusOperationsAction
{
    /**
     * @param int $bonusOperationId
     * @param array $fields
     * @return BonusOperation
     * @throws \Ensi\CrmClient\ApiException
     */
    public function execute(int $bonusOperationId, array $fields): BonusOperation
    {
        return $this->bonusOperationsApi->patchBonusOperation(
            $bonusOperationId,
            new BonusOperationForPatch($fields)
        )->getData();
    }
}
