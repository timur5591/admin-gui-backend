<?php

namespace App\Domain\Customers\Actions\Addresses;

/**
 * Class SetDefaultCustomerAddressAction
 * @package App\Domain\Customers\Actions\Addresses
 */
class SetDefaultCustomerAddressAction extends CustomerAddressAction
{
    /**
     * @param int $addressId
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function execute(int $addressId): void
    {
        $this->addressesApi->setCustomerAddressesAsDefault($addressId);
    }
}
