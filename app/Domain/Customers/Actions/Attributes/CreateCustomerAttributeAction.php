<?php

namespace App\Domain\Customers\Actions\Attributes;

use Ensi\CustomersClient\Dto\CustomerAttributeForCreateOrReplace;
use Ensi\CustomersClient\Dto\CustomerAttributes;

class CreateCustomerAttributeAction extends CustomerAttributeAction
{
    /**
     * @param array $fields
     * @return CustomerAttributes
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function execute(array $fields): CustomerAttributes
    {
        return $this->attributesApi->createCustomerAttributes(new CustomerAttributeForCreateOrReplace($fields))->getData();
    }
}
