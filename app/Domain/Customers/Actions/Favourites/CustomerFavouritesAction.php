<?php

namespace App\Domain\Customers\Actions\Favourites;

use Ensi\CrmClient\Api\CustomerFavoritesApi;

abstract class CustomerFavouritesAction
{
    public function __construct(protected CustomerFavoritesApi $customerFavoritesApi)
    {
    }
}
