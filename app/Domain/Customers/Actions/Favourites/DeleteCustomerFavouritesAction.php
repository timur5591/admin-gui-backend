<?php

namespace App\Domain\Customers\Actions\Favourites;

use Ensi\CrmClient\Dto\DeleteProductFromCustomerFavoritesRequest;

class DeleteCustomerFavouritesAction extends CustomerFavouritesAction
{
    public function execute(array $fields): void
    {
        $this->customerFavoritesApi->deleteProductFromCustomerFavorites(
            new DeleteProductFromCustomerFavoritesRequest($fields)
        );
    }
}
