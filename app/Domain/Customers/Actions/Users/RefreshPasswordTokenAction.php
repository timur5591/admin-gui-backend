<?php

namespace App\Domain\Customers\Actions\Users;

use Ensi\CustomerAuthClient\Api\UsersApi as CustomerUsersApi;

class RefreshPasswordTokenAction
{
    public function __construct(
        protected CustomerUsersApi $customerUsersApi,
    ) {
    }

    public function execute(int $userId): void
    {
        $this->customerUsersApi->refreshPasswordToken($userId);
    }
}
