<?php

namespace App\Domain\Customers\Actions\Users;

use Ensi\CustomerAuthClient\ApiException;

/**
 * Class DeleteCustomerUserAction
 * @package App\Domain\Customers\Actions\Users
 */
class DeleteCustomerUserAction extends CustomerUserAction
{
    /**
     * @param  int  $customerUserId
     * @throws ApiException
     */
    public function execute(int $customerUserId): void
    {
        $this->usersApi->deleteUser($customerUserId);
    }
}
