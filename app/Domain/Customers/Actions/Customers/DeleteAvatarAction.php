<?php

namespace App\Domain\Customers\Actions\Customers;

use App\Domain\Customers\Data\CustomerData;

/**
 * Class DeleteAvatarAction
 * @package App\Domain\Customers\Actions\Customers
 */
class DeleteAvatarAction extends CustomerAction
{
    /**
     * @param int $customerId
     * @return CustomerData
     * @throws \Ensi\CustomerAuthClient\ApiException
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function execute(int $customerId): CustomerData
    {
        return $this->prepareCustomer($this->customersApi->deleteCustomerAvatar($customerId)->getData());
    }
}
