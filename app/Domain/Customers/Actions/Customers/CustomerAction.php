<?php

namespace App\Domain\Customers\Actions\Customers;

use App\Domain\Customers\Data\CustomerData;
use Ensi\CustomerAuthClient\Api\UsersApi;
use Ensi\CustomerAuthClient\ApiException;
use Ensi\CustomerAuthClient\Dto\User;
use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\CustomersClient\Dto\Customer;

/**
 * Class CustomerAction
 * @package App\Domain\Customers\Actions\Customers
 */
abstract class CustomerAction
{
    /**
     * CustomerAction constructor.
     * @param  CustomersApi  $customersApi
     * @param  UsersApi  $usersApi
     */
    public function __construct(protected CustomersApi $customersApi, protected UsersApi $usersApi)
    {
    }

    /**
     * @param  Customer  $customer
     * @param  User|null  $user
     * @return CustomerData
     * @throws ApiException
     */
    protected function prepareCustomer(Customer $customer, ?User $user = null): CustomerData
    {
        if (is_null($user) && $customer->getUserId()) {
            $user = $this->usersApi->getUser($customer->getUserId())->getData();
        }

        $customerData = new CustomerData($customer);
        if ($user) {
            $customerData->setUser($user);
        }

        return $customerData;
    }
}
