<?php

namespace App\Domain\Customers\Actions\Customers;

/**
 * Class DeleteCustomerAction
 * @package App\Domain\Customers\Actions\Customers
 */
class DeleteCustomerAction extends CustomerAction
{
    /**
     * @param int $customerId
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function execute(int $customerId): void
    {
        $this->customersApi->deleteCustomer($customerId);
    }
}
