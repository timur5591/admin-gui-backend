<?php

namespace App\Domain\Customers\Actions\Customers;

use App\Domain\Customers\Data\CustomerData;
use Ensi\CustomersClient\Dto\CustomerForCreate;

/**
 * Class CreateCustomerAction
 * @package App\Domain\Customers\Actions\Customers
 */
class CreateCustomerAction extends CustomerAction
{
    /**
     * @param  array  $fields
     * @return CustomerData
     * @throws \Ensi\CustomerAuthClient\ApiException
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function execute(array $fields): CustomerData
    {
        return $this->prepareCustomer($this->customersApi->createCustomer(new CustomerForCreate($fields))->getData());
    }
}
