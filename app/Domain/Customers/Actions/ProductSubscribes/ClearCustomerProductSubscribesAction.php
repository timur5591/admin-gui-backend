<?php

namespace App\Domain\Customers\Actions\ProductSubscribes;

use Ensi\CrmClient\Dto\ClearProductSubscribesRequest;

class ClearCustomerProductSubscribesAction extends ProductSubscribesAction
{
    public function execute(array $fields): void
    {
        $this->productSubscribesApi->clearProductSubscribe(new ClearProductSubscribesRequest($fields));
    }
}
