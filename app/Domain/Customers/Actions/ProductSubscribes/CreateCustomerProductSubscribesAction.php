<?php

namespace App\Domain\Customers\Actions\ProductSubscribes;

use Ensi\CrmClient\Dto\ProductSubscribe;
use Ensi\CrmClient\Dto\ProductSubscribeForCreate;

class CreateCustomerProductSubscribesAction extends ProductSubscribesAction
{
    public function execute(array $fields): ProductSubscribe
    {
        return $this->productSubscribesApi->createProductSubscribe(new ProductSubscribeForCreate($fields))->getData();
    }
}
