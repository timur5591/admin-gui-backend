<?php

namespace App\Domain\Common\Data\Meta\Fields;

use App\Http\ApiV1\OpenApiGenerated\Enums\FieldTypeEnum;

class PluralNumericField extends AbstractField
{
    protected function type(): FieldTypeEnum
    {
        return FieldTypeEnum::PLURAL_NUMERIC;
    }

    protected function init()
    {
        $this->sort()->filterRange();
    }
}
