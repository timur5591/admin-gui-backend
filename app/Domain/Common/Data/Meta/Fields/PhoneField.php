<?php

namespace App\Domain\Common\Data\Meta\Fields;

use App\Http\ApiV1\OpenApiGenerated\Enums\FieldTypeEnum;

class PhoneField extends AbstractField
{
    protected function type(): FieldTypeEnum
    {
        return FieldTypeEnum::PHONE;
    }

    protected function init()
    {
        $this->filterLike()->sort();
    }
}
