<?php

namespace App\Domain\Common\Data\Meta\Enum\Logistic;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;

class LogisticPointEnumInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->endpointName = 'logistic.searchPointEnumValues';
    }
}
