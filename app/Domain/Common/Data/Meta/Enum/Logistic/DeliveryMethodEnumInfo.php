<?php

namespace App\Domain\Common\Data\Meta\Enum\Logistic;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;
use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Dto\DeliveryMethodEnum;

class DeliveryMethodEnumInfo extends AbstractEnumInfo
{
    public function __construct(protected DeliveryServicesApi $deliveryServicesApi)
    {
        $this->enumClassToValues(DeliveryMethodEnum::class);
    }
}
