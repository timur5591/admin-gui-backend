<?php

namespace App\Domain\Common\Data\Meta\Enum\Logistic;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;
use Ensi\LogisticClient\Dto\DeliveryServiceEnum;

class DeliveryServiceEnumInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->enumClassToValues(DeliveryServiceEnum::class);
    }
}
