<?php

namespace App\Domain\Common\Data\Meta\Enum\Catalog;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;

class BrandEnumInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->endpointName = 'catalog.searchBrandEnumValues';
    }
}
