<?php

namespace App\Domain\Common\Data\Meta\Enum\Catalog;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;
use Ensi\PimClient\Dto\PropertyTypeEnum;

class PropertyTypeEnumInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->enumClassToValues(PropertyTypeEnum::class);
    }
}
