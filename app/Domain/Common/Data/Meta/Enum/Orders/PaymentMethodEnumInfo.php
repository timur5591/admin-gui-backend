<?php

namespace App\Domain\Common\Data\Meta\Enum\Orders;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;
use Ensi\OmsClient\Dto\PaymentMethodEnum;

class PaymentMethodEnumInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->enumClassToValues(PaymentMethodEnum::class);
    }
}
