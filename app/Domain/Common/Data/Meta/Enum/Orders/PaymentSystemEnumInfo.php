<?php

namespace App\Domain\Common\Data\Meta\Enum\Orders;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;
use Ensi\OmsClient\Dto\PaymentSystemEnum;

class PaymentSystemEnumInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->enumClassToValues(PaymentSystemEnum::class);
    }
}
