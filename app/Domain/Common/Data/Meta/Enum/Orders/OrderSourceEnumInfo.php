<?php

namespace App\Domain\Common\Data\Meta\Enum\Orders;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;
use Ensi\OmsClient\Dto\OrderSourceEnum;

class OrderSourceEnumInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->enumClassToValues(OrderSourceEnum::class);
    }
}
