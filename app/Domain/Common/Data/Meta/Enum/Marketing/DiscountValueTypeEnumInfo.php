<?php

namespace App\Domain\Common\Data\Meta\Enum\Marketing;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;
use Ensi\MarketingClient\Dto\DiscountValueTypeEnum;

class DiscountValueTypeEnumInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->enumClassToValues(DiscountValueTypeEnum::class);
    }
}
