<?php

namespace App\Domain\Common\Data\Meta\Enum\Customers;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;

class CustomerUserEnumInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->endpointName = 'customers.searchCustomerEnumValues';
    }
}
