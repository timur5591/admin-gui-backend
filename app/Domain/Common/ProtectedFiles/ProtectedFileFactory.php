<?php

namespace App\Domain\Common\ProtectedFiles;

use App\Domain\Communication\ProtectedFiles\MessageAttachment;
use App\Domain\Logistic\ProtectedFiles\DeliveryServiceDocumentFile;
use App\Domain\Orders\ProtectedFiles\OrderFile;
use App\Domain\Orders\ProtectedFiles\RefundFile;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProtectedFileFactory
{
    public function build($entity): ProtectedFile
    {
        return match ($entity) {
            MessageAttachment::entity() => new MessageAttachment(),
            DeliveryServiceDocumentFile::entity() => new DeliveryServiceDocumentFile(),
            OrderFile::entity() => new OrderFile(),
            RefundFile::entity() => new RefundFile(),
            default => throw new ModelNotFoundException("Не удалось определить сущность"),
        };
    }
}
