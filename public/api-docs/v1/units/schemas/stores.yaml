StoreAddress:
 type: object
 description: Адрес
 properties:
  address_string:
   type: string
   description: Адрес одной строкой
   example: "107140, г Москва, Красносельский р-н, ул Красносельская Верхн., д 3А"
  country_code:
   type: string
   description: Код страны
   example: "RU"
  post_index:
   type: string
   description: Почтовый индекс
   example: "107140"
  region:
   type: string
   description: Регион
   example: "г Москва"
  region_guid:
   type: string
   description: ФИАС ID региона
   example: "0c5b2444-70a0-4932-980c-b4dc0d3f02b5"
  area:
   type: string
   description: Район в регионе
   example: "Центральный"
  area_guid:
   type: string
   description: ФИАС ID района в регионе
   example: ""
  city:
   type: string
   description: Город/населенный пункт
   example: "г Москва"
  city_guid:
   type: string
   description: ФИАС ID города/населенного пункта
   example: "0c5b2444-70a0-4932-980c-b4dc0d3f02b5"
  street:
   type: string
   description: Улица
   example: "ул Красносельская Верхн."
  house:
   type: string
   description: Дом
   example: "д 3А"
  block:
   type: string
   description: Строение/корпус
   example: ""
  flat:
   type: string
   description: Квартира/офис
   example: ""
  porch:
   type: string
   description: Подъезд
   example: ""
  floor:
   type: string
   description: Этаж
   example: ""
  intercom:
   type: string
   description: Домофон
   example: ""
  comment:
   type: string
   description: Комментарий к адресу
   example: ""
  geo_lat:
   type: string
   description: "координаты: широта"
   example: "55.785513"
  geo_lon:
   type: string
   description: "координаты: долгота"
   example: "37.665408"
StoreReadonlyProperties:
 type: object
 properties:
  id:
   type: integer
   description: Идентификатор склада
   example: 1
  created_at:
   type: string
   description: Время создания склада
   example: "2021-01-15T14:55:35.000000Z"
  updated_at:
   type: string
   description: Время обновления склада
   example: "2021-01-15T14:55:35.000000Z"
StoreFillableProperties:
 type: object
 properties:
  seller_id:
   type: integer
   description: ID продавца
   example: 2
  xml_id:
   type: string
   description: ID склада в системе продавца
   example: "015"
  active:
   type: boolean
   description: Флаг активности склада
   example: true
  name:
   type: string
   description: Название
   example: "Сокольники"
  address:
   $ref: '#/StoreAddress'
  timezone:
   type: string
   description: Часовой пояс
   example: "Europe/Moscow"
StoreRequiredProperties:
 type: object
 required:
  - seller_id
  - name
StoreIncludes:
 type: object
 properties:
  workings:
   type: array
   items:
    $ref: './store_workings.yaml#/StoreWorking'
  contacts:
   type: array
   items:
    $ref: './store_contacts.yaml#/StoreContact'
  pickup_times:
   type: array
   items:
    $ref: './store_pickup_times.yaml#/StorePickupTime'
StoreForCreate:
 allOf:
  - $ref: '#/StoreFillableProperties'
  - $ref: '#/StoreRequiredProperties'
StoreForReplace:
 allOf:
  - $ref: '#/StoreFillableProperties'
  - $ref: '#/StoreRequiredProperties'
StoreForPatch:
 allOf:
  - $ref: '#/StoreFillableProperties'
Store:
 allOf:
  - $ref: '#/StoreReadonlyProperties'
  - $ref: '#/StoreFillableProperties'
  - $ref: '#/StoreIncludes'

StoresSearchRequest:
 type: object
 properties:
  sort:
   type: array
   items:
    type: string
    enum:
     - id
     - name
     - address_string
     - contact_name
     - contact_phone
  filter:
   type: object
   properties:
    id:
     type: integer
     description: ID склада
     example: 1
    seller_id:
     type: integer
     description: ID продавца
     example: 1
    name:
     type: string
     description: Название склада
     example: 'Третий склад'
    address_string:
     type: string
     description: Адрес склада
     example: '124482, г Москва, г Зеленоград, р-н Савелки, к 305'
    contact_name:
     type: string
     description: ФИО контактного лица
     example: 'Данилов Яков Евгеньевич'
    contact_phone:
     type: string
     description: Телефон контактного лица
     example: '+79996663322'
  include:
   type: array
   items:
    type: string
    enum:
     - contact
  pagination:
   $ref: '../../common_schemas.yaml#/RequestBodyPagination'

StoreResponse:
 type: object
 properties:
  data:
   $ref: '#/Store'
  meta:
   type: object
 required:
  - data
StoresSearchResponse:
 type: object
 properties:
  data:
   type: array
   items:
    $ref: '#/Store'
  meta:
   type: object
   properties:
    pagination:
     $ref: '../../common_schemas.yaml#/ResponseBodyPagination'
 required:
  - data
  - meta