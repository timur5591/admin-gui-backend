OrderReadonlyProperties:
  type: object
  properties:
    id:
      type: integer
      description: ID заказа
      example: 1
    number:
      type: string
      description: номер заказа
      example: "1000001"
    customer_id:
      type: integer
      description: id покупателя
      example: 5
    customer_email:
      type: string
      description: email покупателя
      example: "example@domain.ru"
    cost:
      type: integer
      description: стоимость (без учета скидки) (рассчитывается автоматически) в коп.
      example: 10000
    price:
      type: integer
      description: стоимость (с учетом скидок) (рассчитывается автоматически) в коп.
      example: 10000
    spent_bonus:
      type: integer
      description: списано бонусов
      example: 0
    promo_code:
      type: string
      description: Использованный промокод
      example: 'promo_code'
      nullable: true
    added_bonus:
      type: integer
      description: начислено бонусов
      example: 0
    source:
      type: integer
      description: источник взаимодействия из OrdersOrderSourceEnum
      example: 1
    status_at:
      type: string
      description: дата установки статуса заказа
      example: "2021-06-11T11:27:10.000000Z"
      nullable: true
    payment_status:
      type: integer
      description: статус оплаты из OrdersPaymentStatusEnum
      example: 1
    payment_status_at:
      type: string
      description: дата установки статуса оплаты
      example: "2021-06-11T11:27:10.000000Z"
      nullable: true
    payed_at:
      type: string
      format: date-time
      description: Дата оплаты
      example: "2020-12-15T15:21:34.000000Z"
      nullable: true
    payment_expires_at:
      type: string
      format: date-time
      description: Дата, до которой нужно провести оплату
      nullable: true
      example: "2020-12-15T15:21:34.000000Z"
    payment_method:
      type: integer
      description: метод оплаты из PaymentMethodEnum
    payment_system:
      type: integer
      description: система оплаты из PaymentSystemEnum
#    payment_link:
#      type: string
#      description: Ссылка для оплаты во внещней системе
#      nullable: true
    payment_external_id:
      type: string
      description: ID оплаты во внешней системе
      nullable: true
    is_problem_at:
      type: string
      description: дата установки флага проблемного заказа
      example: "2021-06-11T11:27:10.000000Z"
      nullable: true
    is_expired:
      type: boolean
      description: флаг, что заказ просроченный
    is_expired_at:
      type: string
      description: дата установки флага просроченного заказа
      example: "2021-06-11T11:27:10.000000Z"
      nullable: true
    is_return:
      type: boolean
      description: флаг, что заказ возвращен
    is_return_at:
      type: string
      description: дата установки флага возвращенного заказа
      example: "2021-06-11T11:27:10.000000Z"
      nullable: true
    is_partial_return:
      type: boolean
      description: флаг, что заказ частично возвращен
    is_partial_return_at:
      type: string
      description: дата установки флага частично возвращенного заказа
      example: "2021-06-11T11:27:10.000000Z"
      nullable: true
    created_at:
      type: string
      description: дата создания заказа
      example: "2021-06-11T11:27:10.000000Z"
      nullable: true
    updated_at:
      type: string
      description: дата обновления заказа
      example: "2021-06-11T11:27:10.000000Z"
      nullable: true
OrderFillableProperties:
  type: object
  properties:
    responsible_id:
      type: integer
      description: Идентификатор ответственного за заказ
      example: 1
      nullable: true
    status:
      type: integer
      description: статус заказа из OrdersOrderStatusEnum
      example: 1
    client_comment:
      type: string
      description: комментарий менеджера
      example: "покупатель хамит"
      nullable: true
    receiver_name:
      type: string
      description: имя получателя
      example: "Иванов Иван Иванович"
      nullable: true
    receiver_phone:
      type: string
      description: телефон получателя
      example: "+79998887766"
      nullable: true
    receiver_email:
      type: string
      description: e-mail получателя
      example: "ivanov@example.ru"
      nullable: true
    is_problem:
      type: boolean
      description: флаг, что заказ проблемный
      example: true
      nullable: true
    problem_comment:
      type: string
      description:  последнее сообщение продавца о проблеме со сборкой
      example: "нет части корзины"
      nullable: true
OrderDeliveryFillableProperties:
  type: object
  properties:
    delivery_service:
      type: integer
      description: служба доставки
      example: 11
    delivery_method:
      type: integer
      description: метод доставки
      example: 1
    delivery_cost:
      type: integer
      description: стоимость доставки (без учета скидки) в коп.
      example: 20000
    delivery_price:
      type: integer
      description: стоимость доставки (с учетом скидки) в коп.
      example: 20000
    delivery_tariff_id:
      type: integer
      description: ID тарифа на доставку из сервиса логистики
      example: 70
      nullable: true
    delivery_point_id:
      type: integer
      description: ID пункта самовывоза из сервиса логистики
      example: 55
      nullable: true
    delivery_address:
      $ref: './data/address.yaml#/Address'
    delivery_comment:
      type: string
      description: комментарий к доставке
      example: "Остановиться перед воротами"
      nullable: true
OrderIncludes:
  type: object
  properties:
    deliveries:
      type: array
      items:
        $ref: './deliveries.yaml#/Delivery'
    customer:
      $ref: '../../customers/schemas/customers.yaml#/Customer'
    responsible:
      $ref: '../../units/schemas/admin_users.yaml#/AdminUser'
    files:
      type: array
      items:
        $ref: './order_files.yaml#/OrderFile'

OrderForPatch:
  allOf:
    - $ref: '#/OrderFillableProperties'


OrderDeliveryForPatch:
  allOf:
    - $ref: '#/OrderDeliveryFillableProperties'

Order:
  allOf:
    - $ref: '#/OrderReadonlyProperties'
    - $ref: '#/OrderFillableProperties'
    - $ref: '#/OrderDeliveryFillableProperties'
    - $ref: '#/OrderIncludes'

SearchOrdersRequest:
  type: object
  properties:
    sort:
      type: array
      items:
        type: string
        enum:
          - number
          - created_at
          - price
          - delivery_price
    filter:
      type: object
      description: Большая часть доступных фильтров указана в /orders:meta
      properties:
        deliveries.shipments.seller_id:
          type: array
          items:
            type: integer
            description: Продавец отгрузки
        deliveries.shipments.store_id:
          type: array
          items:
            type: integer
            description: Продавец отгрузки
    include:
      type: array
      items:
        type: string
        description: orderItems.* будет добавляться только если загрузить элементы корзины через один из доступных способов (пока это только deliveries.shipments.orderItems)
        enum:
          # Добавляя новый include не забывай дорабатывать тест на all include
          - files
          - deliveries
          - deliveries.shipments
          - deliveries.shipments.orderItems
          - deliveries.shipments.seller
          - deliveries.shipments.store
          - responsible
          - customer
          - customer.user
          - orderItems.product
          - orderItems.product.images
          - orderItems.product.category
          - orderItems.product.brand
          - orderItems.stock
    pagination:
      $ref: '../../common_schemas.yaml#/RequestBodyPagination'

SearchOrdersResponse:
  type: object
  properties:
    data:
      type: array
      items:
        $ref: '#/Order'
    meta:
      type: object
      properties:
        pagination:
          $ref: '../../common_schemas.yaml#/ResponseBodyPagination'
  required:
    - data
    - meta

OrderResponse:
  type: object
  properties:
    data:
      $ref: '#/Order'
    meta:
      type: object
  required:
    - data

OrderChangePaymentSystemRequest:
  type: object
  properties:
    payment_system:
      type: integer
      description: Система оплаты из PaymentSystemEnum
  required:
    - payment_system

